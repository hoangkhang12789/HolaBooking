﻿using HolaBooking.Models;
using HolaBooking.Models.DAO;
using HolaBooking.src;
using Microsoft.AspNetCore.Mvc;

namespace HolaBooking.Controllers
{
    [Route("booking")]
    public class BookingViewController : Controller
    {
        [HttpGet("")]
        public IActionResult Booking(int HotelID, int RoomID,string booking)
        {

            var userID = HttpContext.Session.GetString("userID");
            if (userID == null)
            {
                return Redirect("/login");
            }
            else
            {
                String AccountID = Secret.Decode(userID);

                Profile profile = ProfileDao.GetAccountByID(Int32.Parse(AccountID));
                Hotel hotel = HotelDao.GetHotelByID(HotelID);
                Room room = RoomDao.GetRoomByID(RoomID);
                room.ListServices = ServiceDao.GetAllServiceByRoomID(RoomID);
                ViewBag.BookingStatus = booking;
                ViewBag.Profile = profile;
                ViewBag.Hotel = hotel;
                ViewBag.Room = room;

                return View("Views/Booking/Booking.cshtml");
            }
        }
    }
}
