﻿/*
 *Author : Nguyễn Hoàng Khang
*/
using HolaBooking.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Web;
using Microsoft.AspNetCore.Session;
using HolaBooking.Models.DAO;
using HolaBooking.src;

namespace HolaBooking.Controllers
{
    [Route("")]
    public class HomeController : Controller
    {

        [HttpGet("/")]
        public IActionResult HomePage()
        {
            
            var userID = HttpContext.Session.GetString("userID");
            if (userID != null)
            {
                AccountUser accountUser = AccountUserDao.GetAccountByUserId(Int32.Parse(Secret.Decode(userID)));
                if (accountUser.Enable == 0)
                {
                    HttpContext.Response.Cookies.Delete("userID");
                    HttpContext.Session.Remove("userID");
                    return Redirect("/warning");
                }
            }
            List<TopHotel> ListTopCountry = new List<TopHotel>();
            List<TopCountry> hotelsLocation = new List<TopCountry>();
            List<HotelByLocation> hotelByLocations = new List<HotelByLocation>();
            List<Room> GenDerPrice = SearchDAO.GetHotelRoomPriceList();
            var loaction = HttpContext.Request.Cookies["location"];
            if (loaction == null)
            {
                ListTopCountry = HomeDAO.GetListTopCountry();/*Load dữ liệu cho tabui*/
                hotelByLocations = HomeDAO.GetAllCityByLocation();
                foreach (var item in hotelByLocations)
                {
                    item.Img1 = HomeDAO.GetImgByCity(item.City);
                }



            }
            else
            {
                ListTopCountry = HomeDAO.GetListTopCountryByLocation(loaction);
                hotelsLocation = HomeDAO.GetListTopLocation(loaction);
                hotelByLocations = HomeDAO.GetListCityByLocation(loaction);


                foreach (var item in hotelByLocations)
                {
                    item.Img1 = HomeDAO.GetImgByCity(item.City);
                }

                foreach (var itemListCountry in hotelsLocation)
                {
                    itemListCountry.Img1 = HomeDAO.GetImgByCountry(itemListCountry.Country);
                }
                ViewBag.location = loaction;
            }

            foreach (TopHotel top in ListTopCountry)
            {
                top.HotelList = HomeDAO.GetListByCity(top.City);

            }
            ViewBag.IsLoaction = loaction;
            ViewBag.NameLocation = hotelsLocation;
            ViewBag.topHotel = ListTopCountry;
            ViewBag.GetCity = hotelByLocations;
            ViewBag.Room = GenDerPrice;

            return View();
        }






    }
}