﻿/*
 *Author : Nguyễn Hoàng Khang
*/
using HolaBooking.Models;
using HolaBooking.Models.DAO;
using HolaBooking.src;
using Microsoft.AspNetCore.Mvc.Filters;
using static HolaBooking.Models.DataConnection;
namespace HolaBooking.Controllers
{
    public class ActionFilter : IActionFilter
    {

        public void OnActionExecuted(ActionExecutedContext context)
        {
          
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {


            if (SQL.State.ToString() == "Closed")
            {

                SQL.Open();
            }

            var cookie = context.HttpContext.Request.Cookies["userID"];
            if (cookie != null)
            {
                AccountUser accountUser = AccountUserDao.GetAccountByUserId(Int32.Parse(Secret.Decode(cookie)));
                Profile profile = ProfileDao.GetAccountByID(Int32.Parse(Secret.Decode(cookie)));
                context.HttpContext.Session.SetString("userID", cookie);
                if (profile != null)
                {
                    String FullName = profile.Firstname + " " + profile.Lastname;
                    String? Image = profile.Avatar;
                    int PermissionID=  accountUser.PermissionID;
                    context.HttpContext.Session.SetString("name", FullName);
                    context.HttpContext.Session.SetString("img", Image);
                    context.HttpContext.Session.SetString("permissionID", PermissionID.ToString());

                }

            }


        }
    }
}
