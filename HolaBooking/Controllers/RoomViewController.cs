﻿using HolaBooking.Models;
using HolaBooking.Models.DAO;
using Microsoft.AspNetCore.Mvc;

namespace HolaBooking.Controllers
{
    [Route("room")]
    public class RoomViewController : Controller
    {
        [HttpGet("")]

        public IActionResult RoomSetup(string HotelID, string roomID)
        {
            if (roomID != null)
            {
                Room room = RoomDao.GetRoomByID(int.Parse(roomID));
                ViewBag.roomInfo = room;
            }
            else
            {
                Room room = new Room();
                room.RoomId = -1;
                ViewBag.roomInfo = room;
            }
            List<Device> devices = SearchDAO.GetAllDeviceList();
            List<Service> services = SearchDAO.GetAllServiceList();
            List<RoomType> roomTypes = RoomSetupDAO.GetAllRoomType();
            List<Service> allSevice = SearchDAO.GetAllServiceList();
            ViewBag.HotelID = HotelID;
            ViewBag.RoomTypes = roomTypes;
            ViewBag.Devices = devices;
            ViewBag.Services = services;
            ViewBag.AllSevice = allSevice;
            return View("Views/RoomSetup/RoomSetup.cshtml");

        }
        [HttpGet("update")]
        public IActionResult RoomSetting(string HotelID, string roomID)
        {
            Room room = RoomDao.GetRoomByID(int.Parse(roomID));
            List<Device> devices = SearchDAO.GetAllDeviceList();
            List<Service> services = SearchDAO.GetAllServiceList();
            List<RoomType> roomTypes = RoomSetupDAO.GetAllRoomType();
            List<Device> allDevice = DeviceDao.GetAllDeviceByRoomID(int.Parse(roomID));
            List<Service> roomService = ServiceDao.GetAllServiceByRoomID(int.Parse(roomID));

            ViewBag.roomID = roomID;
            ViewBag.allDevice = allDevice;
            ViewBag.allService = roomService;
            ViewBag.roomInfo = room;
            ViewBag.HotelID = HotelID;
            ViewBag.RoomTypes = roomTypes;
            ViewBag.Devices = devices;
            ViewBag.Services = services;
            return View("Views/MyAccount/RoomSetting.cshtml");
        }


    }
}
