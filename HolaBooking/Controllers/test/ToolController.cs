﻿
using HolaBooking.Models;
using HolaBooking.Models.DAO;
using HolaBooking.src;
using HolaBooking.Src.QRCode;
using Microsoft.AspNetCore.Mvc;
using QRCoder;
using System.Drawing;

namespace HolaBooking.Controllers.test
{
    [Route("process/tool")]
    public class ToolController : Controller
    {
        public IActionResult Index()
        {
            ;
            var a = HttpContext.Session.GetString("userID");

            //String Url = Src.QRCode.QRCode.ReadQrCode("wwwroot/image/QrCode/file-53d3.qrr");
            ViewBag.Image = "sss";
            ViewBag.Session = a;

            return View();
        }
        [HttpGet("encode")]
        
        public String Encode(String code)
        {
            return Secret.BCryptEncode(code);
        }
        [HttpGet("decode")]
        public String Decode(String code)
        {
            return Secret.Decode(code);
        }
       
    }
}
