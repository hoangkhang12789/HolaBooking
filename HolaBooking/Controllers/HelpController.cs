﻿using Microsoft.AspNetCore.Mvc;

namespace HolaBooking.Controllers
{
    public class HelpController : Controller
    {
        [Route("help")]
        public IActionResult Help()
        {
            Response.Cookies.Append("user", "hello");
            //HttpContext.Session.SetString("uid", "khang");
            return View();
        }
    }
}
