﻿using HolaBooking.Models;
using HolaBooking.Models.DAO;
using Microsoft.AspNetCore.Mvc;

namespace HolaBooking.Controllers
{
    [Route("search")]
    public class SearchViewController : Controller
    {
        [HttpGet("")]
        public IActionResult SearchAdvanced(string Name, string roomtype)
        {
            List<Hotel> listpage = new List<Hotel>();
            List<Hotel> listName = new List<Hotel>();
            double MaxPrice = RoomDao.GetMaxRoomPrice();
            List<String> fltStars = new List<String>();
            fltStars.Add("0");
            fltStars.Add($"{MaxPrice}");
            List<String> fltFaci = new List<String>();
            List<String> fltServi = new List<String>();
            if (roomtype == null || roomtype == "5")
            {
                roomtype = "10";
                listpage = SearchDAO.GetListByName(Name);
                listName = SearchDAO.GetPagingListByName(1, Name);
                //listpage = SearchDAO.GetAllFilterLists(Name, int.Parse(roomtype), 1, fltStars, fltFaci, fltServi, false, false);
                //listName = SearchDAO.GetAllFilterLists(Name, int.Parse(roomtype), 1, fltStars, fltFaci, fltServi, false, true);
            }
            else
            {
                listpage = SearchDAO.GetAllFilterLists(Name, int.Parse(roomtype), 1, fltStars, fltFaci, fltServi, true, false, false, true);
                listName = SearchDAO.GetAllFilterLists(Name, int.Parse(roomtype), 1, fltStars, fltFaci, fltServi, true, true, false, true);
            }
            List<Facilities> listFaci = SearchDAO.GetFacilitiesList();
            List<Facilities> AllFaci = SearchDAO.GetAllFacilities();
            List<Service> AllSev = SearchDAO.GetAllServiceList();
            List<Room> listRoom = SearchDAO.GetHotelRoomPriceList();
            int endPage = 0;
            if (listpage == null)
            {
                ViewBag.Name = null;
            }
            else
            {
                ViewBag.Name = listName;
                int total = listpage.Count;
                endPage = total / 4;
                if (total % 4 != 0)
                {
                    endPage++;
                }
            }

            ViewBag.maxPrice = MaxPrice;
            ViewBag.coutPage = endPage;
            ViewBag.Faci = listFaci;
            ViewBag.Room = listRoom;
            ViewBag.AllFaci = AllFaci;
            ViewBag.AllSev = AllSev;
            ViewBag.type = roomtype;
            ViewBag.searchName = Name;
            return View("Views/Search/SearchAdvanced.cshtml");
        }
    }
}
