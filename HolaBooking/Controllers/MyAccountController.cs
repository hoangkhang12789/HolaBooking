﻿using Microsoft.AspNetCore.Mvc;
using HolaBooking.src;
using HolaBooking.Models.DAO;
using HolaBooking.Models;
using System.Globalization;

namespace HolaBooking.Controllers
{
    [Route("account")]
    public class MyAccountController : Controller
    {
        [HttpGet("profile")]
        public IActionResult Profile()
        {
            var user = HttpContext.Session.GetString("userID");
            var userID = "";
            if (user != null)
            {
                userID = Secret.Decode(user);
                AccountUser account = AccountUserDao.GetAccountByUserId(Int32.Parse(userID));
                Profile profile = ProfileDao.GetAccountByID(Int32.Parse(userID));
                ViewBag.Account = account;
                ViewBag.Profile = profile;
                return View();
            }
            else
            {
                return Redirect("/");
            }


        }

        [HttpGet("statistical")]
        public IActionResult Statistical()
        {
            List<int> ListInComming = new List<int>();
            List<int> ListComplete = new List<int>();
            List<int> ListCancel = new List<int>();
            Time InComming = new Time();
            Time Complete = new Time();
            Time Cancel = new Time();
            CultureInfo provider = CultureInfo.InvariantCulture;
            string user = Secret.Decode(HttpContext.Session.GetString("userID"));
            AccountUser account = AccountUserDao.GetAccountByUserId(Int32.Parse(user));
            int PermissionID = account.PermissionID;

            List<Booking> Listbooking = new List<Booking>();
           
            if (PermissionID == 1)
            {
                
                Listbooking = BookingDao.GetALlBooking();
            }
            if (PermissionID == 2)
            {

                Listbooking = BookingDao.GetALlBookingByHotelOfAccountID(Int32.Parse(user));
            }
            if (PermissionID == 3)
            {
                Listbooking = BookingDao.GetALlBookingByAccountID(Int32.Parse(user));
              
            }
            if (Listbooking != null)
            {
                foreach (Booking booking in Listbooking)
                {
                    if (booking.CheckOut != "")
                    {

                        DateTime time = DateTime.Parse(booking.CheckIn);
                        if (DateTime.Now.Year == time.Year)
                        {
                           

                            switch (time.Month)
                            {
                                case 1:
                                    if (booking.BookingStatusID == 1)
                                    {
                                        InComming.January++;
                                    }
                                    if (booking.BookingStatusID == 2)
                                    {
                                        Complete.January++;
                                    }
                                    if (booking.BookingStatusID == 3)
                                    {
                                        Cancel.January++;
                                    }
                                    break;
                                case 2:
                                    if (booking.BookingStatusID == 1)
                                    {
                                        InComming.February++;
                                    }
                                    if (booking.BookingStatusID == 2)
                                    {
                                        Complete.February++;
                                    }
                                    if (booking.BookingStatusID == 3)
                                    {
                                        Cancel.February++;
                                    }
                                    break;
                                case 3:
                                    if (booking.BookingStatusID == 1)
                                    {
                                        InComming.March++;
                                    }
                                    if (booking.BookingStatusID == 2)
                                    {
                                        Complete.March++;
                                    }
                                    if (booking.BookingStatusID == 3)
                                    {
                                        Cancel.March++;
                                    }
                                    break;
                                case 4:
                                    if (booking.BookingStatusID == 1)
                                    {
                                        InComming.April++;
                                    }
                                    if (booking.BookingStatusID == 2)
                                    {
                                        Complete.April++;
                                    }
                                    if (booking.BookingStatusID == 3)
                                    {
                                        Cancel.April++;
                                    }
                                    break;
                                case 5:
                                    if (booking.BookingStatusID == 1)
                                    {
                                        InComming.May++;
                                    }
                                    if (booking.BookingStatusID == 2)
                                    {
                                        Complete.May++;
                                    }
                                    if (booking.BookingStatusID == 3)
                                    {
                                        Cancel.May++;
                                    }
                                    break;
                                case 6:
                                    if (booking.BookingStatusID == 1)
                                    {
                                        InComming.June++;
                                    }
                                    if (booking.BookingStatusID == 2)
                                    {
                                        Complete.June++;
                                    }
                                    if (booking.BookingStatusID == 3)
                                    {
                                        Cancel.June++;
                                    }
                                    break;
                                case 7:
                                    if (booking.BookingStatusID == 1)
                                    {
                                        InComming.July++;
                                    }
                                    if (booking.BookingStatusID == 2)
                                    {
                                        Complete.July++;
                                    }
                                    if (booking.BookingStatusID == 3)
                                    {
                                        Cancel.July++;
                                    }
                                    break;
                                case 8:
                                    if (booking.BookingStatusID == 1)
                                    {
                                        InComming.August++;
                                    }
                                    if (booking.BookingStatusID == 2)
                                    {
                                        Complete.August++;
                                    }
                                    if (booking.BookingStatusID == 3)
                                    {
                                        Cancel.August++;
                                    }
                                    break;
                                case 9:
                                    if (booking.BookingStatusID == 1)
                                    {
                                        InComming.September++;
                                    }
                                    if (booking.BookingStatusID == 2)
                                    {
                                        Complete.September++;
                                    }
                                    if (booking.BookingStatusID == 3)
                                    {
                                        Cancel.September++;
                                    }
                                    break;
                                case 10:
                                    if (booking.BookingStatusID == 1)
                                    {
                                        InComming.October++;
                                    }
                                    if (booking.BookingStatusID == 2)
                                    {
                                        Complete.October++;
                                    }
                                    if (booking.BookingStatusID == 3)
                                    {
                                        Cancel.October++;
                                    }
                                    break;
                                case 11:
                                    if (booking.BookingStatusID == 1)
                                    {
                                        InComming.November++;
                                    }
                                    if (booking.BookingStatusID == 2)
                                    {
                                        Complete.November++;
                                    }
                                    if (booking.BookingStatusID == 3)
                                    {
                                        Cancel.November++;
                                    }
                                    break;
                                case 12:
                                    if (booking.BookingStatusID == 1)
                                    {
                                        InComming.December++;
                                    }
                                    if (booking.BookingStatusID == 2)
                                    {
                                        Complete.December++;
                                    }
                                    if (booking.BookingStatusID == 3)
                                    {
                                        Cancel.December++;
                                    }
                                    break;
                            }
                        }
                    }

                }
            }
            else
            {
                
            }
           
            ListInComming.Add(InComming.January);
            ListInComming.Add(InComming.February);
            ListInComming.Add(InComming.March);
            ListInComming.Add(InComming.April);
            ListInComming.Add(InComming.May);
            ListInComming.Add(InComming.June);
            ListInComming.Add(InComming.July);
            ListInComming.Add(InComming.August);
            ListInComming.Add(InComming.September);
            ListInComming.Add(InComming.October);
            ListInComming.Add(InComming.November);
            ListInComming.Add(InComming.December);


            ListComplete.Add(Complete.January);
            ListComplete.Add(Complete.February);
            ListComplete.Add(Complete.March);
            ListComplete.Add(Complete.April);
            ListComplete.Add(Complete.May);
            ListComplete.Add(Complete.June);
            ListComplete.Add(Complete.July);
            ListComplete.Add(Complete.August);
            ListComplete.Add(Complete.September);
            ListComplete.Add(Complete.October);
            ListComplete.Add(Complete.November);
            ListComplete.Add(Complete.December);

            ListCancel.Add(Cancel.January);
            ListCancel.Add(Cancel.February);
            ListCancel.Add(Cancel.March);
            ListCancel.Add(Cancel.April);
            ListCancel.Add(Cancel.May);
            ListCancel.Add(Cancel.June);
            ListCancel.Add(Cancel.July);
            ListCancel.Add(Cancel.August);
            ListCancel.Add(Cancel.September);
            ListCancel.Add(Cancel.October);
            ListCancel.Add(Cancel.November);
            ListCancel.Add(Cancel.December);

            ViewBag.InComming = ListInComming;
            ViewBag.Complete = ListComplete;
            ViewBag.Cancel = ListCancel;
            return View();
        }



        //my hotel
        //all acount
        [HttpGet("allAcount")]
        public IActionResult AllAcount()
        {
            var user = HttpContext.Session.GetString("userID");
            int userID = Int32.Parse(Secret.Decode(user));
            List<Profile> profiles = AccountManagerDAO.GetAllProfile();
            List<AccountUser> users = AccountManagerDAO.GetAllAccount();
            ViewBag.AccountID = userID;
            ViewBag.profiles = profiles;
            ViewBag.users = users;
            return View();
        }

        [HttpGet("allHotel")]
        public IActionResult AllHotel()
        {
            
            List<Profile> profiles = AccountManagerDAO.GetAllProfile();
            List<Hotel> hotels = AccountManagerDAO.GetListHotel();
            List<AccountUser> users = AccountManagerDAO.GetAllAccount();
            ViewBag.listhotel = hotels;
            ViewBag.profiles = profiles;
            ViewBag.users = users;
            return View();
        }

        [HttpGet("MyBooking")]
        public IActionResult AllBooking()
        {
            var user = HttpContext.Session.GetString("userID");
            int userID = Int32.Parse(Secret.Decode(user));
            
            List<AccountUser> users = AccountManagerDAO.GetAllAccount();
            List<Booking> bookings = BookingDao.GetMyBookingByStatusID(userID,1);
            List<Room> accRooms = AccountManagerDAO.GetRoomBookingByAccountID(userID, 1);
            List<Hotel> accHotels = AccountManagerDAO.GetHotelBookingByAccountID(userID, 1);
            List<Booking> bookings2 = BookingDao.GetMyBookingByStatusID(userID,2);
            List<Room> accRooms2 = AccountManagerDAO.GetRoomBookingByAccountID(userID, 2);
            List<Hotel> accHotels2 = AccountManagerDAO.GetHotelBookingByAccountID(userID, 2);
            List<Booking> bookings3 = BookingDao.GetMyBookingByStatusID(userID,3);
            List<Room> accRooms3 = AccountManagerDAO.GetRoomBookingByAccountID(userID, 3);
            List<Hotel> accHotels3 = AccountManagerDAO.GetHotelBookingByAccountID(userID, 3);
            ViewBag.users = users;
            ViewBag.AccountID = userID;
            if (accHotels == null)
            {
                ViewBag.bookings = null;
                ViewBag.accRooms = null;
                ViewBag.accHotels = null;
            }
            else
            {
                ViewBag.bookings = bookings;
                ViewBag.accRooms = accRooms;
                ViewBag.accHotels = accHotels;
            }

            if (accHotels2 == null)
            {
                ViewBag.bookings2 = null;
                ViewBag.accRooms2 = null;
                ViewBag.accHotels2 = null;
            }
            else
            {
                ViewBag.bookings2 = bookings2;
                ViewBag.accRooms2 = accRooms2;
                ViewBag.accHotels2 = accHotels2;
            }

            if (accHotels3 == null)
            {
                ViewBag.bookings3 = null;
                ViewBag.accRooms3 = null;
                ViewBag.accHotels3 = null;
            }
            else
            {
                ViewBag.bookings3 = bookings3;
                ViewBag.accRooms3 = accRooms3;
                ViewBag.accHotels3 = accHotels3;
            }

            return View();
        }

        [HttpGet("myhotel")]
        public IActionResult MyHotel()
        {
            var user = HttpContext.Session.GetString("userID");
            int userId = Int32.Parse(Secret.Decode(user));
            List<Hotel> list = HotelDao.GetHotelByAccountID(userId);
            if (list == null)
            {
                ViewBag.ListHotel = null;
            }
            else
            {
                ViewBag.ListHotel = list;
            }

            return View();
        }
        [HttpGet("myroom")]
        public IActionResult MyRoom(int hotelID)
        {
            //var user = HttpContext.Session.GetString("userID");
            //int userId = Int32.Parse(Secret.Decode(user));
            
            List<Room> list = RoomDao.GetAllRoomOfHotel(hotelID);
            ViewBag.HotelID = hotelID;
            if (list == null)
            {
                ViewBag.ListRoom = null;
            }
            else
            {
                ViewBag.ListRoom = list;
            }
            return View();
        }

        
    }
}
