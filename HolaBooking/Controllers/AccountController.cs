﻿/*
 *Author : Nguyễn Hoàng Khang
*/
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HolaBooking.Controllers
{
    public class AccountController : Controller
    {
        [Route("login")]
        public IActionResult Login()
        {
            return View();
        }
        [Route("register")]
        public IActionResult Register()
        {
            return View();
        }
        [Route("warning")]
        public IActionResult Warning()
        {
            return View();
        }

    }
        
}
