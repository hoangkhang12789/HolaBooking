﻿using HolaBooking.Models;
using HolaBooking.Models.DAO;
using Microsoft.AspNetCore.Mvc;

namespace HolaBooking.Controllers
{
    [Route("hotel")]
    public class HotelViewController : Controller
    {
        [HttpGet("view")]
        public IActionResult RoomDetail(String ID)
        {
            int Cleanliness = 0;
            int Facilitiesss = 0;
            int Location = 0;
            int Service = 0;

            Hotel hotel = HotelDao.GetHotelByID(Int32.Parse(ID));
            List<Room> listRoom = RoomDao.GetAllRoomOfHotel(hotel.HotelID);
            if (listRoom != null && listRoom.Count > 0)
            {
                foreach (Room room in listRoom)
                {
                    room.ListDevices = DeviceDao.GetAllDeviceByRoomID(room.RoomId);
                }
            }
            List<Review> listReview = ReviewDao.GetAllReviewByHotelID(hotel.HotelID);
            List<Facilities> listFacilities = FacilitiesDao.GetAllFacilitiesByHotelID(hotel.HotelID);
            if (listReview != null)
            {
                foreach (Review review in listReview)
                {
                    Cleanliness += review.Cleanliness;
                    Facilitiesss += review.Facilities;
                    Location += review.Location;
                    Service += review.Service;

                }
                ViewBag.Cleanliness = Math.Round(Decimal.Divide(Cleanliness, listReview.Count), 1);
                ViewBag.Faci = Math.Round(Decimal.Divide(Facilitiesss, listReview.Count), 1);
                ViewBag.Location = Math.Round(Decimal.Divide(Location, listReview.Count), 1);
                ViewBag.Service = Math.Round(Decimal.Divide(Service, listReview.Count), 1);
                ViewBag.Count = listReview.Count;
                ViewData["review"] = listReview;
            }
            else
            {
                ViewBag.Cleanliness = 10;
                ViewBag.Faci = 10;
                ViewBag.Location = 10;
                ViewBag.Service = 10;
                ViewBag.Count = 0;
            }
            if (hotel.Point >= 9)
            {
                ViewBag.PointName = "Exceptional";
            }
            if (hotel.Point < 9 && hotel.Point >= 7)
            {
                ViewBag.PointName = "Good";
            }
            if (hotel.Point < 7 && hotel.Point >= 4)
            {
                ViewBag.PointName = "Bad";
            }
            if (hotel.Point < 4)
            {
                ViewBag.PointName = "Very Bad";
            }
            ViewBag.Hotel = hotel;

            ViewData["facilities"] = listFacilities;
            return View("Views/RoomDetail/RoomDetail.cshtml", listRoom);
        }
    }
}
