﻿using HolaBooking.Models;
using HolaBooking.Models.DAO;
using Microsoft.AspNetCore.Mvc;
using HolaBooking.src;
using System.Text.Json;

namespace HolaBooking.Controllers.Process
{
    [Route("process/roomdetail")]
    public class RoomDetailController : Controller
    {


        [HttpGet("review")]
        public IActionResult GetReview(String hotelID, String Text, String Cleanliness, String Facilitiesss, String Location, String Service)
        {

            int Clean = 0;
            int Faci = 0;
            int Loct = 0;
            int Serv = 0;
            var account = HttpContext.Session.GetString("userID");

            var accountID = Secret.Decode(account);
            ReviewDao.CreateAndUpdateReview(Int32.Parse(accountID), Int32.Parse(hotelID), Text, Int32.Parse(Cleanliness), Int32.Parse(Facilitiesss), Int32.Parse(Location), Int32.Parse(Service));
            List<Review> listReview = ReviewDao.GetAllReviewByHotelID(Int32.Parse(hotelID));
            foreach (Review review in listReview)
            {
                Clean += review.Cleanliness;
                Faci += review.Facilities;
                Loct += review.Location;
                Serv += review.Service;
            }
            Decimal point1 = Decimal.Divide(Clean, listReview.Count);
            Decimal point2 = Decimal.Divide(Faci, listReview.Count);
            Decimal point3 = Decimal.Divide(Loct, listReview.Count);
            Decimal point4 = Decimal.Divide(Serv, listReview.Count);
            Decimal point = point1 + point2 + point3 + point4;
            ViewBag.Hotel = hotelID;

            HotelDao.UpdateHotelPoint(Int32.Parse(hotelID), (float)Math.Round(Decimal.Divide(point, 4), 1));
            return View(listReview);
        }
        [HttpGet("point")]
        public IActionResult GetReviewPoint(String hotelID)
        {

            int Clean = 0;
            int Faci = 0;
            int Loct = 0;
            int Serv = 0;
            List<Review> listReview = ReviewDao.GetAllReviewByHotelID(Int32.Parse(hotelID));
            foreach (Review review in listReview)
            {
                Clean += review.Cleanliness;
                Faci += review.Facilities;
                Loct += review.Location;
                Serv += review.Service;
            }
            Decimal point1 = Decimal.Divide(Clean, listReview.Count);
            Decimal point2 = Decimal.Divide(Faci, listReview.Count);
            Decimal point3 = Decimal.Divide(Loct, listReview.Count);
            Decimal point4 = Decimal.Divide(Serv, listReview.Count);
            Decimal HotelPoint = Math.Round(Decimal.Divide((point1 + point2 + point3 + point4), 4), 1);

            HotelDao.UpdateHotelPoint(Int32.Parse(hotelID), (float)HotelPoint);
            List<Decimal> point = new List<Decimal>();
            point.Add(Math.Round(point1, 1));
            point.Add(Math.Round(point2, 1));
            point.Add(Math.Round(point3, 1));
            point.Add(Math.Round(point4, 1));
            point.Add(HotelPoint);
            return Json(JsonSerializer.Serialize(point));
        }
        [HttpDelete("delete")]
        public void DeleteReview(String ReviewID, String hotelID)
        {
            ReviewDao.DeleteReviewByReviewID(Int32.Parse(ReviewID));
            int Clean = 0;
            int Faci = 0;
            int Loct = 0;
            int Serv = 0;
            List<Review> listReview = ReviewDao.GetAllReviewByHotelID(Int32.Parse(hotelID));
            foreach (Review review in listReview)
            {
                Clean += review.Cleanliness;
                Faci += review.Facilities;
                Loct += review.Location;
                Serv += review.Service;
            }
            Decimal point1 = Decimal.Divide(Clean, listReview.Count);
            Decimal point2 = Decimal.Divide(Faci, listReview.Count);
            Decimal point3 = Decimal.Divide(Loct, listReview.Count);
            Decimal point4 = Decimal.Divide(Serv, listReview.Count);
            Decimal HotelPoint = Decimal.Divide((point1 + point2 + point3 + point4), 4);
            HotelDao.UpdateHotelPoint(Int32.Parse(hotelID), (float)Math.Round(HotelPoint, 1));


        }
        [HttpPost("addtocard")]
        public IActionResult AddtoCard(string roomID)
        {
            var user = HttpContext.Session.GetString("userID");
            var userID = "";
            if (user == null)
            {
                return Json("false");
            }
            else
            {
                 userID = Secret.Decode(user);
                var bookingID = BookingDao.InsertBooking(Int32.Parse(roomID), Int32.Parse(userID), DateTime.Now.ToString(), DateTime.Now.ToString(), 0);
                return Json(bookingID);

            }

        }
    }
}
