﻿/*
 *Author : Huỳnh Minh Nhật
*/
using HolaBooking.Models;
using Microsoft.AspNetCore.Mvc;
using System.Web;
using Microsoft.AspNetCore.Session;
using HolaBooking.Models.DAO;
using System;

namespace HolaBooking.Controllers
{
    [Route("process/search")]
    public class SearchController : Controller
    {


        [HttpGet("page")]
        public IActionResult LoadPage(string name, int roomtype, int page, List<String> filter)
        {
            
            List<Facilities> AllFaci = SearchDAO.GetAllFacilities();
            List<Service> AllSev = SearchDAO.GetAllServiceList();
            int f = AllFaci.Count;
            int s = AllSev.Count;
            int p = f + s;

            List<Hotel> listPage = new List<Hotel>();
            double MaxPrice = RoomDao.GetMaxRoomPrice();

            //filter
            List<String> fltStars = new List<String>();
            List<String> fltFaci = new List<String>();
            List<String> fltServi = new List<String>();
            for (int i = 0; i < filter.Count - p; i++)
            {
                if (filter[i] != "null")
                {
                    fltStars.Add(filter[i]);
                }
            }
            for (int i = 7; i < filter.Count - s; i++)
            {
                if (filter[i] != "null")
                {
                    fltFaci.Add(filter[i]);
                }
            }
            for (int i = 7 + f; i < filter.Count; i++)
            {
                if (filter[i] != "null")
                {
                    fltServi.Add(filter[i]);
                }
            }

            //check list null
            bool isFaci = fltFaci.Count > 0;
            bool isRoom = fltServi.Count > 0 || (fltStars[0] != "0" || fltStars[1] != $"{MaxPrice}");


            if (roomtype == 5)
            {
                listPage = SearchDAO.GetAllFilterLists(name, 1, page, fltStars, fltFaci, fltServi, false, true, isFaci, isRoom);
            }
            else
            {
                listPage = SearchDAO.GetAllFilterLists(name, roomtype, page, fltStars, fltFaci, fltServi, true, true, isFaci, true);
            }
            List<Facilities> listFaci = SearchDAO.GetFacilitiesList();
            List<Room> listRoom = SearchDAO.GetHotelRoomPriceList();
            if (listPage == null)
            {
                ViewBag.page = null;
            }
            else
            {
                ViewBag.page = listPage;
            }
            ViewBag.nowPage = page;
            ViewBag.Faci = listFaci;
            ViewBag.Room = listRoom;
            return View();
        }

        [HttpGet("filter")]
        public IActionResult LoadFilter(string name, int roomtype, List<String> filter)
        {
            
            List<Facilities> AllFaci = SearchDAO.GetAllFacilities();
            List<Service> AllSev = SearchDAO.GetAllServiceList();
            int f = AllFaci.Count;
            int s = AllSev.Count;
            int p = f + s;

            List<Hotel> listFilt = new List<Hotel>();
            List<Hotel> listPage = new List<Hotel>();
            double MaxPrice = RoomDao.GetMaxRoomPrice();
            //filter
            List<String> fltStars = new List<String>();
            List<String> fltFaci = new List<String>();
            List<String> fltServi = new List<String>();
            for (int i = 0; i < filter.Count - p; i++)
            {
                if (filter[i] != "null")
                {
                    fltStars.Add(filter[i]);
                }
            }
            for (int i = 7; i < filter.Count - s; i++)
            {
                if (filter[i] != "null")
                {
                    fltFaci.Add(filter[i]);
                }
            }
            for (int i = 7 + f; i < filter.Count; i++)
            {
                if (filter[i] != "null")
                {
                    fltServi.Add(filter[i]);
                }
            }
            
            bool isFaci = fltFaci.Count > 0;
            bool isRoom = fltServi.Count > 0 || (fltStars[0] != "0" || fltStars[1] != $"{MaxPrice}");
            

            if (roomtype == 5)
            {
                listPage = SearchDAO.GetAllFilterLists(name, 1, 1, fltStars, fltFaci, fltServi, false, false, isFaci, isRoom);
                listFilt = SearchDAO.GetAllFilterLists(name, 1, 1, fltStars, fltFaci, fltServi, false, true, isFaci, isRoom);
            }
            else
            {
                listPage = SearchDAO.GetAllFilterLists(name, roomtype, 1, fltStars, fltFaci, fltServi, true, false, isFaci, true);
                listFilt = SearchDAO.GetAllFilterLists(name, roomtype, 1, fltStars, fltFaci, fltServi, true, true, isFaci, true);
            }
            List<Facilities> listFaci = SearchDAO.GetFacilitiesList();
            List<Room> listRoom = SearchDAO.GetHotelRoomPriceList();
            
            //  paging, check null
            int endPage = 0;
            if (listPage == null)
            {
                ViewBag.fltr = null;
            }
            else
            {
                ViewBag.fltr = listFilt;
                int total = listPage.Count;
                endPage = total / 4;
                if (total % 4 != 0)
                {
                    endPage++;
                }
            }

            //GET
            ViewBag.coutPage = endPage;
            ViewBag.Faci = listFaci;
            ViewBag.Room = listRoom;
            return View();
        }
    }
}
