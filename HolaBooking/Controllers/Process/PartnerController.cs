﻿using HolaBooking.Models;
using HolaBooking.src;
using Microsoft.AspNetCore.Mvc;
using System.Web;
using Microsoft.AspNetCore.Session;
using HolaBooking.Models.DAO;
using System;

namespace HolaBooking.Controllers.Process
{   
    

    [Route("process/partner")]
    public class PartnerController : Controller
    {
        
        [HttpPost("insert")]
        public IActionResult HotelInsert(String hotelname, String address, String district, String city, String country, String star, String description, String latitude, String longitude, List<String> listImg, List<String> addFacilities)
        {
            List<Facilities> ListFacilities = SearchDAO.GetAllFacilities();
           
            Hotel h = new Hotel();

            var user = Secret.Decode(HttpContext.Session.GetString("userID"));

            h.AccountID = Int32.Parse(user);
            h.Name = hotelname;
            h.Address = address;
            h.District = district;
            h.City = city;
            h.Country = country;
            h.Star = double.Parse(star);
            h.Point = 10;
            h.Description = description;
            h.Latitude = double.Parse(latitude);
            h.Longitude = double.Parse(longitude);

            h.Img1 = listImg[0];
            h.Img2 = listImg[1];
            h.Img3 = listImg[2];
            h.Img4 = listImg[3];
            h.Img5 = listImg[4];

            HotelDao.InsertHotel(h);
            h.HotelID = HotelDao.GetHotelIDByLast();

            List<String> listTemp = new List<String>();

            List<Facilities> listfaci = new List<Facilities>();

            for (int i = 0; i < addFacilities.Count; i++)
            {
                int temp = HotelDao.GetFacilitiesIDByName(addFacilities[i]);
                if (temp == -1)
                {
                    listTemp.Add(addFacilities[i]);
                }
                else
                {
                    foreach (Facilities faciliti in ListFacilities)
                    {
                        if (faciliti.FacilitiesName.Contains(addFacilities[i]))
                        {
                            listfaci.Add(faciliti);
                        }
                    }
                }
            }
            if (listTemp.Count > 0)
            {
                HotelDao.InsertFacilities(listTemp);
                ListFacilities = SearchDAO.GetAllFacilities();

                for (int i = 0; i < listTemp.Count; i++)
                {
                    foreach (Facilities faciliti in ListFacilities)
                    {
                        if (faciliti.FacilitiesName.Contains(listTemp[i]))
                        {
                            listfaci.Add(faciliti);
                        }
                    }
                }
            }
            HotelDao.InsertHotelFacilities(listfaci, h.HotelID);
            return Json("");

        }
       
        [HttpPut("HotelEdit")]
        public void HotelEdit(String hotelID,String hotelname, String address, String district, String city, String country, String star, String description, String latitude, String longitude, List<String> listImg, List<String> addFacilities)
        {
            List<Facilities> ListFacilities = SearchDAO.GetAllFacilities();
            
            Hotel h = new Hotel();

            var user = Secret.Decode(HttpContext.Session.GetString("userID"));

            h.HotelID = int.Parse(hotelID);

            h.AccountID = Int32.Parse(user);
            h.Name = hotelname;
            h.Address = address;
            h.District = district;
            h.City = city;
            h.Country = country;
            h.Star = double.Parse(star);
            h.Point = 10;
            h.Description = description;
            h.Latitude = double.Parse(latitude);
            h.Longitude = double.Parse(longitude);

            h.Img1 = listImg[0];
            h.Img2 = listImg[1];
            h.Img3 = listImg[2];
            h.Img4 = listImg[3];
            h.Img5 = listImg[4];

            HotelDao.UpdateHotel(h);
            //h.HotelID = HotelDao.GetHotelIDByLast();
            HotelDao.deleteFacilitiesByHotelID(h.HotelID);

            List<String> listTemp = new List<String>();

            List<Facilities> listfaci = new List<Facilities>();

            for (int i = 0; i < addFacilities.Count; i++)
            {
                int temp = HotelDao.GetFacilitiesIDByName(addFacilities[i]);
                if (temp == -1)
                {
                    listTemp.Add(addFacilities[i]);
                }
                else
                {
                    foreach (Facilities faciliti in ListFacilities)
                    {
                        if (faciliti.FacilitiesName.Contains(addFacilities[i]))
                        {
                            listfaci.Add(faciliti);
                        }
                    }
                }
            }
            if (listTemp.Count > 0)
            {
                HotelDao.InsertFacilities(listTemp);
                ListFacilities = SearchDAO.GetAllFacilities();

                for (int i = 0; i < listTemp.Count; i++)
                {
                    foreach (Facilities faciliti in ListFacilities)
                    {
                        if (faciliti.FacilitiesName.Contains(listTemp[i]))
                        {
                            listfaci.Add(faciliti);
                        }
                    }
                }
            }
            HotelDao.InsertHotelFacilities(listfaci, h.HotelID);

        }

    }
}
