﻿using HolaBooking.Models;
using HolaBooking.Models.DAO;
using HolaBooking.src;
using Microsoft.AspNetCore.Mvc;

namespace HolaBooking.Controllers.Process
{
    [Route("process/booking")]
    public class BookingController : Controller
    {

        [HttpGet("insert")]
        public IActionResult InsertBooking(int roomID, string checkin, string checkout, string price, String listservice)
        {
            var AccountId = Secret.Decode(HttpContext.Session.GetString("userID"));
            if (AccountId == null)
            {
                return Redirect("/login");
            }
            else
            {
                //Console.WriteLine(booking);
                var bookingID = BookingDao.InsertBooking(roomID, Int32.Parse(AccountId), checkin, checkout, float.Parse(price));
                if (listservice != null && listservice.Length > 0)
                {
                    List<String> Service = listservice.Split(',').ToList();
                    ServiceDao.InsertBookingService(Convert.ToInt32(bookingID), Service);
                }

                return Json(bookingID);
            }




        }
        [HttpPut("update")]
        public void updateBooking(int bookingID, int bookingStatusID)
        {

            BookingDao.updateBooking(bookingID, bookingStatusID);

        }
    }
}
