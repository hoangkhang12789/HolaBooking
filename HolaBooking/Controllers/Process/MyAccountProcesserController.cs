﻿using HolaBooking.Models.DAO;
using HolaBooking.src;
using Microsoft.AspNetCore.Mvc;

namespace HolaBooking.Controllers.Process
{
    [Route("process/myAccount")]
    public class MyAccountProcesserController : Controller
    {
        [HttpPut("updateFullName")]
        public void UpdateFullName(String firstName, String lastName)
        {
            var user = HttpContext.Session.GetString("userID");
            if (user != null)
            {
                var userID = Secret.Decode(user);
                ProfileDao.UpdateFullName(Int32.Parse(userID), firstName, lastName);

            }
        }
        [HttpPut("updatePhone")]
        public void UpdatePhone(String phone)
        {
            var user = HttpContext.Session.GetString("userID");
            if (user != null)
            {
                var userID = Secret.Decode(user);
                ProfileDao.UpdatePhone(Int32.Parse(userID), phone);

            }
        }
        [HttpPut("updateEmail")]
        public void UpdateEmail(String mail)
        {
            var user = HttpContext.Session.GetString("userID");
            if (user != null)
            {
                var userID = Secret.Decode(user);
                ProfileDao.UpdateEmail(Int32.Parse(userID), mail);

            }
        }
        [HttpPut("updatePass")]
        public void UpdatePassWord(String pass)
        {
            var user = HttpContext.Session.GetString("userID");
            if (user != null)
            {
                var userID = Secret.Decode(user);
                AccountUserDao.UpdatePassWord(Int32.Parse(userID), pass);

            }
        }
        [HttpPut("updateimage")]
        public string UpdateAvatar(string img)
        {
            var userID = Secret.Decode(HttpContext.Session.GetString("userID"));
            ProfileDao.UpdateImage(Int32.Parse(userID), img);
            return img;
        }
    }
}
