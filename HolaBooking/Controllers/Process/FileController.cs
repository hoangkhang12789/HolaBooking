﻿/*
 *Author : Nguyễn Hoàng Khang
*/
using HolaBooking.Src;
using Microsoft.AspNetCore.Mvc;

namespace HolaBooking.Controllers.Process
{
    [Route("file/")]
    public class FileController : Controller
    {
        private IWebHostEnvironment _hostingEnvironment;

        public FileController(IWebHostEnvironment environment)
        {
            _hostingEnvironment = environment;
        }
        [HttpPost("upload/image")]
        public void UploadImage(IFormFile files, String folder, String fileName)
        {
            string filePath = Path.Combine(_hostingEnvironment.WebRootPath, @"image\" + folder);
            IO.UploadImage(files, filePath, fileName + ".png");

        }
        [HttpPost("delete/image")]
        public void DeleteImage(String path)
        {
            IO.DeleteFile(_hostingEnvironment.WebRootPath + path);
        }
    }
}
