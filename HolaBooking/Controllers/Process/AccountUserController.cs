﻿/*
 *Author : Nguyễn Hoàng Khang
*/
using HolaBooking.Models;
using HolaBooking.Models.DAO;
using HolaBooking.src;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HolaBooking.Controllers.Process
{
    [Route("process/account")]
    public class AccountUserController : Controller
    {
        [HttpGet("signin")]
        public Boolean CheckAccountExists(String userName,String password,String check )
        {
            if (userName == null)
            {
                userName = "    ";
            }
            if (password == null)
            {
                password = "   ";
            }
            if (AccountUserDao.IsAccountExists(userName, password))
            {
                AccountUser accountUser = AccountUserDao.GetAccountByUserName(userName);
                CookieOptions cookieOptions = new CookieOptions();
                cookieOptions.MaxAge = TimeSpan.FromHours(1);
                if (check == "true")
                {
                    cookieOptions.MaxAge = TimeSpan.FromDays(7);
                }
                HttpContext.Response.Cookies.Append("userID", Secret.Encode(accountUser.Id.ToString()), cookieOptions);
                return true;
            }
            
           return false; 
        }
        [HttpGet("checkUserName")]
        public Boolean CheckUserNameAvailable(String userName)
        {
            return AccountUserDao.IsAvailableUsername(userName);
        }
        [HttpPost("createAccount")]
        public void CreateNewAccount(String userName, String password)
        {
            AccountUserDao.CreateNewAccount(userName,password);
            ProfileDao.CreateProfile(AccountUserDao.GetAccountByUserName(userName).Id);
        }
        [HttpGet("logout")]
        public void Logout()
        {
            HttpContext.Response.Cookies.Delete("userID");
            HttpContext.Session.Remove("userID");
         
        }
        
        [HttpPut("updateStatus")]
        public void ChangeAccountStatus(string Status, int AccountId)
        {
            if (Status != null)
            {
                bool enable = true;
                if (Status == "Disable")
                {
                    enable = false;
                }
                AccountManagerDAO.ChangeAccountEnable(AccountId, enable);
            }
        }

        [HttpPut("updateRoom")]
        public void RoomInsert(string roomID, string HotelID, string bedName, string roomPrice, string roomSize, string roomType, string roomPeople, string roomBeds, List<String> listImg,
            List<String> listDevice, List<String> listSevice, List<String> listSPrice, List<String> listDefault)
        {
            
            List<Device> devices = SearchDAO.GetAllDeviceList();
            List<Service> services = SearchDAO.GetAllServiceList();
            
            bool isNewType = false;
            Room room = new Room();
            room.RoomId = int.Parse(roomID);
            room.HotelID = int.Parse(HotelID);
            room.RooomType = roomType;
            room.Image1 = listImg[0];
            room.Image2 = listImg[1];
            room.Image3 = listImg[2];
            room.Image4 = listImg[3];
            room.Image5 = listImg[4];
            room.RoomSize = int.Parse(roomSize);
            room.Price = int.Parse(roomPrice);
            room.BedName = bedName;
            room.Sleep = int.Parse(roomPeople);
            room.Bed = int.Parse(roomBeds);
            room.ListDevices = new List<Device>();
            room.ListServices = new List<Service>();

            int t = RoomSetupDAO.GetRoomTypeIDByName(roomType);
            if (t == -1)
            {
                isNewType = true;
            }
            //DAO
            RoomSetupDAO.UpdateRoomInfo(room, isNewType);
            //delete and add new
            RoomSetupDAO.deleteDeviceByRoomID(room.RoomId);
            RoomSetupDAO.deleteServiceByRoomID(room.RoomId);
            // new or old device
            List<String> listTemp = new List<String>();
            for (int i = 0; i < listDevice.Count; i++)
            {
                int temp = RoomSetupDAO.GetDeviceIDByName(listDevice[i]);
                if (temp == -1)
                {
                    listTemp.Add(listDevice[i]);
                }
                else
                {
                    foreach (Device device in devices)
                    {
                        if (device.DeviceName.Contains(listDevice[i]))
                        {
                            room.ListDevices.Add(device);
                        }
                    }
                }
            }
            if (listTemp.Count > 0)
            {
                RoomSetupDAO.InsertDevice(listTemp);
                devices = SearchDAO.GetAllDeviceList();

                for (int i = 0; i < listTemp.Count; i++)
                {
                    foreach (Device device in devices)
                    {
                        if (device.DeviceName.Contains(listTemp[i]))
                        {
                            room.ListDevices.Add(device);
                        }
                    }
                }
            }
            RoomSetupDAO.InsertRoomDevice(room);

            // new or old sevice
            List<String> listTemp2 = new List<String>();
            List<String> setDefault = new List<String>();
            List<String> setPrice = new List<String>();
            for (int i = 0; i < listSevice.Count; i++)
            {
                int temp = RoomSetupDAO.GetSeviceIDByName(listSevice[i]);
                if (temp == -1)
                {
                    listTemp2.Add(listSevice[i]);
                    setDefault.Add(listDefault[i]);
                    setPrice.Add(listSPrice[i]);
                }
                else
                {
                    foreach (Service service in services)
                    {
                        if (service.ServiceName.Contains(listSevice[i]))
                        {
                            if (listDefault[i] == "0")
                            {
                                service.ServiceDefault = true;
                            }
                            else
                            {
                                service.ServiceDefault = false;
                            }
                            room.ListServices.Add(service);
                        }
                    }
                }
            }
            if (listTemp2.Count > 0)
            {
                RoomSetupDAO.InsertSevice(listTemp2, setPrice);
                services = SearchDAO.GetAllServiceList();
                for (int i = 0; i < listTemp2.Count; i++)
                {
                    foreach (Service service in services)
                    {
                        if (service.ServiceName.Contains(listTemp2[i]))
                        {
                            if (listDefault[i] == "0")
                            {
                                service.ServiceDefault = true;
                            }
                            else
                            {
                                service.ServiceDefault = false;
                            }
                            room.ListServices.Add(service);
                        }
                    }
                }
            }
            RoomSetupDAO.InsertRoomSevice(room);
        }

        [HttpPost("deleteHotel")]
        public void DeleteHotelByID(String hotelID)
        {
            HotelDao.DeleteHotelByID(int.Parse(hotelID));
        }
        [HttpPost("deleteRoom")]
        public void DeleteRoomByID(String roomID)
        {
            RoomDao.DeleteRoomByID(int.Parse(roomID));
        }

        [HttpPut("updatepermission")]
        public void updatePermission(int PermissionID)
        {
            var user = HttpContext.Session.GetString("userID");
            var userID = Secret.Decode(user);
            AccountUserDao.UpdatePermission(Int32.Parse(userID), PermissionID);

        }
    }
}
