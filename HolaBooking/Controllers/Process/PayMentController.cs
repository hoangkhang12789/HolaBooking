﻿/*
 *Author : Nguyễn Hoàng Khang
*/
using HolaBooking.Models;
using HolaBooking.Models.DAO;
using HolaBooking.src;
using HolaBooking.Src.MomoPayment;
using HolaBooking.SrcMomoPayment;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Web;

namespace HolaBooking.Controllers.Process
{
    [Route("process/payment/")]
    public class PayMentController : Controller
    {
        [HttpGet("link")]
        public String Payment(String Mess, int amount,String ExtraData)
        {
             amount = amount*1000;
            return MomoConnection.MomoResponse(Mess, amount.ToString(), ExtraData);

        }
        [HttpGet("confirm")]
        public IActionResult ConfirmPaymentClient()
        {
            int a = Int32.Parse(HttpContext.Request.Query["errorCode"]);
            if (a == 0)
            {
                return View("Views/Payment/Success.cshtml");
            }
            return View("Views/Payment/Error.cshtml");

        }

        [HttpPost("save")]
        public async void SavePayment()
        {
            HttpContext.Request.ContentType="application/json";
            var reader = new StreamReader(HttpContext.Request.Body);
            var body = await reader.ReadToEndAsync();
            var collection = HttpUtility.ParseQueryString(body);
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(collection.AllKeys.ToDictionary(y => y, y => collection[y]));
            var data = JObject.Parse(json);
            int errorCode = Int32.Parse(data.GetValue("errorCode").ToString());
            int bookingId = Int32.Parse(data.GetValue("extraData").ToString());
            if(errorCode == 0)
            {
               
                Booking booking = BookingDao.GetBookingByID(bookingId);
                String QRContent = "BookingId:" + Secret.Encode(booking.BookingID.ToString()) +
                                    "#RoomID:" + Secret.Encode(booking.RoomID.ToString()) +
                                    "#AccountID:" + Secret.Encode(booking.BookingID.ToString()) +
                                    "#CheckIn:" + booking.CheckIn +
                                    "#CheckOut:" + booking.CheckOut;
                String link = Src.QRCode.QRCode.CreateQrCode(QRContent);
                BookingDao.UpdateBookingComplete(bookingId,link);
            }


        }
    }
}


