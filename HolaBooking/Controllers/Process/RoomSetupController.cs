﻿/*
 *Author : Huỳnh Minh Nhật
*/
using Microsoft.AspNetCore.Mvc;
using HolaBooking.Models;
using System.Web;
using Microsoft.AspNetCore.Session;
using HolaBooking.Models.DAO;
using System;

namespace HolaBooking.Controllers.Process
{
    [Route("process/roomsetup")]
    public class RoomSetupController : Controller
    {
        

        [HttpPost("insert")]
        public IActionResult RoomInsert(string HotelID, string bedName, string roomPrice, string roomSize, string roomType, string roomPeople, string roomBeds, List<String> listImg, 
            List<String> listDevice, List<String> listSevice, List<String> listSPrice, List<String> listDefault)
        {
           
            List<Device> devices = SearchDAO.GetAllDeviceList();
            List<Service> services = SearchDAO.GetAllServiceList();
            List<RoomType> roomTypes = RoomSetupDAO.GetAllRoomType();
            bool isNewType = false;
            Room room = new Room();
            room.HotelID = int.Parse(HotelID);
            room.RooomType = roomType;
            room.Image1 = listImg[0];
            room.Image2 = listImg[1];
            room.Image3 = listImg[2];
            room.Image4 = listImg[3];
            room.Image5 = listImg[4];
            room.RoomSize = int.Parse(roomSize);
            room.Price = int.Parse(roomPrice);
            room.BedName = bedName;
            room.Sleep = int.Parse(roomPeople);
            room.Bed = int.Parse(roomBeds);
            room.Discount = 0;
            room.Status = false;
            room.ListDevices = new List<Device>();
            room.ListServices = new List<Service>();

            int t = RoomSetupDAO.GetRoomTypeIDByName(roomType);
            if (t == -1)
            {
                isNewType = true;
            }
            //DAO
            RoomSetupDAO.InsertRoomInfo(room, isNewType);
            room.RoomId = RoomSetupDAO.GetRoomIDByLast();
            // new or old device
            List<String> listTemp = new List<String>();
            for (int i = 0; i < listDevice.Count; i++)
            {
                int temp = RoomSetupDAO.GetDeviceIDByName(listDevice[i]);
                if (temp == -1)
                {
                    listTemp.Add(listDevice[i]);
                }
                else
                {
                    foreach (Device device in devices)
                    {
                        if (device.DeviceName.Contains(listDevice[i]))
                        {
                            room.ListDevices.Add(device);
                        }
                    }
                }
            }
            if (listTemp.Count > 0)
            {
                RoomSetupDAO.InsertDevice(listTemp);
                devices = SearchDAO.GetAllDeviceList();

                for (int i = 0; i < listTemp.Count; i++)
                {
                    foreach (Device device in devices)
                    {
                        if (device.DeviceName.Contains(listTemp[i]))
                        {
                            room.ListDevices.Add(device);
                        }
                    }
                }
            }
            RoomSetupDAO.InsertRoomDevice(room);

            // new or old sevice
            List<String> listTemp2 = new List<String>();
            List<String> setDefault = new List<String>();
            List<String> setPrice = new List<String>();
            for (int i = 0; i < listSevice.Count; i++)
            {
                int temp = RoomSetupDAO.GetSeviceIDByName(listSevice[i]);
                if (temp == -1)
                {
                    listTemp2.Add(listSevice[i]);
                    setDefault.Add(listDefault[i]);
                    setPrice.Add(listSPrice[i]);
                }
                else
                {
                    foreach (Service service in services)
                    {
                        if (service.ServiceName.Contains(listSevice[i]))
                        {
                            if (listDefault[i] == "0")
                            {
                                service.ServiceDefault = true;
                            }
                            else
                            {
                                service.ServiceDefault = false;
                            }
                            room.ListServices.Add(service);
                        }
                    }
                }
            }
            if (listTemp2.Count > 0)
            {
                RoomSetupDAO.InsertSevice(listTemp2, setPrice);
                services = SearchDAO.GetAllServiceList();
                for (int i = 0; i < listTemp2.Count; i++)
                {
                    foreach (Service service in services)
                    {
                        if (service.ServiceName.Contains(listTemp2[i]))
                        {
                            if (listDefault[i] == "0")
                            {
                                service.ServiceDefault = true;
                            }
                            else
                            {
                                service.ServiceDefault = false;
                            }
                            room.ListServices.Add(service);
                        }
                    }
                }
            }
            RoomSetupDAO.InsertRoomSevice(room);

            return Json("");
        }
        /*******************************************************************/
        [HttpDelete("deleteroom")]
        public void deleteRoom(int RoomID)
        {
            RoomDao.DeleteRoomByID(RoomID);
        }
    }
}
