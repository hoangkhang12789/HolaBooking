﻿using HolaBooking.Models;
using HolaBooking.Models.DAO;
using Microsoft.AspNetCore.Mvc;

namespace HolaBooking.Controllers
{
    [Route("partner")]
    public class PartnerViewController : Controller
    {
        [HttpGet("")]
        public IActionResult PartnerView()
        {
            var user = HttpContext.Session.GetString("userID");
            if (user == null)
            {
                return Redirect("/login");
            }
            else
            {
                return View("Views/Partner/PartnerView.cshtml");
            }
            
        }
        [HttpGet("HotelSetUp")]
        public IActionResult HotelSetUp()
        {
            List<Facilities> ListFacilities = SearchDAO.GetAllFacilities();
            ViewBag.listFaci = ListFacilities;
            return View("Views/Partner/HotelSetUp.cshtml");
        }
        [HttpGet("HotelEdit")]
        public IActionResult HotelEdit(String hotelID)
        {
            List<Facilities> ListFaci = FacilitiesDao.GetAllFacilitiesByHotelID(Int32.Parse(hotelID));
            Hotel hotel = HotelDao.GetHotelByID(Int32.Parse(hotelID));
            List<Facilities> ListFacilities = SearchDAO.GetAllFacilities();
            ViewBag.hotelID = hotelID;
            ViewBag.Hotel = hotel;
            ViewBag.listFaci = ListFacilities;
            ViewBag.listFacili = ListFaci;
            return View("Views/Partner/HotelEdit.cshtml");
        }
    }
}
