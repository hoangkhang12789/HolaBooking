﻿using Microsoft.AspNetCore.Mvc;

namespace HolaBooking.Controllers
{
    [Route("error")]
    public class ErrorController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
    }
}
