﻿using Microsoft.AspNetCore.Mvc;

namespace HolaBooking.Controllers
{
    public class AboutController : Controller
    {
        [Route("about")]
        public IActionResult About()
        {
            return View();
        }
    }
}
