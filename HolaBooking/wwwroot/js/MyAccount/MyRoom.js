﻿// delete room
//delete Room
let btnDeleteHotel = document.querySelectorAll(".deteleRoom");
btnDeleteHotel.forEach(d => {
    d.addEventListener("click", () => {
        let RoomID = d.getAttribute("RoomID");
        let popupform = document.getElementById('id01');
        popupform.style.display = 'block';
        document.querySelector(".deletebtn").setAttribute("id", RoomID);;


    });
});
let confirmDelete = document.querySelector(".deletebtn");
confirmDelete.addEventListener("click", () => {
    let query = new QueryData("account/deleteRoom");
    query.addParam("roomID", confirmDelete.id);
    query.addEvent("load", function () {
        btnDeleteHotel.forEach(d => {
            if (d.getAttribute("RoomID") == confirmDelete.id) {
                d.parentNode.parentNode.parentNode.parentNode.removeChild(d.parentNode.parentNode.parentNode);
            }
        });
    });
    query.send("POST");
});

// Get the modal
var modal = document.getElementById('id01');

// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
//===============
let EditBtn = document.querySelectorAll(".btn-edit")
EditBtn.forEach(e => {
    e.addEventListener("click", () => {
        window.location = "/room/update?HotelID=" + e.getAttribute("hotelid") + "&roomID=" + e.getAttribute("roomid")
    })
})
let AddNewBtn = document.querySelector(".btn-add");
AddNewBtn.addEventListener("click", () => {
    window.location = "/room?HotelID=" + AddNewBtn.getAttribute("hotelid")
})
