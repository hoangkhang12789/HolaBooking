﻿let btnDeleteHotel = document.querySelectorAll(".deteleHotel");
btnDeleteHotel.forEach(d => {
    d.addEventListener("click", () => {
        let hotelID = d.getAttribute("hotelID");
        let popupform = document.getElementById('id01');
        popupform.style.display = 'block';
        document.querySelector(".deletebtn").setAttribute("id", hotelID);;


    });
});
let confirmDelete = document.querySelector(".deletebtn");
confirmDelete.addEventListener("click", () => {
    let query = new QueryData("account/deleteHotel");
    query.addParam("hotelID", confirmDelete.id);
    query.addEvent("load", function () {
        btnDeleteHotel.forEach(d => {
            if (d.getAttribute("hotelid") == confirmDelete.id) {
                d.parentNode.parentNode.parentNode.parentNode.removeChild(d.parentNode.parentNode.parentNode);
            }
        });
    });
    query.send("POST");
});

// Get the modal
var modal = document.getElementById('id01');

// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

let AddHotel = document.querySelector(".btn-add");
console.log(AddHotel)
AddHotel.addEventListener("click", () => {
    window.location ="/partner/hotelsetup"
})
let EditBtn = document.querySelectorAll(".btn-edit-hotel");
EditBtn.forEach(e => {
    e.addEventListener("click", () => {
        window.location = "/partner/HotelEdit?hotelID=" + e.getAttribute("hotelid")
    })
})
let ViewBtn = document.querySelectorAll(".btn-view-room");
ViewBtn.forEach(e => {
    e.addEventListener("click", () => {
        window.location = "/account/myroom?hotelID=" + e.getAttribute("hotelid")
    })
})
let ViewHotelBtn = document.querySelectorAll(".viewHotelInfo");
ViewHotelBtn.forEach(e => {
    e.addEventListener("click", () => {
        window.location = "/hotel/view?ID=" + e.getAttribute("hotelid")
    })
})
