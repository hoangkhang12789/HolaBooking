﻿let profile = document.querySelector("#profile");
profile.addEventListener("click", () => {
    window.location = "/account/profile";
})
let statistical = document.querySelector("#statistical");
statistical.addEventListener("click", () => {
    window.location = "/account/statistical";
})
let booking = document.querySelector("#booking");
booking.addEventListener("click", () => {
    window.location = "/account/MyBooking";
})
let accManager = document.querySelector("#accManager");
if (accManager != undefined) {
    accManager.addEventListener("click", () => {
        window.location = "/account/allAcount";
    })
}

let HotelManager = document.querySelector("#HotelManager");
if (HotelManager != undefined) {
    HotelManager.addEventListener("click", () => {
        window.location = "/account/allHotel";
    })
}


let MyHotel = document.querySelector("#MyHotel");
if (MyHotel != null) {
    MyHotel.addEventListener("click", () => {
        window.location = "/account/myhotel";
    })
}


let btnviewHotelInfo = document.querySelectorAll(".viewHotelInfo");
btnviewHotelInfo.forEach(b => {
    b.addEventListener("click", () => {
        let hotelID = b.getAttribute("hotelid");
        window.location = "/hotel/view?ID=" + hotelID;
    });
});



