let BooKingTypeBtn = document.querySelectorAll(".booking-manager-status span");
let BookingBox = document.querySelectorAll(".booking-manager")
BooKingTypeBtn.forEach((element, index) => {
    element.addEventListener("click", () => {
        BookingBox.forEach(e => {
            e.classList.remove("activeBooking");
        })
        BookingBox[index%3].classList.add("activeBooking");
        

    })
})
let BookingBtn = document.querySelectorAll(".booking-mn-pay button");

BookingBtn.forEach(e => {
    e.addEventListener("click", () => {
        window.location = `/booking?HotelID=${e.getAttribute("hotelid")}&RoomID=${e.getAttribute("roomid")}&&booking=${e.getAttribute("bookingid")}`
    })
})
let Cancel = document.querySelectorAll(".booking-mn-cancel button")
Cancel.forEach(e => {
    e.addEventListener("click", () => {
        let query = new QueryData("booking/update");
        query.addParam("bookingID", e.getAttribute("bookingid"));
        query.addParam("bookingStatusID", 3);
        query.addEvent("load", function () {
            e.parentNode.parentNode.parentNode.parentNode.parentNode.remove();
        })
        query.send("PUT");
    })
})

