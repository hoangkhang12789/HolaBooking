﻿
$('#imageInput').on('change', function () {
    $input = $(this);
    if ($input.val().length > 0) {
        fileReader = new FileReader();
        fileReader.onload = function (data) {
            $('.image-preview').attr('src', data.target.result);
        }
        fileReader.readAsDataURL($input.prop('files')[0]);
        $('.image-button').css('display', 'none');
        $('.image-preview').css('display', 'block');
        $('.change-image').css('display', 'block');
    }
});

$('.change-image').on('click', function () {
    $control = $(this);
    $('#imageInput').val('');
    $preview = $('.image-preview');
    $preview.attr('src', '');
    $preview.css('display', 'none');
    $control.css('display', 'none');
    $('.image-button').css('display', 'block');
});

let roomName = document.querySelector(".BedName");
let roomPrice = document.querySelector(".RoomPrice");
let roomSize = document.querySelector(".RoomSize");
let roomType = document.querySelector(".RoomType");
let roomPeople = document.querySelector(".Persons");
let roomBeds = document.querySelector(".ExtraBeds");
let allType = document.querySelectorAll(".TypeItem");
let typeInfoBedName = document.querySelectorAll(".TypeBedName");
let typeInfoSleep = document.querySelectorAll(".TypeSleep");
let typeInfoBed = document.querySelectorAll(".TypeBed");

$(document.querySelector(".Room-Type-option")).on('change', 'input', function () {
    var options = $('datalist')[0].options;
    for (var i = 0; i < options.length; i++) {
        if (allType[i].value == $(this).val()) {
            roomName.value = typeInfoBedName[i].textContent;
            roomName.disabled = true;
            roomPeople.value = typeInfoSleep[i].textContent;
            roomPeople.disabled = true;
            roomBeds.value = typeInfoBed[i].textContent;
            roomBeds.disabled = true;
            break;
        }
        else {
            roomName.value = '';
            roomName.disabled = false;
            roomPeople.value = '';
            roomPeople.disabled = false;
            roomBeds.value = '';
            roomBeds.disabled = false;
        }
    }
});

let AllSevID = document.querySelectorAll(".ASevID");
let AllSevName = document.querySelectorAll(".ASevName");
let AllSevPrice = document.querySelectorAll(".ASevPrice");
let AllSevDefault = document.querySelectorAll(".ASevDefault");
function seviceOptionLoad() {

    let seviceOption = document.querySelectorAll(".Service-Option");
    let sevicePrice = document.querySelectorAll(".ServicePrice");
    let seviceDefault = document.querySelectorAll("#ServiceDefault");

    seviceOption.forEach((element, index) => {

        $(element).on('change', ".SeviceName", function () {
            var options = $('#ServiceName')[0].options;
            for (var i = 0; i < options.length - 1; i++) {
                if (AllSevName[i].textContent == $(this).val()) {
                    sevicePrice[index].value = AllSevPrice[i].textContent;
                    sevicePrice[index].disabled = true;
                    //if (AllSevDefault[index].textContent == 'False') {
                    //    $(seviceDefault[index]).val("1");
                    //} else {
                    //    $(seviceDefault[index]).val("0");
                    //}
                    //$(seviceDefault[index]).prop('disabled', 'disabled');
                    break;
                }
                else {
                    sevicePrice[index].value = null;
                    sevicePrice[index].disabled = false;
                    //$(seviceDefault[index]).val("0");
                    //$(seviceDefault[index]).prop('disabled', false);
                }
            }
        });
    });
};
seviceOptionLoad();

let btnSaveRoom = document.querySelector(".SaveRoom");
btnSaveRoom.addEventListener('click', () => {
    let query = new QueryData("account/updateRoom");
    let roomID = document.querySelector(".roomID");
    let bedName = document.querySelector(".BedName");
    let roomPrice = document.querySelector(".RoomPrice");
    let roomSize = document.querySelector(".RoomSize");
    let roomType = document.querySelector(".RoomType");
    let roomPeople = document.querySelector(".Persons");
    let roomBeds = document.querySelector(".ExtraBeds");
    let roomDevice = document.querySelectorAll(".DeviceName");
    let roomSevice = document.querySelectorAll(".SeviceName");
    let seviPrice = document.querySelectorAll(".ServicePrice");
    let seviDefau = document.querySelectorAll("#ServiceDefault");


    for (var i = 0; i < listImgName.length; i++) {
        query.addParam("listImg", listImgName[i]);
    }
    for (var i = 0; i < roomDevice.length; i++) {
        if (roomDevice[i].value != '') {
            query.addParam("listDevice", roomDevice[i].value);
        }
    }
    for (var i = 0; i < roomSevice.length; i++) {
        if (roomSevice[i].value != '') {
            query.addParam("listSevice", roomSevice[i].value);
            query.addParam("listSPrice", seviPrice[i].value);
            query.addParam("listDefault", seviDefau[i].value);
        }
    }
    query.addParam("HotelID", btnSaveRoom.id);
    query.addParam("roomID", roomID.textContent);
    query.addParam("bedName", bedName.value);
    query.addParam("roomPrice", roomPrice.value);
    query.addParam("roomSize", roomSize.value);
    query.addParam("roomType", roomType.value);
    query.addParam("roomPeople", roomPeople.value);
    query.addParam("roomBeds", roomBeds.value);
    query.addEvent("load", function () {
        window.location = "/account/myroom?hotelID=" + btnSaveRoom.id;
    });
    query.send("PUT");
});


