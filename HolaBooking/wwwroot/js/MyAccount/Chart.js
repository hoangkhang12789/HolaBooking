﻿function LoadChart(listIncoming, listComplete, ListCancel, title) {

    const actions = [
        {
            name: 'Randomize',
            handler(chart) {
                chart.data.datasets.forEach(dataset => {
                    dataset.data = Utils.numbers({ count: chart.data.labels.length, min: -100, max: 100 });
                });
                chart.update();
            }
        },
    ];


    const labels = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September', 
        'October', 
        'November',
        'December'
    ];
    const data = {
        labels: labels,
        datasets: [
            {
                label: 'Incoming',
                data: listIncoming,
                borderColor: 'rgb(247, 185, 40)',
                backgroundColor: 'rgb(247, 185, 40)',
                yAxisID: 'y',
            },
            {
                label: 'Complete',
                data: listComplete,
                borderColor: 'rgb(0, 113, 255)',
                backgroundColor: 'rgb(0, 113, 255)',
                yAxisID: 'y1',
            }
            ,
            {
                label: 'Cancel',
                data: ListCancel,
                borderColor: 'rgb(228, 30, 63)',
                backgroundColor: 'rgb(228, 30, 63)',
                yAxisID: 'y2',
            }
        ]
    };
    const config = {
        type: 'line',
        data: data,
        options: {
            responsive: true,
            interaction: {
                mode: 'index',
                intersect: false,
            },
            stacked: false,
            plugins: {
                title: {
                    display: true,
                    text: title
                }
            },
            scales: {
                y: {
                    type: 'linear',
                    display: true,
                    position: 'left',
                },
                y1: {
                    type: 'linear',
                    display: true,
                    position: 'right',
                    grid: {
                        drawOnChartArea: false,
                    },
                },
            }
        },
    };
    const myChart = new Chart(
        document.getElementById('myChart'),
        config
    );
}
let Incoming = document.querySelectorAll(".InComming p");
let Complete = document.querySelectorAll(".Complete p");
let Cancel = document.querySelectorAll(".Cancel p");
let ListIncoming = [];
let ListComplete = [];
let ListCancel = [];
Incoming.forEach((e) => {
    ListIncoming.push(parseInt(e.id));
})
Complete.forEach((e) => {
    ListComplete.push(parseInt(e.id));
})
Cancel.forEach((e) => {
    ListCancel.push(parseInt(e.id));
})
console.log(ListComplete)
LoadChart(ListIncoming,ListComplete,ListCancel,"Booking")