let btnStatus = document.querySelectorAll(".Status");

btnStatus.forEach(s => {
    s.addEventListener('click', () => {
        let query = new QueryData("account/updateStatus");
        query.addParam("Status", s.textContent);
        query.addParam("AccountId", s.id);
        query.addEvent("load", function () {
            if (s.textContent == "Disable") {
                s.textContent = "Enable";
                s.className = "btn-enable Status";
            } else {
                s.textContent = "Disable";
                s.className = "btn-disable Status";
            }
        });
        query.send("PUT");
    });
});
