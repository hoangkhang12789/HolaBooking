let patt_password = /^(?=.{6,20}$)[a-zA-Z0-9]+$/;
let editUserNameBox = document.querySelector(".profile-edit .userName");
editUserNameBox.addEventListener("click", () => {
    let editBox = document.querySelector(".profile-edit-infor");
    editBox.style.display = "flex";

})
let emailEdit = document.querySelector(".emailEdit");
emailEdit.addEventListener("click", () => {
    let emailText = document.querySelector("#email");
    if (emailEdit.textContent == "Edit") {

        emailText.disabled = false;
        emailText.focus()
        emailEdit.innerText = "Save"
    } else {
        emailText.disabled = true;
        emailText.blur()
        emailEdit.innerText = "Edit"
        if (emailText.value.trim() != "") {
            let query = new QueryData("myAccount/updateEmail");
            query.addParam("mail", emailText.value);
            query.addEvent("load", function () {

            })
            query.send("PUT");
        } else {
            toast({
                title: 'Error',
                message: 'Email Not Null',
                type: 'error',
                duration: 3000
            });
        }
    }
})
let phoneEdit = document.querySelector(".phone");
phoneEdit.addEventListener("click", () => {
    let phoneText = document.querySelector("#phone");
    if (phoneEdit.textContent == "Edit") {

        phoneText.disabled = false;
        phoneText.focus()
        phoneEdit.innerText = "Save"
    } else {
        phoneText.disabled = true;
        phoneText.blur()
        phoneEdit.innerText = "Edit"
        if (phoneText.value.trim() != "") {
            let query = new QueryData("myAccount/updatePhone");
            query.addParam("phone", phoneText.value);
            query.addEvent("load", function () {

            })
            query.send("PUT");
        } else {
            toast({
                title: 'Error',
                message: 'Phone Not Null',
                type: 'error',
                duration: 3000
            });
        }
    }
})
let editPassBox = document.querySelector(".profile-edit .editPass");
editPassBox.addEventListener("click", () => {
    let editBox = document.querySelector(".profile-edit-pass");
    editBox.style.display = "flex";

})
let cancel = document.querySelectorAll(".cancel");
cancel.forEach((e) => {
    e.addEventListener("click", () => {
        e.parentNode.parentNode.style.display = "none";
    })
})
let saveInformationBtn = document.querySelector(".saveInformation");

saveInformationBtn.addEventListener("click", () => {
    let firstName = document.querySelector("#firstName");
    let lastName = document.querySelector("#lastName")
    if (firstName.value.trim() != "" && lastName.value.trim() != "") {
        let query = new QueryData("myAccount/updateFullName");
        query.addParam("firstName", firstName.value);
        query.addParam("lastName", lastName.value);
        query.addEvent("load", function () {

        })
        query.send("PUT");
        saveInformationBtn.parentNode.parentNode.style.display = "none";
        let fullName = document.querySelector(".profile-name p");
        let fullNameHeader = document.querySelector("#header-profile");
        fullNameHeader.innerText = `${firstName.value} ${lastName.value}`
        fullName.innerText = `${firstName.value} ${lastName.value}`
    } else {
        toast({
            title: 'Error',
            message: 'First Name Or Last Name Is Null',
            type: 'error',
            duration: 3000
        });
    }
})
let savePassBtn = document.querySelector("#savePass");
savePassBtn.addEventListener("click", () => {
    let newPass = document.querySelector("#newPassword")
    let repassword = document.querySelector("#RePassword");
    if (!patt_password.test(newPass.value)) {
        toast({
            title: 'Error',
            message: 'Password must only contains <span>a-z, A-Z, 0-9</span> with <span>first letter is a-z, A-Z</span> and is between <span>6 and 20 characters long</span>!',
            type: 'error',
            duration: 2000
        });
        newPass.value = "";
        repassword.value = "";
    } else if (repassword.value !== newPass.value) {
        toast({
            title: 'Error',
            message: 'Password must be <span>the same</span>!',
            type: 'error',
            duration: 2000
        });
        newPass.value = "";
        repassword.value = "";
    } else {
        let query = new QueryData("myAccount/updatePass");
        query.addParam("pass", newPass.value);
        query.addEvent("load", function () {
            document.querySelector(".cancelPass").parentNode.parentNode.style.display = "none";
            newPass.value = "";
            repassword.value = "";
        })
        query.send("PUT");
    }
})
let input = document.querySelector("#avatar");
let img = document.querySelector("#avatarImg");
input.addEventListener("change", () => {
   
    let filename = "holabooking" + Date.now() ;
    let query = new QueryData("myAccount/updateimage")
    let link = "/image/Profile/" + filename + ".png"
    query.addParam("img", link)
    query.addEvent("load", function () {
        let query1 = new QueryFileUpload("upload/image");
        query1.addParam("files", input.files[0]);
        query1.addParam("folder", "Profile");
        query1.addParam("fileName", filename);
        query1.addEvent('load', function () {
            setTimeout(() => {

                img.src = `/image/Profile/${filename}.png`;
            }, 3000)
        });
        query1.send("POST");
       
    })
    query.send("PUT")
    

})