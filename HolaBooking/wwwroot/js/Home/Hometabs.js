function openCity(evt, cityName) {
    // Declare all variables
    var i, hometabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    hometabcontent = document.getElementsByClassName("hometabcontent");
    for (i = 0; i < hometabcontent.length; i++) {
        hometabcontent[i].style.display = "none";
    }

    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(cityName).style.display = "grid";
    evt.currentTarget.className += " active";
}
document.getElementById("defaultOpen").click();

function openForm() {
    document.getElementById("myForm").style.display = "block";
  }
  
  function closeForm() {
    document.getElementById("myForm").style.display = "none";
  }

  var count = 1;
  var countEl = document.getElementById("count-people");
  function plus(){
      count++;
      countEl.value = count;
  }
  function minus(){
    if (count > 1) {
      count--;
      countEl.value = count;
    }  
}
function myReadMore() {
    var dots = document.getElementById("dotsreed");
    var moreText = document.getElementById("more");
    var btnText = document.getElementById("HomeBtn");

    if (dots.style.display === "none") {
        dots.style.display = "inline";
        btnText.innerHTML = "Read more";
        moreText.style.display = "none";
    } else {
        dots.style.display = "none";
        btnText.innerHTML = "Read less";
        moreText.style.display = "inline";
    }
}

    /*window.location.href = "https://localhost:7251/process/search/load";*/
let btn = document.querySelector("#SearchBoxClick")
btn.addEventListener("click", () => {
    let SearchText = document.querySelector(".search-text").value
    let CheckIn = document.querySelector(".search-date-one")
    let CheckOut = document.querySelector(".search-date-two")
    let ChoseValue = document.querySelector("#choice-room-mate").value
    let NumberOfPeople = document.querySelector("#count-people")


    window.location.assign("/search?Name=" + SearchText+ "&roomtype=" + ChoseValue  );

})
function MyCloseButton() {
    document.getElementById("home-dear-box-popup-hide").style.display = "none";
}

let listHotel = document.querySelectorAll(".home-top-hotel-item");
listHotel.forEach((e) => {
    e.addEventListener("click", () => {

        window.location = `/hotel/view?ID=${e.id}`
    })

})
let AddressSelector = document.querySelectorAll(".location-image")
AddressSelector.forEach((e) => {
    e.addEventListener("click", () => {
        let name = e.querySelector(".Address-Name").textContent
        let searchtext = e.querySelector(".Address-Name").textContent;
        let searchroom = document.querySelector("#choice-room-mate").value;
        window.location.assign("/search?Name=" + searchtext.trim() + "&roomtype=" + 5);
    })
})



let Address = document.querySelectorAll(".would-image")
Address.forEach((e) => {
    e.addEventListener("click", () => {
       
        let searchtext = e.querySelector(".would-image-title").textContent;

        window.location.assign("/search?Name=" + searchtext.trim() + "&roomtype=" + 5);
    })
})




