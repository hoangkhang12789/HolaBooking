﻿$(document).ready(function () {
    
        $('.home-dear-slider').slick({
            infinite: true,
            slidesToShow: 2,
            autoplay: true,
            autoplaySpeed: 2000,
            arrows: true,
            variableWidth: true,
            prevArrow: `<button type='button' class='slick-prevx slick-arrowx'><ion-icon name="arrow-back-outline"></ion-icon></button>`,
            nextArrow: `<button type='button' class='slick-nextx slick-arrowx'><ion-icon name="arrow-forward-outline"></ion-icon></button>`,
            dots: true,
        });

    $('.home-dear-location-image').slick({
            infinite: false,
            slidesToShow: 6,
            slidesToScroll: 1,
            arrows: true,
            variableWidth: true,
            prevArrow: `<button type='button' class='slick-prevs slick-arrows'><ion-icon name="chevron-back-outline"></ion-icon></button>`,
            nextArrow: `<button type='button' class='slick-nexts slick-arrows'><ion-icon name="chevron-forward-outline"></ion-icon></button>`,
        })
        $('.home-dear-would-image').slick({
            infinite: false,
            slidesToShow: 6,
            arrows: true,
            variableWidth: true,
            prevArrow: `<button type='button' class='slick-prevm slick-arrowm'><ion-icon name="chevron-back-outline"></ion-icon></button>`,
            nextArrow: `<button type='button' class='slick-nextm slick-arrowm'><ion-icon name="chevron-forward-outline"></ion-icon></button>`,
        })

});





