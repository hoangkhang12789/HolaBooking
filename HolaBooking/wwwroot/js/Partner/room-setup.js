$('#imageInput').on('change', function() {
	$input = $(this);
	if($input.val().length > 0) {
		fileReader = new FileReader();
		fileReader.onload = function (data) {
		$('.image-preview').attr('src', data.target.result);
		}
		fileReader.readAsDataURL($input.prop('files')[0]);
		$('.image-button').css('display', 'none');
		$('.image-preview').css('display', 'block');
		$('.change-image').css('display', 'block');
	}
});
						
$('.change-image').on('click', function() {
	$control = $(this);			
	$('#imageInput').val('');	
	$preview = $('.image-preview');
	$preview.attr('src', '');
	$preview.css('display', 'none');
	$control.css('display', 'none');
	$('.image-button').css('display', 'block');
});
async function loadMap(address) {
	let data = await convertAddress(address);
	let hotelName = document.querySelector(".HotelName");
	let img = document.querySelector("#ImgPreview")
	let hotelStar = document.querySelector(".HotelStar");
	initMap(data.lat, data.lng, hotelName.value, img.src, hotelStar.value, 10, "Exceptional")
	let latAndlongtide = document.querySelector(".latAndlongtide");
	latAndlongtide.setAttribute("lat", data.lat)
	latAndlongtide.setAttribute("lng", data.lng)
}
let loadMapBtn = document.querySelector(".loadMap")
loadMapBtn.addEventListener("click", () => {
	let addresss = document.querySelector(".AddressName");

	let countryName = document.querySelector(".CountryName");
	let cityName = document.querySelector(".CityName");
	let districtName = document.querySelector(".DistrictName");
	let address = document.querySelector(".mapAddress");
	address.style.display = "block"
	address.textContent = addresss.value + "," + districtName.value + "," + cityName.value + "," + countryName.value
	loadMap(address.textContent)
})
let currentBtn = document.querySelector(".currentMap");
currentBtn.addEventListener("click", () => {
	let hotelName = document.querySelector(".HotelName");
	let img = document.querySelector("#ImgPreview")
	let hotelStar = document.querySelector(".HotelStar");
	getCurrentLocationMap(hotelName.value, img.src, hotelStar.value)
})
