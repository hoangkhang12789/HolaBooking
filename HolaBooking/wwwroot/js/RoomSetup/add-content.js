
function addnewContentfacilities() {
    const main = document.getElementById('content-setting-service-facilities');
    if (main) {
        const selectSearch = document.createElement('div');
        selectSearch.onclick = function (e) {
            if (e.target.closest('.remove-content')) {
                main.removeChild(selectSearch);
            }
        }
        selectSearch.classList.add('content-setting-addnew');
        selectSearch.innerHTML = `
        <div class="content-setting-addnew">
        <div class=" selectSearch">
            <datalist id="FacilitiesName">
                <option value="24-hour front desk">
                <option value="Laundry">
                <option value="Lift">
                <option value="1 swimming pool">
                <option value="Free WiFi">
                <option value="Non-smoking rooms">
                <option value="Fitness centre">
                <option value="Bar">
                <option value="Free parking">
                <option value="Terrace">
                <option value="Restaurant">
                <option value="Complimentary tea">
            </datalist>
            <input autoComplete="on" list="FacilitiesName" placeholder="Add Facilities ......"
                class="CountryName" />
            <button class="remove-content"> <i
                    class="fal fa-times"></i></button>

        </div>
    </div>`;
        main.appendChild(selectSearch);
    }
}

let DeviceList = document.querySelectorAll(".DeviceItem");
let d = 0;
function addnewContentDevice() {
    const main = document.getElementById('content-setting-service-device');
    if (main) {
        const selectSearch = document.createElement('div');
        selectSearch.onclick = function (e) {
            if (e.target.closest('.remove-content')) {
                main.removeChild(selectSearch);
            }
        }
        selectSearch.classList.add('content-setting-addnew');
        selectSearch.innerHTML = `                    
        <div class="content-setting-addnew">
        <div class=" selectSearch">
        <datalist id="DeviceName">
            
        </datalist>
            <input autoComplete="on" list="DeviceName" placeholder="Choose Device......" class="DeviceName" />
            <button class="remove-content"><i
                    class="fal fa-times"></i></button>
        
        </div>
    </div>`;
        main.appendChild(selectSearch);
        if (d == 0) {
            const elm = document.getElementById('DeviceName');
            var newElement = document.createElement('option');
            DeviceList.forEach((d) => {
                newElement.innerHTML += '<option data-value="' + d.id + '" value="' + d.textContent + '">';
            });
            elm.appendChild(newElement);
            d++;
        }
    }
}

let SeviceList = document.querySelectorAll(".ServiceItem");
let s = 0;
function addnewContentSevice() {
    
    const main = document.getElementById('content-setting-service');
    if (main) {
        const selectSearch = document.createElement('div');
        selectSearch.onclick = function (e) {
            if (e.target.closest('.remove-content')) {
                main.removeChild(selectSearch);
            }
        }
        selectSearch.classList.add('content-setting-addnew');
        selectSearch.innerHTML = `                    
        <div class="content-setting-addnew">
        <div class="selectSearch Service-Option">
        <datalist id="ServiceName">
            
        </datalist>
        <input autoComplete="on" list="ServiceName" placeholder="Choose Service......"
        class="SeviceName" />
        <input type="number" class="ServicePrice" placeholder="Price USD.."/>
        <label class="input-group-text" for="inputGroupSelect01"> 
        <select id="ServiceDefault">
                            <option value="0">Default</option>
                            <option value="1">Option</option>
        </select></label>
            <button class="remove-content"> <i
                    class="fal fa-times"></i></button>
        
        </div>
    </div>`;
        main.appendChild(selectSearch);
        if (s == 0) {
            const elm = document.getElementById('ServiceName');
            var newElement = document.createElement('option');
            SeviceList.forEach((s) => {
                newElement.innerHTML += '<option data-value="' + s.id + '" value="' + s.textContent + '">';
            });
            elm.appendChild(newElement);
            s++;
        }
        seviceOptionLoad();
    }
}

let loadRemoveBtn = document.querySelectorAll(".remove-content");
loadRemoveBtn.forEach(l => {
    l.addEventListener("click", () => {
        l.parentNode.parentNode.parentNode.parentNode.removeChild(l.parentNode.parentNode.parentNode);
    });
});

