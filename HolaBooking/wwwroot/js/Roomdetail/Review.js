﻿let editComment = document.querySelectorAll(".edit-comment-list p")
let allLisstCL = document.querySelectorAll(".cl")
let allLisstFa = document.querySelectorAll(".fan")
let allLisstLo = document.querySelectorAll(".lo")
let allLisstSe = document.querySelectorAll(".se")
function loadEditAndDeleteBtn() {
    if (editComment.length != 0) {
        editComment[0].addEventListener("click", () => {
            let parentBox = editComment[0].parentNode.parentNode.parentNode.parentNode;
            let point = parentBox.querySelectorAll(".comment-point-info .user-poin");
            let mainPoint = document.querySelectorAll(".review-create .choose-poin >.rating> .poin");
            let userComment = document.querySelector("#user-comment");
            let comment = document.querySelector(".comment-user-content p");
            mainPoint.forEach((element, index) => {
                element.innerHTML = parseInt(point[index].textContent);
                parentBox.remove();
            })
            let content = comment.textContent;
            userComment.value = content;

        })
        //Delete Review
        editComment[1].addEventListener("click", () => {
            let query = new QueryData("roomdetail/delete");
           
            query.addParam("hotelID", editComment[1].getAttribute("code"));
            query.addParam("ReviewID", editComment[1].id);
            query.addEvent("load", function () {
                let query2 = new QueryData("roomdetail/point", "json");
                query2.addParam("hotelID", commentBtn.id);
                query2.addEvent("load", function () {
                    resetReviewCount()
                    let responseJson = JSON.parse(this.response)
                    let item1 = responseJson[0];
                    let item2 = responseJson[1];
                    let item3 = responseJson[2];
                    let item4 = responseJson[3];
                    let point = responseJson[4];
                    hotelPoint.forEach((e) => {
                        e.textContent = point;
                    })
                    allLisstCL.forEach(e => {
                        e.textContent = item1;
                        if (!e.classList.contains("noprogress")) {
                            e.parentNode.parentNode.querySelector("#file").value = item1;
                        }

                    })
                    allLisstFa.forEach(e => {
                        e.textContent = item2;
                        if (!e.classList.contains("noprogress")) {
                            e.parentNode.parentNode.querySelector("#file").value = item2;
                        }
                    })
                    allLisstLo.forEach(e => {
                        e.textContent = item3;
                        if (!e.classList.contains("noprogress")) {
                            e.parentNode.parentNode.querySelector("#file").value = item3
                        }
                    })
                    allLisstSe.forEach(e => {
                        e.textContent = item4;
                        if (!e.classList.contains("noprogress")) {
                            e.parentNode.parentNode.querySelector("#file").value = item4
                        }
                    })

                })
                query2.send("GET");
            })
            query.send("DELETE");
            editComment[1].parentNode.parentNode.parentNode.parentNode.remove();
        })
    }
}
let commentContent = document.querySelector("#user-comment");
let point = document.querySelectorAll(".choose-poin >.rating >.poin");
let commentBtn = document.querySelector(".comment-user-submit button");
let reviewBox = document.querySelector(".review-list-comment");
let hotelPoint = document.querySelectorAll(".HotelPoint");


  
    commentBtn.addEventListener("click", () => {
        if (commentContent.value.trim() != "") {
            let html;

            let query = new QueryData("roomdetail/review");
            query.addParam("hotelID", commentBtn.id);
            query.addParam("Text", commentContent.value);
            query.addParam("Cleanliness", point[0].textContent);
            query.addParam("Facilitiesss", point[1].textContent);
            query.addParam("Location", point[2].textContent);
            query.addParam("Service", point[3].textContent);
            query.addEvent("load", function () {
                html = this.response;
                reviewBox.innerHTML = html;
                editComment = document.querySelectorAll(".edit-comment-list p")
                loadEditAndDeleteBtn()
                let query2 = new QueryData("roomdetail/point", "json");
                query2.addParam("hotelID", commentBtn.id);
                query2.addEvent("load", function () {
                    resetReviewCount()
                    let responseJson = JSON.parse(this.response)
                    let item1 = responseJson[0];
                    let item2 = responseJson[1];
                    let item3 = responseJson[2];
                    let item4 = responseJson[3];
                    let point = responseJson[4];
                    hotelPoint.forEach((e) => {
                        e.textContent = point;
                    })
                    allLisstCL.forEach(e => {
                        e.textContent = item1;
                        if (!e.classList.contains("noprogress")) {
                            e.parentNode.parentNode.querySelector("#file").value = item1;
                        }

                    })
                    allLisstFa.forEach(e => {
                        e.textContent = item2;
                        if (!e.classList.contains("noprogress")) {
                            e.parentNode.parentNode.querySelector("#file").value = item2;
                        }
                    })
                    allLisstLo.forEach(e => {
                        e.textContent = item3;
                        if (!e.classList.contains("noprogress")) {
                            e.parentNode.parentNode.querySelector("#file").value = item3
                        }
                    })
                    allLisstSe.forEach(e => {
                        e.textContent = item4;
                        if (!e.classList.contains("noprogress")) {
                            e.parentNode.parentNode.querySelector("#file").value = item4
                        }
                    })

                })
                query2.send("GET");
            })
            query.send("GET");
            resetReview()
           
        }
    })

function resetReview() {
    commentContent.value = "";
    point.forEach((e) => {
        e.textContent = 10;
    })
}
function resetReviewCount() {
    let reviewCount = document.querySelectorAll(".rating-count p")
    let numberOfReview = document.querySelectorAll(".list-comment-item");
    reviewCount.forEach(e => {
        e.textContent = numberOfReview.length;
    })
}
loadEditAndDeleteBtn()