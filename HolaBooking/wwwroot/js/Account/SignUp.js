﻿let patt_username = /^(?=.{6,20}$)[a-z0-9]+$/;
let patt_password = /^(?=.{6,20}$)[a-zA-Z0-9]+$/;

let signupUsername = document.querySelector(".sign-up-username");
let signupPassword = document.querySelector(".sign-up-password");
let signupRepassword = document.querySelector(".sign-up-repassword");
let signupBtn = document.querySelector(".btn-sign");
let checkBox = document.getElementById("check");

signupBtn.addEventListener("click", () => {
    if (!patt_username.test(signupUsername.value)) {
        toast({
            title: 'Error',
            message: 'Username must only contains <span>a-z, 0-9</span> with <span>first letter is a-z</span> and is between <span>6 and 20 characters long</span>!',
            type: 'error',
            duration: 2000
        });
        reset();
    } else if (!patt_password.test(signupPassword.value)) {
        toast({
            title: 'Error',
            message: 'Password must only contains <span>a-z, A-Z, 0-9</span> with <span>first letter is a-z, A-Z</span> and is between <span>6 and 20 characters long</span>!',
            type: 'error',
            duration: 2000
        });
        reset();
    } else if (signupRepassword.value !== signupPassword.value) {
        toast({
            title: 'Error',
            message: 'Password must be <span>the same</span>!',
            type: 'error',
            duration: 2000
        });
        reset();
    }
    else if (!checkBox.checked) {
        toast({
            title: 'Error',
            message: 'You must agree to all of our terms',
            type: 'error',
            duration: 2000
        });

    } else {
        let query = new QueryData("account/checkUserName")
        query.addParam("userName", signupUsername.value);
        query.addEvent("load", function () {
            if (this.response === "false") {
                let query1 = new QueryData("account/createAccount")
                query1.addParam("userName", signupUsername.value);
                query1.addParam("password", signupPassword.value);
                query1.send("POST")
                toast({
                    title: 'Success',
                    message: 'Create New Account Success',
                    type: 'success',
                    duration: 2000
                });
                reset();
                setTimeout(() => { window.location = "/login" }, 2000)
            } else {
                toast({
                    title: 'Error',
                    message: 'Account already exists',
                    type: 'error',
                    duration: 2000
                });
                reset();
            }
        })
        query.send("GET");
    }
})

signupUsername.addEventListener("keyup", checkUsername);
signupPassword.addEventListener("keyup", checkPassword);
signupRepassword.addEventListener("keyup", checkConfirmPassword);



function checkUsername() {

    if (signupUsername.value == "") {
        signupUsername.parentNode.parentNode.classList.remove("success");
        signupUsername.parentNode.parentNode.classList.remove("error");
    } else if (patt_username.test(signupUsername.value)) {
        signupUsername.parentNode.parentNode.classList.add("success");
        signupUsername.parentNode.parentNode.classList.remove("error");
    } else {
        signupUsername.parentNode.parentNode.classList.remove("success");
        signupUsername.parentNode.parentNode.classList.add("error");
    }
}

function checkPassword() {
    if (signupPassword.value == "") {
        signupPassword.parentNode.parentNode.classList.remove("success");
        signupPassword.parentNode.parentNode.classList.remove("error");
    } else if (patt_password.test(signupPassword.value)) {
        signupPassword.parentNode.parentNode.classList.add("success");
        signupPassword.parentNode.parentNode.classList.remove("error");
    } else {
        signupPassword.parentNode.parentNode.classList.remove("success");
        signupPassword.parentNode.parentNode.classList.add("error");
    }
}

function checkConfirmPassword() {
    if (signupRepassword.value == "") {
        signupRepassword.parentNode.parentNode.classList.remove("success");
        signupRepassword.classList.remove("error");
    } else if (patt_password.test(signupRepassword.value)) {
        signupRepassword.parentNode.parentNode.classList.add("success");
        signupRepassword.parentNode.parentNode.classList.remove("error");
    } else {
        signupRepassword.parentNode.parentNode.classList.remove("success");
        signupRepassword.parentNode.parentNode.classList.add("error");
    }
}
function reset() {
    signupUsername.value = "";
    signupPassword.value = "";
    signupRepassword.value = "";
    checkBox.checked = false;

}
const inputs = document.querySelectorAll(".input");

function addcl() {
    let parent = this.parentNode.parentNode;
    parent.classList.add("focus");
}

function remcl() {
    let parent = this.parentNode.parentNode;
    if (this.value == "") {
        parent.classList.remove("focus");
    }
}


inputs.forEach(input => {
    input.addEventListener("focus", addcl);
    input.addEventListener("blur", remcl);
});

