

const inputs = document.querySelectorAll(".input");
const userNameLogin = document.querySelector(".login-user");
const passwordLogin = document.querySelector(".login-pass");
const loginBtn = document.querySelector(".btn-login");
const checkBox = document.querySelector("#check");
loginBtn.addEventListener("click", () => {
    let query = new QueryData("account/signin");
    query.addParam("userName", userNameLogin.value);
    query.addParam("password", passwordLogin.value);
    if (checkBox.checked) {
        query.addParam("check", "true");
    } else {
        query.addParam("check", "false");
    }
    query.addEvent("load", function () {
        if (this.response === "true") {
            toast({
                title: 'Success',
                message: 'Login successfully',
                type: 'success',
                duration: 3000
            });
            setTimeout(() => { window.location = "/" }, 2000)
        }
        else {
            toast({
                title: 'Error',
                message: 'Account or password is incorrect',
                type: 'error',
                duration: 3000
            });
            userNameLogin.value = "";
            passwordLogin.value = "";
        }
    })

    query.send("GET");

})
function addcl() {
    let parent = this.parentNode.parentNode;
    parent.classList.add("focus");
}

function remcl() {
    let parent = this.parentNode.parentNode;
    if (this.value == "") {
        parent.classList.remove("focus");
    }
}


inputs.forEach(input => {
    input.addEventListener("focus", addcl);
    input.addEventListener("blur", remcl);
});

