
var headers = new Headers();
headers.append("X-CSCAPI-KEY", "NDFZcGc5R3g5UzloV0xMYk5jeWZCY2FPWWpNMDUwNk95SDI3bVFOQg==");

var requestOptions = {
    method: 'GET',
    headers: headers,
    redirect: 'follow'
};
function getCurrentLocationMap(hotelName, hotelImg, hotelStar) {
    let lat = 0;
    let long = 0;
    window.navigator.geolocation.getCurrentPosition(function (location) {
        lat = location.coords.latitude;
        long = location.coords.longitude;
        let latAndlongtide = document.querySelector(".latAndlongtide");
        latAndlongtide.setAttribute("lat", lat)
        latAndlongtide.setAttribute("lng", long)
        initMap(lat, long, hotelName, hotelImg, hotelStar, 10, "Exceptional");
    })

}
function getCurrentLocation() {
    let lat = 0;
    let long = 0;
    window.navigator.geolocation.getCurrentPosition(function (location) {
        lat = location.coords.latitude;
        long = location.coords.longitude;
        getCountryName(lat, long);
  })

}
async function getCountryName(lat, long) {
    let latLong = new google.maps.LatLng(lat, long);;
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'latLng': latLong }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            if (results[0]) {
                loc = getCountry(results);
                getCountries(loc)

            }
        }
    });

    function getCountry(results) {
        for (var i = 0; i < results[0].address_components.length; i++) {
            var shortName = results[0].address_components[i].short_name;
            var longName = results[0].address_components[i].long_name;
            var type = results[0].address_components[i].types;
            if (type.indexOf("country") != -1) {
                if (!isNullOrWhitespace(shortName)) {
                    return shortName;
                }
                else {
                    return longName;
                }
            }
        }

    }

    function isNullOrWhitespace(text) {
        if (text == null) {
            return true;
        }
        return text.replace(/\s/gi, '').length < 1;
    }
    async function getCountries(ID) {
        console.log(ID)
        let dataCountries = await fetch("https://api.countrystatecity.in/v1/countries", requestOptions)
            .then(response => response.json())
            .then(result => result)
            .catch(error => console.log('error', error));
        dataCountries.forEach((e) => {
             
            if (e.iso2 == ID) {
                document.cookie = `location = ${e.name}`;
                window.location="/"
            }


        })
    };
}