let dropDown = document.querySelector(".fa-caret-down");
if (dropDown != null) {
    dropDown.addEventListener("click", () => {
        let myAccount = document.querySelector(".header-drop-down");
        myAccount.style.display = "flex";
    })
}
let logOutBtn = document.querySelector(".sign-out button");
if (logOutBtn != null) {
    logOutBtn.addEventListener("click", () => {
        let query = new QueryData("account/logout");
        query.addEvent("load", function () {
            window.location = "/"
        })
        query.send("GET");


    })
}
