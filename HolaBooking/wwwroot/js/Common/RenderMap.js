

let mapContainer = document.getElementById("map");


async function convertAddress(address) {
  let data = await fetch(`https://maps.googleapis.com/maps/api/geocode/json?address=${address}&key=AIzaSyA8wBXD1LVs-OQ7S0ZzLnzQ1bhBmqZkUyA`)
    .then(res => res.json());
  if (data.status == "OK") {
    let latLng = data.results[0].geometry.location;
    return latLng;
  }
}


async function initMap(lat, long, hotelName, hotelImg, hotelStar, hotelPoint, hotelPointName) {
    let star = "";
    for (let i = 0; i < hotelStar; i++) {
        star += `<i class="fad fa-star"></i>`
    }
    const contentString = `<div class="map-display">
<div class="map-display-left">
  <img src="${hotelImg}"
    alt="">
</div>
<div class="map-display-right">
  <span>${hotelName}</span>
  <div class="hotelPoint">
    <div class="hotelStar">
   ${star}
     <p>|</p>
    </div>
    <div>
 
      <p> ${hotelPoint}  ${hotelPointName}</p>
    </div>
  </div>
  
</div>
</div>`
    

  const infoWindow = new google.maps.InfoWindow({
    content: contentString,
  });
  map = new google.maps.Map(document.getElementById("map"), {
    center: { lat: lat, lng: long },
    zoom: 15,
  });
  window.setTimeout(() => {
    const marker = new google.maps.Marker({
      position: { lat: lat, lng: long },
      map: map,
      animation: google.maps.Animation.DROP
    });
    
    marker.addListener("click", () => {
      infoWindow.open({
        anchor: marker,
        map,
        shouldFocus: false,
      });
        
    });

  }, 1000)

}
