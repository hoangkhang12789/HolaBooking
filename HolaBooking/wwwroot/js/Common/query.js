class QueryData {
    constructor(path, type) {
        this.xhr = new XMLHttpRequest();
        this.params = new URLSearchParams();
        this.xhr.responseType = type || "";
        this.link = `/process/${path}`;
    }

    addEvent(eventType, func, ...args) {
        this.xhr.addEventListener(eventType, func, args);
    }

    addParam(key, value) {
        this.params.append(key, value);
    }

    send(method) {
        if (method === "GET") {
            this.xhr.open(method, `${this.link}?${this.params.toString()}`);
            this.xhr.send();
        } else if (method === "POST") {
            this.xhr.open(method, this.link);
            this.xhr.send(this.params);
        } else if (method === "PUT") {
            this.xhr.open(method, this.link);
            this.xhr.send(this.params);
        } else if (method === "DELETE") {
            this.xhr.open(method, this.link);
            this.xhr.send(this.params);
        } else {
            throw "Only accept GET,POST,PUT and DELETE";
        }
    }
}

class QueryFileUpload {
    constructor(path) {
        this.xhr = new XMLHttpRequest();
        this.params = new FormData();
        this.link = `/file/${path}`;
    }

    addEvent(eventType, func, ...args) {
        this.xhr.upload.addEventListener(eventType, func, args);
    }

    addParam(key, value) {
        this.params.append(key, value);
    }

    send() {
        this.xhr.open("POST", this.link);
        this.xhr.send(this.params);
    }
}
