﻿
function toast({ title = '', message = '', type = 'info ', duration = 3000 }) {
	const main = document.getElementById('toast');
	if (main) {

		const toast = document.createElement('div');
		const remotimeout = setTimeout(function () {
			main.removeChild(toast);
		}, duration)

		toast.onclick = function (e) {
			if (e.target.closest('.toast__close')) {
				main.removeChild(toast);
				clearTimeout(remotimeout);
			}
		}

		const icons = {
			success: 'far fa-check-circle',
			info: 'fas fa-info',
			error: 'fas fa-exclamation-triangle',
			warning: 'fas fa-exclamation',
		};
		const iconcheck = icons[type];
		const delay = (duration / 1000).toFixed(2);
		toast.classList.add('toastMess', `toast--${type}`);
		toast.style.animation = `msgInleft ease .3s, closemsg linear 1s ${delay}s forwards`;
		toast.innerHTML = `
			<div class="toast__icon">
				<i class="${iconcheck}"></i>
			</div>
			<div class="toast__body">
				<h3 class="toast__title">
					${title}
				</h3>
				<p class="toast__msg">
					${message}
				</p>
			</div>
			<div class="toast__close">
				<i class="fal fa-times"></i>
			</div>
			`;
		main.appendChild(toast);
	}
}

