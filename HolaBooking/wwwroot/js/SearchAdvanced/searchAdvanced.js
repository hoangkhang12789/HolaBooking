function showMoreLessButton(name) {
    var type = name + "";
    var dots = document.getElementById("dots" + type);
    var moreText = document.getElementById("more" + type);
    var btnText = document.getElementById("myBtn" + type);

    if (dots.style.display === "none") {
        dots.style.display = "inline";
        btnText.innerHTML = "Show more <i class=\"fas fa-sort-down\"></i>";
        moreText.style.display = "none";
    } else {
        dots.style.display = "none";
        btnText.innerHTML = "Show less <i class=\"fas fa-sort-up\"></i>";
        moreText.style.display = "inline";
    }
}
function openForm() {
    document.getElementById("myForm").style.display = "block";
}

function closeForm() {
    document.getElementById("myForm").style.display = "none";
}

var count = 1;
var countEl = document.getElementById("count-people");
function plus() {
    count++;
    countEl.value = count;
}
function minus() {
    if (count > 1) {
        count--;
        countEl.value = count;
    }
}


let applyBtn = document.querySelectorAll(".apply-price");
let listHotel = document.querySelector(".content-box");
let listPage = document.querySelector(".pagination");
let pagebtn = document.querySelectorAll(".page-link");
let pageitem = document.querySelectorAll(".page-item");
let maxPage = document.querySelector(".maxPage").id;
var nowpage;

//===================filter==================================================
applyBtn.forEach(f => {
    f.addEventListener("click", () => {
        let query = new QueryData("search/filter");
        let minPrice = document.querySelector(".min-price").value;
        let maxPrice = document.querySelector(".max-price").value;
        let starsflr = document.querySelectorAll("#star-filter");
        let filter = [];
        filter.push(minPrice);
        filter.push(maxPrice);
        starsflr.forEach((e) => {
            if (e.checked) {
                filter.push(e.value);
            }
            else {
                filter.push(null);
            }
        });
        for (var i = 0; i < filter.length; i++) {
            query.addParam("filter", filter[i]);
        }
        query.addParam("name", document.querySelector(".seachName").id);
        query.addParam("roomtype", document.querySelector("#roomTypeID").value);
        query.addEvent("load", function () {
            listHotel.innerHTML = this.response;
            //----------note--------------
            if (document.querySelector(".maxPagef") != null) {
                maxPage = document.querySelector(".maxPagef").id;
                let maxPagef = document.querySelector(".maxPagef").id;
                listPage.innerHTML = '';
                listPage.innerHTML += ' <li class="page-item"> <a href="#" class="page-link" id="pre" value="0">Previous</a> </li>';
                for (var i = 1; i <= maxPagef; i++) {
                    if (i == 1) {
                        listPage.innerHTML += '<li class="page-item active"><a href="#" class="page-link">' + i + '</a></li>';
                    } else {
                        listPage.innerHTML += '<li class="page-item"><a href="#" class="page-link">' + i + '</a></li>';

                    }
                }
                listPage.innerHTML += ' <li class="page-item"><a href="#" class="page-link" id="nex" value="1">Next</a></li>';
            } else {
                listPage.innerHTML = '';
            }
            listHotel = document.querySelector(".content-box");
            listPage = document.querySelector(".pagination");
            pagebtn = document.querySelectorAll(".page-link");
            pageitem = document.querySelectorAll(".page-item");
            nowpage = 1;
            ReLoadEventViewHotel();
            ReLoadEventPage();
        });
        query.send("GET");
    });
});

//===================paging==================================================

function ReLoadEventPage() {
    pagebtn.forEach(e => {
        e.addEventListener("click", () => {
            let query = new QueryData("search/page");
            pageitem.forEach(p => {
                if (p.classList.contains("active")) {
                    nowpage = p.textContent;
                    //console.log(nowpage);
                    //if (nowpage == maxPage) {
                    //    console.log("Adhu");
                    //}
                }
            });
            if (e.textContent.charAt(0) == 'N') {
                if (nowpage == maxPage) {
                    e.textContent = maxPage;
                } else {
                    e.textContent = ++nowpage;
                }
                nowpage = -1;
            }
            if (e.textContent.charAt(0) == 'P') {
                if (nowpage == 1) {
                    e.textContent = 1;
                } else {
                    e.textContent = --nowpage;
                }
                nowpage = 0;
            }
            let minPrice = document.querySelector(".min-price").value;
            let maxPrice = document.querySelector(".max-price").value;
            let starsflr = document.querySelectorAll("#star-filter");
            let filter = [];
            filter.push(minPrice);
            filter.push(maxPrice);
            starsflr.forEach((e) => {
                if (e.checked) {
                    filter.push(e.value);
                }
                else {
                    filter.push(null);
                }
            });
            for (var i = 0; i < filter.length; i++) {
                query.addParam("filter", filter[i]);
            }
            query.addParam("page", e.textContent);
            query.addParam("name", document.querySelector(".seachName").id);
            query.addParam("roomtype", document.querySelector("#roomTypeID").value);
            query.addEvent("load", function () {
                pageitem.forEach(p => {
                    p.classList.remove("active");
                });
                listHotel.innerHTML = this.response;
                if (nowpage == 0) {
                    pagebtn.item(e.textContent).parentNode.classList.add("active");
                    e.textContent = "Previous";
                } else if (nowpage == -1) {
                    pagebtn.item(e.textContent).parentNode.classList.add("active");
                    e.textContent = "Next";
                } else {
                    e.parentNode.classList.add("active");
                }
                ReLoadEventViewHotel();
            });
            query.send("GET");
        });
    });
}

function ReLoadEventViewHotel() {
    let ViewHotelBtn = document.querySelectorAll(".booking-bnt");
    ViewHotelBtn.forEach(e => {
        e.addEventListener("click", () => {
            window.location = "/hotel/view?ID=" + e.id
        })
    })
}

ReLoadEventViewHotel();
ReLoadEventPage();