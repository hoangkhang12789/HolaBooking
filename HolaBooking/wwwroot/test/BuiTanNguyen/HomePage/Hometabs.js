function openCity(evt, cityName) {
    // Declare all variables
    var i, hometabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    hometabcontent = document.getElementsByClassName("hometabcontent");
    for (i = 0; i < hometabcontent.length; i++) {
        hometabcontent[i].style.display = "none";
    }

    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(cityName).style.display = "grid";
    evt.currentTarget.className += " active";
}


function openForm() {
    document.getElementById("myForm").style.display = "block";
  }
  
  function closeForm() {
    document.getElementById("myForm").style.display = "none";
  }

  var count = 1;
  var countEl = document.getElementById("count-people");
  function plus(){
      count++;
      countEl.value = count;
  }
  function minus(){
    if (count > 1) {
      count--;
      countEl.value = count;
    }  
}
function myReadMore() {
    var dots = document.getElementById("dotsreed");
    var moreText = document.getElementById("more");
    var btnText = document.getElementById("HomeBtn");

    if (dots.style.display === "none") {
        dots.style.display = "inline";
        btnText.innerHTML = "Read more";
        moreText.style.display = "none";
    } else {
        dots.style.display = "none";
        btnText.innerHTML = "Read less";
        moreText.style.display = "inline";
    }
}

    /*window.location.href = "https://localhost:7251/process/search/load";*/
let btn = document.querySelector("#SearchBoxClick")
btn.addEventListener("click", () => {
    let a = document.querySelector(".search-text")
    let b = document.querySelector(".search-date-one")
    let c = document.querySelector(".search-date-two")
    let d = document.querySelector("#choice-room-mate")
    let e = document.querySelector("#count-people")


    window.location.replace("https://localhost:7251/process/search/load?");

})
function MyCloseButton() {
    document.getElementById("home-dear-box-popup-hide").style.display = "none";
    }




