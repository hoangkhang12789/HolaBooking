function showMoreLessButton(name) {
    var type = name+"";
    var dots = document.getElementById("dots"+type);
    var moreText = document.getElementById("more"+type);
    var btnText = document.getElementById("myBtn"+type);
  
    if (dots.style.display === "none") {
      dots.style.display = "inline";
      btnText.innerHTML = "Show more <i class=\"fas fa-sort-down\"></i>"; 
      moreText.style.display = "none";
    } else {
      dots.style.display = "none";
      btnText.innerHTML = "Show less <i class=\"fas fa-sort-up\"></i>"; 
      moreText.style.display = "inline";
    }
  }
  function openForm() {
    document.getElementById("myForm").style.display = "block";
  }
  
  function closeForm() {
    document.getElementById("myForm").style.display = "none";
  }

  var count = 1;
  var countEl = document.getElementById("count-people");
  function plus(){
      count++;
      countEl.value = count;
  }
  function minus(){
    if (count > 1) {
      count--;
      countEl.value = count;
    }  
  }