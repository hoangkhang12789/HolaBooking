
function addnewContentfacilities() {
    const main = document.getElementById('content-setting-service-facilities');
    if (main) {
        const selectSearch = document.createElement('div');
        selectSearch.onclick = function (e) {
            if (e.target.closest('.remove-content')) {
                main.removeChild(selectSearch);
            }
        }
        selectSearch.classList.add('content-setting-addnew');
        selectSearch.innerHTML = `
        <div class="content-setting-addnew">
        <div class=" selectSearch">
            <datalist id="FacilitiesName">
                <option value="24-hour front desk">
                <option value="Laundry">
                <option value="Lift">
                <option value="1 swimming pool">
                <option value="Free WiFi">
                <option value="Non-smoking rooms">
                <option value="Fitness centre">
                <option value="Bar">
                <option value="Free parking">
                <option value="Terrace">
                <option value="Restaurant">
                <option value="Complimentary tea">
            </datalist>
            <input autoComplete="on" list="FacilitiesName" placeholder="Choose Facilities ......"
                class="CountryName" />
            <button class="remove-content"> <i
                    class="fal fa-times"></i></button>

        </div>
    </div>`;
        main.appendChild(selectSearch);
    }
}

function addnewContentDevice() {
    const main = document.getElementById('content-setting-service-device');
    if (main) {
        const selectSearch = document.createElement('div');
        selectSearch.onclick = function (e) {
            if (e.target.closest('.remove-content')) {
                main.removeChild(selectSearch);
            }
        }
        selectSearch.classList.add('content-setting-addnew');
        selectSearch.innerHTML = `                    
        <div class="content-setting-addnew">
        <div class=" selectSearch">
        <datalist id="DeviceName">
            <option value="Air conditioning">
            <option value="Bathrobes">
            <option value="Closet">
            <option value="Clothes rack">
            <option value="Coffee/tea maker">
            <option value="Free bottled water">
            <option value="Hair dryer">
            <option value="In-room safe box">
            <option value="Linens">
            <option value="Mirror">
            <option value="Refrigerator">
            <option value="Slippers">
            <option value="Sofa">
            <option value="Telephone">
            <option value="Towels">
        </datalist>
            <input autoComplete="on" list="DeviceName" placeholder="Choose Device......" class="CountryName" />
            <button class="remove-content"><i
                    class="fal fa-times"></i></button>

        </div>
    </div>`;
        main.appendChild(selectSearch);
    }
}
function addnewContentSevice() {
    const main = document.getElementById('content-setting-service');
    if (main) {
        const selectSearch = document.createElement('div');
        selectSearch.onclick = function (e) {
            if (e.target.closest('.remove-content')) {
                main.removeChild(selectSearch);
            }
        }
        selectSearch.classList.add('content-setting-addnew');
        selectSearch.innerHTML = `                    
        <div class="content-setting-addnew">
        <div class=" selectSearch">
        <datalist id="ServiceName">
            <option value="Daily housekeeping">
            <option value="Airport shuttle">
            <option value="Free WiFi">
            <option value="Pets allowed">
            <option value="Laundry">
            <option value="Bar">
            <option value="Restaurant">
            <option value="Fitness centre">
        </datalist>
        <input autoComplete="on" list="ServiceName" placeholder="Choose Service......"
        class="CountryName" />
            <button class="remove-content"> <i
                    class="fal fa-times"></i></button>

        </div>
    </div>`;
        main.appendChild(selectSearch);
    }
}