var headers = new Headers();
headers.append("X-CSCAPI-KEY", "NDFZcGc5R3g5UzloV0xMYk5jeWZCY2FPWWpNMDUwNk95SDI3bVFOQg==");

var requestOptions = {
  method: 'GET',
  headers: headers,
  redirect: 'follow'
};
let dataCountries;
let dataCity;
async function getAllCountries() {
  dataCountries = await fetch("https://api.countrystatecity.in/v1/countries", requestOptions)
    .then(response => response.json())
    .then(result => result)
    .catch(error => console.log('error', error));
  let countries = document.querySelector("#CountrySuggestions");
  dataCountries.forEach(element => {
    let option = document.createElement("option")
    option.setAttribute("id", element.iso2);
    option.textContent = element.name;
    countries.appendChild(option);
  });
}
async function getCityByCountriesID() {
  let id = "";
  let CountryName = document.querySelector(".CountryName");

  dataCountries.forEach(element => {
    if (CountryName.value == element.name) {
      id = element.iso2
    }
  })

  dataCity = await fetch(`https://api.countrystatecity.in/v1/countries/${id}/states`, requestOptions)
    .then(response => response.json())
    .then(result => result)
    .catch(error => console.log('error', error));
  let city = document.querySelector("#CitySuggestions");
  dataCity.forEach(element => {
    let option = document.createElement("option")
    option.setAttribute("id", element.id);
    option.textContent = element.name;
    city.appendChild(option);
  });
}
async function getDistrictByCityID() {
  let countriesId = "";
  let cityID = ""
  let CountryName = document.querySelector(".CountryName");

  dataCountries.forEach(element => {
    if (CountryName.value == element.name) {
      countriesId = element.iso2
    }
  })
  let cityName = document.querySelector(".CityName");

  dataCity.forEach(element => {
    if (cityName.value == element.name) {
      cityID = element.iso2
    }
  })
  let data = await fetch(`https://api.countrystatecity.in/v1/countries/${countriesId}/states/${cityID}/cities`, requestOptions)
    .then(response => response.json())
    .then(result => result)
    .catch(error => console.log('error', error));
    let district = document.querySelector("#DistrictSuggestions");
  data.forEach(element => {
    let option = document.createElement("option")
    option.setAttribute("id", element.id);
    option.textContent = element.name;
    district.appendChild(option);


  })
}


window.onload = () => {
  getAllCountries();
}
let CountryNameInput = document.querySelector(".CountryName");
let CityNameInput = document.querySelector(".CityName");
let DistrictNameInput = document.querySelector(".DistrictName");
CountryNameInput.addEventListener("change", () => {
  let city = document.querySelector("#CitySuggestions");
  city.innerHTML = ""
  CityNameInput.value=""
  DistrictNameInput.value=""
  getCityByCountriesID();
})

CityNameInput.addEventListener("change", () => {
  let district = document.querySelector("#DistrictSuggestions");
  district.innerHTML = ""
  DistrictNameInput.value=""
  getDistrictByCityID();
})