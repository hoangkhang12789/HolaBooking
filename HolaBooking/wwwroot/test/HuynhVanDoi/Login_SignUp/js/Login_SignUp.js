const inputs = document.querySelectorAll(".input");


function addcl() {
	let parent = this.parentNode.parentNode;
	parent.classList.add("focus");
}

function remcl() {
	let parent = this.parentNode.parentNode;
	if (this.value == "") {
		parent.classList.remove("focus");
	}
}


inputs.forEach(input => {
	input.addEventListener("focus", addcl);
	input.addEventListener("blur", remcl);
});

function toast({ title = '', message = '', type = 'info ', duration = 3000 }) {
	const main = document.getElementById('toast');
	if (main) {

		const toast = document.createElement('div');
		 const remotimeout =  setTimeout(function() {
			main.removeChild(toast);
		}, duration)

		toast.onclick = function(e){
			if(e.target.closest('.toast__close')){
				main.removeChild(toast);
				clearTimeout(remotimeout);
			}
		}
		
		const icons = {
			success: 'far fa-check-circle',
			info: 'fas fa-info',
			error: 'fas fa-exclamation-triangle',
			warning: 'fas fa-exclamation',
		};
		const iconcheck = icons[type];
		const delay = (duration / 1000).toFixed(2);
		toast.classList.add('toast', `toast--${type}`);
		toast.style.animation = `msgInleft ease .3s, closemsg linear 1s ${delay}s forwards`;
		toast.innerHTML = `
			<div class="toast__icon">
				<i class="${iconcheck}"></i>
			</div>
			<div class="toast__body">
				<h3 class="toast__title">
					${title}
				</h3>
				<p class="toast__msg">
					${message}
				</p>
			</div>
			<div class="toast__close">
				<i class="fal fa-times"></i>
			</div>
			`;
		main.appendChild(toast);
	}
}
function showSuccess() {
	toast({
	title: 'Success',
	message: 'Đăng Nhập Thành Công',
	type: 'success',
	duration: 3000
});
}
function showError() {
	toast({
	title: 'Error',
	message: 'Lỗi Rồi Đấy Làm Gì Thì Làm! Tôi Không Biết',
	type: 'error',
	duration: 6000
});
} 
function showWarning() {
	toast({
	title: 'Warning',
	message: 'Sắp Lỗi Rồi Đó Sao Mà Phá Quá Vậy',
	type: 'warning',
	duration: 9000
});
} 
function showInfo() {
	toast({
	title: 'Info',
	message: 'Hi! friend...Are you OK!',
	type: 'info',
	duration: 12000
});
}