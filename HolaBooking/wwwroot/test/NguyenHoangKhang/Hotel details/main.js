let slideIndex = 1;
let slideRoomIndex = 1;
let seeAllPhotos = document.querySelector(".see-all");
let allPhotosShow = document.querySelectorAll(".hotel-photo #overlay");
let closeSlideShow = document.querySelectorAll(".exit-slide-show");
let ratingPoin = document.querySelectorAll(".list-star .rating");
let miniMap = document.querySelector(".details-hotel-info-map");
let showMap = document.querySelector(".show-map");
let exitMap = document.querySelector(".exit-map");
let navMap = document.querySelector(".mini-map");
let detailMap = document.querySelector(".details-map")
let editComment = document.querySelectorAll(".edit-comment-list p")
//Edit Review
editComment[0].addEventListener("click", () => {
  let parentBox = editComment[0].parentNode.parentNode.parentNode.parentNode;
  let point = parentBox.querySelectorAll(".comment-point-info .user-poin");
  let mainPoint = document.querySelectorAll(".review-create .choose-poin >.rating> .poin");
  let userComment = document.querySelector("#user-comment");
  let comment = document.querySelector(".comment-user-content p");
  mainPoint.forEach((element, index) => {
    element.innerHTML = parseInt(point[index].textContent);
    editComment[1].click();
  })
  let content = comment.textContent;
  userComment.value = content;

})
//Delete Review
editComment[1].addEventListener("click", () => {
  editComment[1].parentNode.parentNode.parentNode.parentNode.remove();
})
//Exit Map
exitMap.addEventListener("click", () => {
  showMap.style.display = "none"
})
//Show Map
miniMap.addEventListener("click", () => {
  showMap.style.display = "block"
})
navMap.addEventListener("click", () => {
  showMap.style.display = "block"
})
detailMap.addEventListener("click", () => {
  showMap.style.display = "block"
})
// Slide Show
showSlides(slideIndex);
function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  let i;
  let slides = document.getElementsByClassName("mySlides");
  let dots = document.getElementsByClassName("demo");
  let captionText = document.getElementById("caption");
  if (n > slides.length) { slideIndex = 1 }
  if (n < 1) { slideIndex = slides.length }
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
    slides[slideRoomIndex - 1].style.display = "flex";
    dots[slideRoomIndex - 1].className += " active";
    captionText.innerHTML = dots[slideRoomIndex - 1].alt;
}
// Open Slide Show
allPhotosShow.forEach((img, index) => {
  img.addEventListener("click", () => {
    document.querySelector(".show-photos").style.display = "block";
    showSlides(slideIndex = index + 1);
    document.body.style.backgroundColor = "#080808"
  })
})
seeAllPhotos.addEventListener("click", () => {
  document.querySelector(".show-photos").style.display = "block";
  document.body.style.backgroundColor = "#080808"
})
// CLose Slide Show
closeSlideShow.forEach((element, index) => {
  element.addEventListener("click", () => {
    document.querySelector(".show-photos").style.display = "none";
    showSlides(slideIndex = index + 1);
    document.body.style.backgroundColor = "#ffffff"
  })
})
// Get Current Time
function currentTime() {
  const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  const dates = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday",]
  let currentDate = new Date();
  return {
    year: currentDate.getFullYear(),
    month: months[currentDate.getMonth()],
    date: currentDate.getDate(),
    day: dates[currentDate.getDay()],
    hours: currentDate.getHours() + 1,
    minute: currentDate.getMinutes(),
    second: currentDate.getSeconds(),
    millisecond: currentDate.getMilliseconds(),
    time: currentDate.getTime()
  };

}
// Get and Render Weather information
async function loadWeather(location) {
  let img = document.querySelector(".weather-container img");
  let decs = document.querySelector(".weather-desc");
  let temperature = document.querySelector(".weather-temp")
  let windBox = document.querySelector(".wind p")
  let humidityBox = document.querySelector(".humidity p")
  let rainBox = document.querySelector(".rain p");
  let snowBox = document.querySelector(".snow p");
  let dateName = document.querySelectorAll(".date-dayname");
  let locationBox = document.querySelectorAll(".location");
  let time = document.querySelectorAll(".date-day");
  try {
    let api = `https://api.openweathermap.org/data/2.5/weather?q=${location}&appid=b009589cc77f39d3a6700f99db5bf71d`;
    let data = await fetch(api).then(res => res.json());
    let icon = data.weather[0].icon;
    let weatherStatus = data.weather[0].main;
    let temp = Math.round(data.main.temp - 273.15);
    let wind = data.wind.speed;
    let humidity = data.main.humidity;
    // let rain =data.rain.1h;
    // let snow = data.snow.1h;

    img.src = `http://openweathermap.org/img/wn/${icon}@2x.png`
    decs.innerText = weatherStatus;
    temperature.innerText = temp + "°C";
    windBox.innerText = wind;
    humidityBox.innerText = humidity;
    dateName[0].innerText = currentTime().day;
    dateName.forEach((element, index) => {
      dateName[index].innerText = currentTime().day;
      locationBox[0].innerText = data.name;
      time[index].innerText = `${currentTime().date} ${currentTime().month} ${currentTime().year}`
    })
  } catch (error) {
    decs.innerText = "?";
    temperature.innerText = "?" + "°C";
    windBox.innerText = "?";
    humidityBox.innerText = "?";
    dateName[0].innerText = currentTime().day;
    dateName.forEach((element, index) => {
      dateName[index].innerText = currentTime().day;
      locationBox[0].innerText = "?";
      time[index].innerText = `${currentTime().date} ${currentTime().month} ${currentTime().year}`
    })
  }
}
// Get and Render Covid information
async function loadCovidInformation(location) {
  let covidInformation = document.querySelectorAll(".covid-info-item p")
  let locationBox = document.querySelectorAll(".location");
  location = location.charAt(0).toUpperCase() + location.substring(1);
  locationBox[1].innerText = location;
  try {
    let api = `https://api.covid19api.com/country/${location}`;
    let data = await fetch(api)
      .then(res => res.json())
      .catch(err => { console.log(err + "ssssss") });
    let currentData = data[data.length - 1];

    let info = [currentData.Confirmed, currentData.Deaths, currentData.Recovered, currentData.Active];
    covidInformation.forEach((element, index) => {
      element.innerHTML = info[index];
    })

  } catch (error) {
    covidInformation.forEach((element, index) => {
      element.innerHTML = "Unknown";
    })
  }

}
// Choose Point Review
ratingPoin.forEach((element, index) => {
  element.addEventListener("click", () => {
    let point = element.parentNode.parentNode.querySelector(".poin");
    point.innerText = element.querySelector(".poin").textContent;
  })
})
loadWeather("Ca mau")
loadCovidInformation("vietnam");