﻿/*
 *Author : Nguyễn Hoàng Khang
*/
using System.Drawing.Imaging;
using System.Drawing;
namespace HolaBooking.Src.QRCode
{
    public  class BitmapExtension
    {
        public static Byte[] BitmapToBytes(Bitmap img)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                img.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                return stream.ToArray();
            }
        }
    }
}
