﻿/*
 *Author : Nguyễn Hoàng Khang
*/
using QRCoder;
using System.Drawing;

namespace HolaBooking.Src.QRCode
{
    public class QRCode
    {
        public static String CreateQrCode(String Text)
        {
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(Text, QRCodeGenerator.ECCLevel.Q);

            string fileGuid = Guid.NewGuid().ToString().Substring(0, 4);

            qrCodeData.SaveRawData("wwwroot/image/QrCode/file-" + fileGuid + ".qrr", QRCodeData.Compression.Uncompressed);

            return "wwwroot/image/QrCode/file-" + fileGuid + ".qrr";
        }
        public static string ReadQrCode(String path)
        {
            QRCodeData qrCodeData1 = new QRCodeData(path, QRCodeData.Compression.Uncompressed);
            QRCoder.QRCode qrCode = new QRCoder.QRCode(qrCodeData1);
            Bitmap qrCodeImage = qrCode.GetGraphic(20);
            var CodeByte=  BitmapExtension.BitmapToBytes(qrCodeImage);
            string QrUri = string.Format("data:image/png;base64,{0}", Convert.ToBase64String(CodeByte));
            return QrUri;
        }
    }
}
