﻿/*
 *Author : Nguyễn Hoàng Khang
*/
using static HolaBooking.src.Logger;

namespace HolaBooking.Src
{
    public class IO
    {
        public static async void UploadImage(IFormFile files, String path, String fileName)
        {
            try
            {
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                String filePath = $@"{path}/{fileName}";
                using (var stream = System.IO.File.Create(filePath))
                {
                   await files.CopyToAsync(stream);
                }

            }
            catch (System.Exception ex)
            {
                ErrorLog(ex.Message);
            }
        }
        public static void DeleteFile(string path)
        {
            try 
            {
                if (File.Exists(path))
                {
                    File.Delete(path);
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex.Message);
            }
          
        }
    }
}
