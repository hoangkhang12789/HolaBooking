﻿/*
 *Author : Nguyễn Hoàng Khang
*/

namespace HolaBooking.src
{
    public class Secret
    {

        public static string Encode(string text)
        {
            var mybyte = System.Text.Encoding.UTF8.GetBytes(text);
            String s = System.Convert.ToBase64String(mybyte);
            char[] array = s.ToCharArray();
            Array.Reverse(array);
            return new string(array);
        }

        public static string Decode(string text)
        {
            char[] array = text.ToCharArray();
            Array.Reverse(array);
            var mybyte = System.Convert.FromBase64String(new string(array));
            return System.Text.Encoding.UTF8.GetString(mybyte);
        }
        public static string BCryptEncode(string text)
        {
            return BCrypt.Net.BCrypt.HashPassword(text);
        }
        public static Boolean BCryptDecode(string text1,String text2)
        {
            return BCrypt.Net.BCrypt.Verify(text1, text2);
        }
    }
}
