﻿/*
 *Author : Nguyễn Hoàng Khang
*/
namespace HolaBooking.src
{
    public  class Logger
    {
        public static void SuccessLog(String text)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(text);
            Console.ResetColor();
        }
        public static void WarningLog(String text)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(text);
            Console.ResetColor();
        }
        public static void ErrorLog(String text)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(text);
            Console.ResetColor();
        }
    }
}
