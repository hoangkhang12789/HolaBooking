﻿namespace HolaBooking.Models

{
    public class Hotel
    {



        public int HotelID { get; set; }
        public int AccountID { get; set; }
        public string? Name { get; set; }
        public string? Img1 { get; set; }
        public string? Img2 { get; set; }
        public string? Img3 { get; set; }
        public string? Img4 { get; set; }
        public string? Img5 { get; set; }
        public string? Address { get; set; }
        public string? District { get; set; }
        public string? City { get; set; }
        public string? Country { get; set; }
        public double Star { get; set; }
        public double Point { get; set; }
        public string? Description { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }



    }
}
