﻿namespace HolaBooking.Models
{
    public class Time
    {
        public int January { get; set; } = 0;
        public int February { get; set; }= 0;
        public int March { get; set; }= 0;
        public int April { get; set; }= 0;
        public int May { get; set; }= 0;
        public int June { get; set; }= 0;
        public int July { get; set; }= 0;
        public int August { get; set; }= 0;
        public int September { get; set; }= 0;
        public int October { get; set; }= 0;
        public int November { get; set; }= 0;
        public int December { get; set; }= 0;
    }
}
