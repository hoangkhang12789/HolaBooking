﻿namespace HolaBooking.Models
{
    public class HotelByLocation
    {
        public string? Img1 { get; set; }
        public string? City { get; set; }
        public int NumberOfCity { get; set; }
    }
}
