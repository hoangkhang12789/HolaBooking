﻿namespace HolaBooking.Models
{
    public class Review
    {
        public int ReviewID { get; set; }
        public int AccountID { get; set; }
        public int HotelID { get; set; }
        public int Cleanliness { get; set; }
        public int Facilities { get; set; }
        public int Location { get; set; }
        public int Service { get; set; }
        public String? Text { get; set; }
        public DateTime Time { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public int Point { get; set; }
    }
}
