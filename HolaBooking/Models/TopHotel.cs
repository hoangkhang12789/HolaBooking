﻿namespace HolaBooking.Models
{
    public class TopHotel
    {
        public string City { get; set; }
        public List<Hotel> HotelList { get; set; }
    }
}
