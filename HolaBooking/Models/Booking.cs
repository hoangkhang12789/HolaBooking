﻿namespace HolaBooking.Models
{
    public class Booking
    {
        public int BookingID { get; set; }
        public int RoomID { get; set; }
        public int AccountID { get; set; }
        public String ?CheckIn { get; set; }
        public String ?CheckOut { get; set; }
        public int BookingStatusID { get; set; }
        public String ?QRCode { get; set; }

    }
}
