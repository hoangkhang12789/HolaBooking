﻿namespace HolaBooking.Models
{
    public class Device
    {
        public int RoomId { get; set; }
        public int DeviceID { get; set; }
        public String? DeviceName { get; set; }
    }
}
