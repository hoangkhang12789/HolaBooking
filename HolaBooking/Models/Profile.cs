﻿namespace HolaBooking.Models
{
    public class Profile
    {
        public int AccountID { get; set; }
        public string ?Firstname { get; set; }
        public string ?Lastname { get; set; }
        public string ?Avatar { get; set; }
        public string ?Email { get; set; }
        public string ?Phone { get; set; }

    }
}
