﻿namespace HolaBooking.Models
{
    public class Service
    {
        public int RoomId { get; set; }
        public int ServiceId { get; set; }
        public String? ServiceName { get; set; }
        public Boolean ServiceDefault { get; set; }

        public double ServicePrice { get; set; }
    }
}
