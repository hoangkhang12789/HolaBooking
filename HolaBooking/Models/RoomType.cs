﻿namespace HolaBooking.Models
{
    public class RoomType
    {
        public int RoomTypeID { get; set; }
        public String Name { get; set; }
        public String BedName { get; set; }
        public int Bed { get; set; }
        public int Sleep { get; set; }
    }
}
