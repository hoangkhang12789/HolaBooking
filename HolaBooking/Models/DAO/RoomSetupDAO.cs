﻿/*
 *Author : Huỳnh Minh Nhật
*/
using System.Data;
using System.Data.SqlClient;
using static HolaBooking.Models.DataConnection;
using static HolaBooking.src.Logger;

namespace HolaBooking.Models.DAO
{
    public class RoomSetupDAO
    {


        public static void InsertRoomInfo(Room room, bool isNewType)
        {
            try
            {
                int RoomTypeID = GetRoomTypeIDByName(room.RooomType);
                List<Room> list = new List<Room>();
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = "";
                if (isNewType)
                {
                    InsertRoomType(room);
                    RoomTypeID = GetRoomTypeIDByName(room.RooomType);
                }
                query += @" insert into Room(HotelID,RoomTypeID,Img1,Img2,Img3,Img4,Img5,RoomSize,Price,Discount,[Status])
                            values (@HotelID,@RoomTypeID,@Img1,@Img2,@Img3,@Img4,@Img5,@RoomSize,@Price,@Discount,@Status)";
                
                command.CommandText = query;
                command.Parameters.AddWithValue("@HotelID", room.HotelID);
                command.Parameters.AddWithValue("@RoomTypeID", RoomTypeID);
                command.Parameters.AddWithValue("@Img1", room.Image1);
                command.Parameters.AddWithValue("@Img2", room.Image2);
                command.Parameters.AddWithValue("@Img3", room.Image3);
                command.Parameters.AddWithValue("@Img4", room.Image4);
                command.Parameters.AddWithValue("@Img5", room.Image5);
                command.Parameters.AddWithValue("@RoomSize", room.RoomSize);
                command.Parameters.AddWithValue("@Price", room.Price);
                command.Parameters.AddWithValue("@Discount", room.Discount);
                command.Parameters.AddWithValue("@Status", room.Status);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ErrorLog("Insert Room Info Error ");
                ErrorLog(ex.Message);
            }
        }

        public static void InsertRoomType(Room room)
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = "";
                query += @"insert into RoomType([Name],BedName,Bed,Sleep)
                           values (@Name,@BedName,@Bed,@Sleep)";

                
                command.CommandText = query;
                command.Parameters.AddWithValue("@Name", room.RooomType);
                command.Parameters.AddWithValue("@BedName", room.BedName);
                command.Parameters.AddWithValue("@Bed", room.Bed);
                command.Parameters.AddWithValue("@Sleep", room.Sleep);

                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ErrorLog("Insert Room Type Error ");
                ErrorLog(ex.Message);
            }
        }

        public static void InsertDevice(List<String> room)
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = "";
                query += $" insert into Device(DeviceName) values ";
                for (int i = 0; i < room.Count; i++)
                {
                    if (i == room.Count - 1 && i == 0)
                    {
                        query += $" (@Name{i}) ";
                    }
                    else if (i == 0)
                    {
                        query += $" (@Name{i}) ";
                    }
                    else if (i == room.Count - 1)
                    {
                        query += $", (@Name{i}); ";
                    }
                    else
                    {
                        query += $", (@Name{i}) ";
                    }
                }
                
                command.CommandText = query;
                for (int i = 0; i < room.Count; i++)
                {
                    command.Parameters.AddWithValue($"@Name{i}", room[i]);
                }

                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ErrorLog("Insert Device Type Error ");
                ErrorLog(ex.Message);
            }
        }

        public static void InsertRoomDevice(Room room)
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = "";
                query += $" insert into RoomDevice(RoomID,DeviceID)  values ";
                for (int i = 0; i < room.ListDevices.Count; i++)
                {
                    if (i == room.ListDevices.Count - 1 && i == 0)
                    {
                        query += $" (@RoomID{i}, @DeviceID{i}) ";
                    }
                    else if (i == 0)
                    {
                        query += $" (@RoomID{i}, @DeviceID{i}) ";
                    }
                    else if (i == room.ListDevices.Count - 1)
                    {
                        query += $", (@RoomID{i}, @DeviceID{i}); ";
                    }
                    else
                    {
                        query += $", (@RoomID{i}, @DeviceID{i}) ";
                    }
                }

               
                command.CommandText = query;
                for (int i = 0; i < room.ListDevices.Count; i++)
                {
                    command.Parameters.AddWithValue($"@RoomID{i}", room.RoomId);
                    command.Parameters.AddWithValue($"@DeviceID{i}", room.ListDevices[i].DeviceID);
                }

                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ErrorLog("Insert Room Device Error ");
                ErrorLog(ex.Message);
            }
        }

        public static int GetDeviceIDByName(string Name)
        {
            try
            {
                int DeviceID = 0;
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"select  *
                                from Device
                                where DeviceName = @Name";
                command.CommandText = query;
                command.Parameters.AddWithValue("@Name", Name);
                using var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        DeviceID = reader.GetInt32(0);
                    }
                    return DeviceID;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get DeviceID Error ");
                ErrorLog(ex.Message);
            }
            return -1;
        }

        public static void InsertSevice(List<String> room, List<String> price)
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = "";
                query += $" insert into [Service](ServiceName,Price) values ";
                for (int i = 0; i < room.Count; i++)
                {
                    if (i == room.Count - 1 && i == 0)
                    {
                        query += $" (@Name{i}, @Price{i}) ";
                    }
                    else if (i == 0)
                    {
                        query += $" (@Name{i}, @Price{i}) ";
                    }
                    else if (i == room.Count - 1)
                    {
                        query += $", (@Name{i}, @Price{i}); ";
                    }
                    else
                    {
                        query += $", (@Name{i}, @Price{i}) ";
                    }
                }
                
                command.CommandText = query;
                for (int i = 0; i < room.Count; i++)
                {
                    command.Parameters.AddWithValue($"@Name{i}", room[i]);
                    command.Parameters.AddWithValue($"@Price{i}", price[i]);
                }

                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ErrorLog("Insert Sevice Error ");
                ErrorLog(ex.Message);
            }
        }

        public static void InsertRoomSevice(Room room)
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = "";
                query += $"  insert into RoomService(RoomID,ServiceID,ServiceDefault)  values ";
                for (int i = 0; i < room.ListServices.Count; i++)
                {
                    if (i == room.ListDevices.Count - 1 && i == 0)
                    {
                        query += $" (@RoomID{i}, @ServiceID{i}, @ServiceDefault{i}) ";
                    }
                    else if (i == 0)
                    {
                        query += $" (@RoomID{i}, @ServiceID{i}, @ServiceDefault{i}) ";
                    }
                    else if (i == room.ListDevices.Count - 1)
                    {
                        query += $", (@RoomID{i}, @ServiceID{i}, @ServiceDefault{i}); ";
                    }
                    else
                    {
                        query += $", (@RoomID{i}, @ServiceID{i}, @ServiceDefault{i}) ";
                    }
                }
                
                command.CommandText = query;
                for (int i = 0; i < room.ListServices.Count; i++)
                {
                    command.Parameters.AddWithValue($"@RoomID{i}", room.RoomId);
                    command.Parameters.AddWithValue($"@ServiceID{i}", room.ListServices[i].ServiceId);
                    command.Parameters.AddWithValue($"@ServiceDefault{i}", room.ListServices[i].ServiceDefault);
                }

                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ErrorLog("Insert Sevice Type Error ");
                ErrorLog(ex.Message);
            }
        }

        public static int GetSeviceIDByName(string Name)
        {
            try
            {
                int SeviceID = 0;
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"select  *
                                from [Service]
                                where ServiceName = @Name";
                command.CommandText = query;
                command.Parameters.AddWithValue("@Name", Name);
                using var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        SeviceID = reader.GetInt32(0);
                    }
                    return SeviceID;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get SeviceID Error ");
                ErrorLog(ex.Message);
            }
            return -1;
        }

        public static List<RoomType> GetAllRoomType()
        {
            try
            {
                List<RoomType> list = new List<RoomType>();
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"select  *
                                from RoomType";
                command.CommandText = query;
                using var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        RoomType t = new RoomType();
                        t.RoomTypeID = reader.GetInt32(0);
                        t.Name = reader.GetString(1);
                        t.BedName = reader.GetString(2);
                        t.Bed = reader.GetInt32(3);
                        t.Sleep = reader.GetInt32(4);
                        list.Add(t);
                    }
                    return list;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get List RoomType Error ");
                ErrorLog(ex.Message);
            }
            return null;
        }

        public static int GetRoomTypeIDByName(string Name)
        {
            try
            {
                int RoomTypeID = 0;
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"select  *
                                from RoomType
                                where [Name] = @Name";
                command.CommandText = query;
                command.Parameters.AddWithValue("@Name", Name);
                using var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        RoomTypeID = reader.GetInt32(0);
                    }
                    return RoomTypeID;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get List RoomType Error ");
                ErrorLog(ex.Message);
            }
            return -1;
        }

        public static int GetRoomIDByLast()
        {
            try
            {
                int RoomTypeID = 0;
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"select TOP(1) RoomID
                                from Room
                                ORDER BY RoomID DESC;";
                command.CommandText = query;
                using var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        RoomTypeID = reader.GetInt32(0);
                    }
                    return RoomTypeID;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get Last RoomID Error ");
                ErrorLog(ex.Message);
            }
            return -1;
        }

        public static List<Service> GetAllServiceList()
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"select r.RoomID, s.ServiceID, s.ServiceName, r.ServiceDefault, s.Price
                                    from [Service] s, RoomService r
                                    where s.ServiceID = r.ServiceID";
                command.CommandText = query;
                using var reader = command.ExecuteReader();
                List<Service> list = new List<Service>();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Service service = new Service();
                        service.RoomId = reader.GetInt32(0);
                        service.ServiceId = reader.GetInt32(1);
                        service.ServiceName = reader.GetString(2);
                        service.ServiceDefault = reader.GetBoolean(3);
                        service.ServicePrice = reader.GetDouble(4);
                        list.Add(service);
                    }
                    return list;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get All Service List Error ");
                ErrorLog(ex.Message);
            }
            return null;


        }

        public static void UpdateRoomInfo(Room room, bool isNewType)
        {
            try
            {
                int RoomTypeID = GetRoomTypeIDByName(room.RooomType);
                List<Room> list = new List<Room>();
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = "";
                if (isNewType)
                {
                    InsertRoomType(room);
                    RoomTypeID = GetRoomTypeIDByName(room.RooomType);
                }
                query += @" UPDATE Room
                            SET RoomTypeID=@RoomTypeID, Img1=@Img1, 
	                            Img2=@Img2, Img3=@Img3, Img4=@Img4, Img5=@Img5, 
	                            RoomSize=@RoomSize, Price=@Price
                            WHERE RoomID = @RoomID;";
                
                command.CommandText = query;
                command.Parameters.AddWithValue("@RoomID", room.RoomId);
                command.Parameters.AddWithValue("@RoomTypeID", RoomTypeID);
                command.Parameters.AddWithValue("@Img1", room.Image1);
                command.Parameters.AddWithValue("@Img2", room.Image2);
                command.Parameters.AddWithValue("@Img3", room.Image3);
                command.Parameters.AddWithValue("@Img4", room.Image4);
                command.Parameters.AddWithValue("@Img5", room.Image5);
                command.Parameters.AddWithValue("@RoomSize", room.RoomSize);
                command.Parameters.AddWithValue("@Price", room.Price);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ErrorLog("Update Room Info Error ");
                ErrorLog(ex.Message);
            }
        }

        public static void deleteDeviceByRoomID(int id)
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = "";
                query += $" DELETE FROM RoomDevice WHERE RoomID=@RoomID; ";
                command.CommandText = query;
                command.Parameters.AddWithValue($"@RoomID", id);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ErrorLog("delete room Device Error ");
                ErrorLog(ex.Message);
            }
        }

        public static void deleteServiceByRoomID(int id)
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = "";
                query += $" DELETE FROM RoomService WHERE RoomID=@RoomID; ";
                command.CommandText = query;
                command.Parameters.AddWithValue($"@RoomID", id);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ErrorLog("delete room Service Error ");
                ErrorLog(ex.Message);
            }
        }

        //public static List<Device> GetAllDeviceList()
        //{
        //    try
        //    {
        //        using SqlCommand command = new SqlCommand();
        //        command.Connection = SQL;
        //        String query = @"select r.RoomID, d.DeviceID, d.DeviceName
        //                            from Device d, RoomDevice r
        //                            where d.DeviceID = r.DeviceID";
        //        command.CommandText = query;
        //        using var reader = command.ExecuteReader();
        //        List<Device> list = new List<Device>();
        //        if (reader.HasRows)
        //        {
        //            while (reader.Read())
        //            {
        //                Device device = new Device();
        //                device.RoomId = reader.GetInt32(0);
        //                device.DeviceID = reader.GetInt32(1);
        //                device.DeviceName = reader.GetString(2);
        //                list.Add(device);
        //            }
        //            return list;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorLog("Get All Device List Error ");
        //        ErrorLog(ex.Message);
        //    }
        //    return null;


        //}
    }
}
