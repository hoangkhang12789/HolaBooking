﻿using System.Data;
using System.Data.SqlClient;
using static HolaBooking.Models.DataConnection;
using static HolaBooking.src.Logger;

namespace HolaBooking.Models.DAO
{
    public class HomeDAO
    {
      
        public static List<Hotel> GetListByCity(string city)

        {
            try
            {
                List<Hotel> list = new List<Hotel>();
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"select * from Hotel
                                 where City like @City";
                command.CommandText = query;
                command.Parameters.AddWithValue("@City", city);
                using var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Hotel h = new Hotel();
                        h.HotelID = reader.GetInt32(0);
                        h.AccountID = reader.GetInt32(1);
                        h.Name = reader.GetString(2);
                        h.Img1 = reader.GetString(3);
                        h.Img2 = reader.GetString(4);
                        h.Img3 = reader.GetString(5);
                        h.Img4 = reader.GetString(6);
                        h.Img5 = reader.GetString(7);
                        h.Address = reader.GetString(8);
                        h.District = reader.GetString(9);
                        h.City = reader.GetString(10);
                        h.Country = reader.GetString(11);
                        h.Star = reader.GetDouble(12);
                        h.Point = reader.GetDouble(13);
                        h.Description = reader.GetString(14);
                        list.Add(h);

                    }

                    return list;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get List Hotel Error  ");
                ErrorLog(ex.Message);
            }
            return null;
        }
        public static List<TopHotel> GetListTopCity()
        {
            try
            {
                List<TopHotel> list = new List<TopHotel>();
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"select top(5) AVG(Point) as AVGPONT, City as City from Hotel group by City";
                command.CommandText = query;
                using var reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        TopHotel h = new TopHotel();
                        h.City = reader.GetString(1);
                        list.Add(h);
                    }

                    return list;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get List Top City Error ");
                ErrorLog(ex.Message);
            }
            return null;
        }
        //top 5 thành phố của country việt nam 
        public static List<TopHotel> GetListTopCountry()
        {
            try
            {
                List<TopHotel> list = new List<TopHotel>();
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"select * into #Table
                                 from Hotel
                                 where Country = 'Vietnam'
                                 select top(5) AVG(Point) as AVGPONT, City as City from #Table group by City order by AVGPONT DESC
                                 drop table #Table";
                command.CommandText = query;
                using var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        TopHotel h = new TopHotel();
                        h.City = reader.GetString(1);
                        list.Add(h);
                    }

                    return list;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get List Top Country Error ");
                ErrorLog(ex.Message);
            }
            return null;
        }
        public static List<TopHotel> GetListTopCountryByLocation(string Country)
        {
            try
            {
                List<TopHotel> list = new List<TopHotel>();
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"select * into #Table
                                 from Hotel
                                 where Country = @Country
                                 select top(5) AVG(Point) as AVGPONT, City as City from #Table group by City
                                 drop table #Table";
                command.CommandText = query;
                command.Parameters.AddWithValue("@Country", Country);
                using var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        TopHotel h = new TopHotel();
                        h.City = reader.GetString(1);
                        list.Add(h);
                    }

                    return list;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get List Top CountryBy Location  Error  ");
                ErrorLog(ex.Message);
            }
            return null;
        }
        /*Ham tra ve số lượng đất nước khác location hiện tại */
        public static List<TopCountry> GetListTopLocation(String Country)
        {
            try
            {
                List<TopCountry> list = new List<TopCountry>();
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"select top 10 COUNT(Country) as NumberOfCountry  , Country from Hotel where Country != @Country
                                 group by Country";
                command.CommandText = query;
                command.Parameters.AddWithValue("@Country", Country);
                using var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        TopCountry h = new TopCountry();
                        h.NumberOfCountry = reader.GetInt32(0);
                        h.Country = reader.GetString(1);
                        list.Add(h);
                    }

                    return list;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get List Top Location Error  ");
                ErrorLog(ex.Message);
            }
            return null;
        }
        //Hàm trả vê ảnh của Country khác location hiện tại 
        public static string GetImgByCountry(string Country)

        {
            try
            {

                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"select top 1 Img1, Country from Hotel
                                    where Country = @Country";
                command.CommandText = query;
                command.Parameters.AddWithValue("@Country", Country);
                using var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    reader.Read();
                    return reader.GetString(0);

                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get List Img By Country Error  ");
                ErrorLog(ex.Message);
            }
            return null;
        }
        /*hàm sử dụng để load các thành phố của country nào đó theo Location*/
        public static List<HotelByLocation> GetListCityByLocation(string Country)
        {
            try
            {
                List<HotelByLocation> list = new List<HotelByLocation>();
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"select top 10 COUNT(City) as numberofcity, City from Hotel where Country = @Country
                                        group by City";
                command.CommandText = query;
                command.Parameters.AddWithValue("@Country", Country);
                using var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        HotelByLocation h = new HotelByLocation();
                        h.NumberOfCity = reader.GetInt32(0);
                        h.City = reader.GetString(1);
                        list.Add(h);
                    }

                    return list;
                }
            }
            catch (Exception ex)
            {
                ErrorLog(" Get List City By Location Error ");
                ErrorLog(ex.Message);
            }
            return null;
        }
        /*hàm sử dụng để load ảnh của từng hotel top 1*/
        public static string GetImgByCity(string city)

        {
            try
            {
                
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"select top 1 Img1, City from Hotel where City = @City";
                command.CommandText = query;
                command.Parameters.AddWithValue("@City", city);
                using var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    reader.Read();
                    return reader.GetString(0);

                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get List Img By City Error  ");
                ErrorLog(ex.Message);
            }
            return null;
        }
        public static List<HotelByLocation> GetAllCityByLocation()
        {
            try
            {
                List<HotelByLocation> list = new List<HotelByLocation>();
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"select top 10 COUNT(City) as numberofcity, City from Hotel 
                                        group by City";
                command.CommandText = query;
                using var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        HotelByLocation h = new HotelByLocation();
                        h.NumberOfCity = reader.GetInt32(0);
                        h.City = reader.GetString(1);
                        list.Add(h);
                    }

                    return list;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get All City By Location Error ");
                ErrorLog(ex.Message);
            }
            return null;
        }


    }
}
