﻿using System.Data.SqlClient;
using static HolaBooking.Models.DataConnection;
using static HolaBooking.src.Logger;
namespace HolaBooking.Models.DAO
{
    public class FacilitiesDao
    {
        
        public static List<Facilities> GetAllFacilitiesByHotelID(int Id)
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"Select HotelID,hf.FacilitiesID,FacilitiesName
                                 From HotelFacilities hf ,Facilities f
                                 Where HotelID = @ID and hf.FacilitiesID = f.FacilitiesID";
                command.CommandText = query;
                command.Parameters.AddWithValue("@ID", Id);
                using var reader = command.ExecuteReader();
              List<Facilities> list = new List<Facilities>();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Facilities facilities = new Facilities();
                        facilities.HotelID = reader.GetInt32(0);
                        facilities.FacilitiesID = reader.GetInt32(1);
                        facilities.FacilitiesName = reader.GetString(2);
                        list.Add(facilities);
                    }
                    return list;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get All Facilities By Hotel ID Error ");
                ErrorLog(ex.Message);
            }
            return null;
        
        
        }
    }
}
