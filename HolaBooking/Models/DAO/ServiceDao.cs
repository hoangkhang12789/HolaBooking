﻿using System.Data.SqlClient;
using static HolaBooking.Models.DataConnection;
using static HolaBooking.src.Logger;
namespace HolaBooking.Models.DAO
{
    public class ServiceDao
    {
        

        public static List<Service> GetAllServiceByRoomID(int Id)
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"Select RoomID,s.ServiceID,ServiceName,ServiceDefault, Price
                                 From RoomService rs ,Service s
                                 Where RoomID = @ID and rs.ServiceID = s.ServiceID";
                command.CommandText = query;
                command.Parameters.AddWithValue("@ID", Id);
                using var reader = command.ExecuteReader();
                List<Service> list = new List<Service>();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Service service = new Service();
                        service.RoomId = reader.GetInt32(0);
                        service.ServiceId = reader.GetInt32(1);
                        service.ServiceName = reader.GetString(2);
                        service.ServiceDefault = reader.GetBoolean(3);
                        service.ServicePrice = reader.GetDouble(4);
                        list.Add(service);
                    }
                    return list;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get All Service By Room ID Error ");
                ErrorLog(ex.Message);
            }
            return null;


        }

        public static void InsertBookingService(int BookingID, List<String> ListService)
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = "";
                
                for (int i = 0; i < ListService.Count; i++)
                {
                    
                    query += $@"Insert into BookingService ([BookingID], [ServiceID]) 
                                values (@BookingID,@ServiceID{i})
                                ";

                }
                
                command.CommandText = query;
                command.Parameters.AddWithValue("@BookingID", BookingID);
                for(int i = 0;i < ListService.Count; i++)
                {
                    command.Parameters.AddWithValue($"@ServiceID{i}", Int32.Parse(ListService[i]));
                }
                
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                ErrorLog("Insert Booking Service Error ");
                ErrorLog(ex.Message);
            }
        }

    }
}
