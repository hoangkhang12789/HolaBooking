﻿using System.Data.SqlClient;
using static HolaBooking.Models.DataConnection;
using static HolaBooking.src.Logger;
namespace HolaBooking.Models.DAO
{
    public class ProfileDao
    {
       

        public static Profile GetAccountByID(int Id)
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"Select *
                                From Profile
                                Where AccountID = @ID";
                command.CommandText = query;
                command.Parameters.AddWithValue("@ID", Id);
                using var reader = command.ExecuteReader();
                Profile profile = new Profile();

                if (reader.HasRows)
                {
                    reader.Read();

                    profile.AccountID = reader.GetInt32(0);
                    profile.Firstname = reader.GetString(1);
                    profile.Lastname = reader.GetString(2);
                    profile.Avatar = reader.GetString(3);

                    if (!reader.IsDBNull(4))
                    {
                        profile.Email = reader.GetString(4);
                    }
                    if (!reader.IsDBNull(5))
                    {
                        profile.Phone = reader.GetString(5);
                    }

                    return profile;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get Profile By Id Error ");
                ErrorLog(ex.Message);
            }
            return null;
        }
        public static void CreateProfile(int Id)
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"INSERT INTO Profile (AccountID, Firstname, Lastname, Avatar,Email,Phone)
                                 VALUES (@ID,'HolaBooking',@date,@avatar,'holabooking@gmail.com','0123456789');";
                command.CommandText = query;
                command.Parameters.AddWithValue("@ID", Id);
                command.Parameters.AddWithValue("@date", DateTime.Now.TimeOfDay);
                command.Parameters.AddWithValue("@avatar", @"/image/Profile/Hola.png");
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                ErrorLog("Create Profile  Error ");
                ErrorLog(ex.Message);
            }

        }
        public static void UpdateFullName(int id, String firstName, String Lastname)
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"Update Profile 
                                Set Firstname = @firstName , Lastname=@lastName
                                Where AccountID =@ID;";
                command.CommandText = query;
                command.Parameters.AddWithValue("@ID", id);
                command.Parameters.AddWithValue("@firstName", firstName);
                command.Parameters.AddWithValue("@lastName", Lastname);
                ;
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ErrorLog("Update Name Error ");
                ErrorLog(ex.Message);
            }
        }
        public static void UpdatePhone(int AccountID, String Phone)
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"Update Profile 
                                Set Phone = @phone
                                Where AccountID =@ID;";
                command.CommandText = query;
                command.Parameters.AddWithValue("@ID", AccountID);
                command.Parameters.AddWithValue("@phone", Phone);
                ;
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ErrorLog("Update Phone Error ");
                ErrorLog(ex.Message);
            }
        }
        public static void UpdateEmail(int AccountID, String Mail)
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"Update Profile 
                                Set Email = @mail 
                                Where AccountID =@ID;";
                command.CommandText = query;
                command.Parameters.AddWithValue("@ID", AccountID);
                command.Parameters.AddWithValue("@mail", Mail);

                ;
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ErrorLog("Update Email Error ");
                ErrorLog(ex.Message);
            }
        }
        public static void UpdateImage (int Id, string link)
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"UPDATE Profile
                               SET Avatar = @img
                               WHERE AccountID=@ID;";
                command.CommandText = query;
                command.Parameters.AddWithValue("@ID", Id);
                command.Parameters.AddWithValue("@img", link);
                command.ExecuteNonQuery();
                
                
            }
            catch (Exception ex)
            {
                ErrorLog("Update Image Error ");
                ErrorLog(ex.Message);
            }
           
        }
    }
}
