﻿using System.Data.SqlClient;
using static HolaBooking.Models.DataConnection;
using static HolaBooking.src.Logger;
namespace HolaBooking.Models.DAO
{
    public class DeviceDao
    {
       

        public static List<Device> GetAllDeviceByRoomID(int Id)
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"Select RoomID,d.DeviceID,DeviceName
                                 From RoomDevice rd ,Device d
                                 Where RoomID = @ID and rd.DeviceID = d.DeviceID";
                command.CommandText = query;
                command.Parameters.AddWithValue("@ID", Id);
                using var reader = command.ExecuteReader();
                List<Device> list = new List<Device>();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Device device = new Device();
                        device.RoomId = reader.GetInt32(0);
                        device.DeviceID = reader.GetInt32(1);
                        device.DeviceName = reader.GetString(2);
                       
                        list.Add(device);
                    }
                    return list;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get All Device By Room ID Error ");
                ErrorLog(ex.Message);
            }
            return null;


        }
    }
}
