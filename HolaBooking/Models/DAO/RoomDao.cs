﻿using System.Data.SqlClient;
using static HolaBooking.Models.DataConnection;
using static HolaBooking.src.Logger;
namespace HolaBooking.Models.DAO
{
    public class RoomDao
    {
      

        public static List<Room> GetAllRoomOfHotel(int ID)
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"Select RoomID,HotelID,t.Name,Img1,Img2,Img3,Img4,Img5,RoomSize,Price,Discount,Status,t.Bed,t.BedName,t.Sleep
                                 From Room r, RoomType t
                                 Where r.RoomTypeID = t.RoomTypeID and HotelID =@ID";
                command.CommandText = query;
                command.Parameters.AddWithValue("@ID", ID);
                using var reader = command.ExecuteReader();
                List<Room> list = new List<Room>();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Room room = new Room();
                        room.RoomId = reader.GetInt32(0);
                        room.HotelID = reader.GetInt32(1);
                        room.RooomType = reader.GetString(2);
                        room.Image1 = reader.GetString(3);
                        room.Image2 = reader.GetString(4);
                        room.Image3 = reader.GetString(5);
                        room.Image4 = reader.GetString(6);
                        room.Image5 = reader.GetString(7);
                        room.RoomSize = reader.GetDouble(8);
                        room.Price = reader.GetDouble(9);
                        room.Discount = reader.GetInt32(10);
                        room.Status = reader.GetBoolean(11);
                        room.Bed = reader.GetInt32(12);
                        room.BedName = reader.GetString(13);
                        room.Sleep = reader.GetInt32(14);

                        list.Add(room);
                    }

                    return list;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get All Room By Hotel ID Error ");
                ErrorLog(ex.Message);
            }
            return null;
        }

        public static Room GetRoomByID(int RoomID)
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"Select RoomID,HotelID,t.Name,Img1,Img2,Img3,Img4,Img5,RoomSize,Price,Discount,Status,t.Bed,t.BedName,t.Sleep
                                 From Room r, RoomType t
                                 Where r.RoomTypeID = t.RoomTypeID and RoomID =@ID";
                command.CommandText = query;
                command.Parameters.AddWithValue("@ID", RoomID);
                using var reader = command.ExecuteReader();
                Room room = new Room();
                if (reader.HasRows)
                {
                    reader.Read();
                    room.RoomId = reader.GetInt32(0);
                    room.HotelID = reader.GetInt32(1);
                    room.RooomType = reader.GetString(2);
                    room.Image1 = reader.GetString(3);
                    room.Image2 = reader.GetString(4);
                    room.Image3 = reader.GetString(5);
                    room.Image4 = reader.GetString(6);
                    room.Image5 = reader.GetString(7);
                    room.RoomSize = reader.GetDouble(8);
                    room.Price = reader.GetDouble(9);
                    room.Discount = reader.GetInt32(10);
                    room.Status = reader.GetBoolean(11);
                    room.Bed = reader.GetInt32(12);
                    room.BedName = reader.GetString(13);
                    room.Sleep = reader.GetInt32(14);

                    return room;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get Room By Room ID Error ");
                ErrorLog(ex.Message);
            }
            return null;
        }

        public static double GetMaxRoomPrice()
        {
            try
            {
                double MaxPrice = 0;
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"select TOP(1) r.Price
                                    from Room r
                                    order by r.Price desc";
                command.CommandText = query;
                using var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        MaxPrice = reader.GetDouble(0);
                    }
                    return MaxPrice;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get Max Room Price Error ");
                ErrorLog(ex.Message);
            }
            return -1;
        }
        /**********************************New**************************************/
        public static void DeleteRoomByID(int RoomID)
        {
            try 
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"delete Room where Room.RoomID = @RoomID";

                command.CommandText = query;

                command.Parameters.AddWithValue("@RoomID", RoomID);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ErrorLog("Delete Room Error");
                ErrorLog(ex.Message);
            }
        }
    }
}
