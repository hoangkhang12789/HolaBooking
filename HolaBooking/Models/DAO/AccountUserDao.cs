﻿/*
 *Author : Nguyễn Hoàng Khang
*/
using System.Data.SqlClient;
using static HolaBooking.Models.DataConnection;
using static HolaBooking.src.Logger;
using HolaBooking.src;

namespace HolaBooking.Models.DAO
{
    public class AccountUserDao
    {
       

        public static Boolean IsAccountExists(String userName, String password)
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"Select Username,Password
                               From Account
                               Where Username = @username";
                command.CommandText = query;
                command.Parameters.AddWithValue("@username", userName);
                using var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    reader.Read();
                    Boolean check = Secret.BCryptDecode(password, reader.GetString(1));
                    if (check)
                    {
                        return true;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorLog("IsAccountExists is Error ");
                ErrorLog(ex.Message);
            }
            return false;
        }


        public static Boolean IsAvailableUsername(String userName)
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"Select Username
                               From Account
                               Where Username = @username ";
                command.CommandText = query;
                command.Parameters.AddWithValue("@username", userName);
                using var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("IsAvailableUsername is Error ");
                ErrorLog(ex.Message);
            }
            return false;
        }
        public static void CreateNewAccount(String userName, String passWord)
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"INSERT INTO Account(Username,Password,PermissionID,Enable)
                                 VALUES (@username,@password,'3','True'); ";
                command.CommandText = query;
                command.Parameters.AddWithValue("@username", userName);

                command.Parameters.AddWithValue("@password", Secret.BCryptEncode(passWord));
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                ErrorLog("Create New Account is Error ");
                ErrorLog(ex.Message);
            }

        }
        public static AccountUser GetAccountByUserName(String userName)
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"Select *
                               From Account
                               Where Username = @username";
                command.CommandText = query;
                command.Parameters.AddWithValue("@username", userName);
                using var reader = command.ExecuteReader();
                AccountUser account = new AccountUser();
                if (reader.HasRows)
                {
                    reader.Read();
                    account.Id = reader.GetInt32(0);
                    return account;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get Account By User Name Error ");
                ErrorLog(ex.Message);
            }
            return null;
        }
        public static AccountUser GetAccountByUserId(int id)
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"Select *
                                From Account
                                Where AccountID = @Id";
                command.CommandText = query;
                command.Parameters.AddWithValue("@Id", id);
                using var reader = command.ExecuteReader();
                AccountUser account = new AccountUser();
                if (reader.HasRows)
                {
                    reader.Read();
                    account.Id = reader.GetInt32(0);
                    account.UserName = reader.GetString(1);
                    account.Password = reader.GetString(2);
                    account.PermissionID = reader.GetInt32(3);
                 
                    if(reader.GetBoolean(4)){
                        account.Enable =1;
                    }else{
                        account.Enable = 0;
                    }
                    return account;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get Account By User Name Error ");
                ErrorLog(ex.Message);
            }
            return null;
        }

        public static void UpdatePassWord(int AccountID,String passWord)
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"Update Account 
                                Set Password = @password
                                Where AccountID =@ID;";
                command.CommandText = query;
                command.Parameters.AddWithValue("@ID", AccountID);
                command.Parameters.AddWithValue("@password", Secret.BCryptEncode(passWord));
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                ErrorLog("Update PassWord is Error ");
                ErrorLog(ex.Message);
            }
        }
        public static void UpdatePermission(int accountID, int permissionID)
        {
            try
            {

                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"update Account
                                 Set Account.PermissionID = @permissionID
                                 where AccountID = @accountID";
                command.CommandText = query;
                command.Parameters.AddWithValue("@accountID", accountID);
                command.Parameters.AddWithValue("@permissionID", permissionID);

                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                ErrorLog("Update Permission is Error ");
                ErrorLog(ex.Message);
            }
        }
    }
}

