﻿using HolaBooking.src;
using System.Data.SqlClient;
using static HolaBooking.Models.DataConnection;
using static HolaBooking.src.Logger;
namespace HolaBooking.Models.DAO
{
    public class HotelDao
    {

        public static Hotel GetHotelByID(int Id)
        {

            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"Select *
                                 From Hotel
                                 Where HotelID = @ID";
                command.CommandText = query;
                command.Parameters.AddWithValue("@ID", Id);
                using var reader = command.ExecuteReader();
                Hotel hotel = new Hotel();
                if (reader.HasRows)
                {
                    reader.Read();
                    hotel.HotelID = reader.GetInt32(0);
                    hotel.AccountID = reader.GetInt32(1);
                    hotel.Name = reader.GetString(2);
                    hotel.Img1 = reader.GetString(3);
                    hotel.Img2 = reader.GetString(4);
                    hotel.Img3 = reader.GetString(5);
                    hotel.Img4 = reader.GetString(6);
                    hotel.Img5 = reader.GetString(7);
                    hotel.Address = reader.GetString(8);
                    hotel.District = reader.GetString(9);
                    hotel.City = reader.GetString(10);
                    hotel.Country = reader.GetString(11);
                    hotel.Star = reader.GetDouble(12);
                    hotel.Point = Math.Round(reader.GetDouble(13), 1);
                    hotel.Description = reader.GetString(14);
                    hotel.Latitude = reader.GetDouble(15);
                    hotel.Longitude = reader.GetDouble(16);

                    return hotel;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get Hotel By ID Error ");
                ErrorLog(ex.Message);
            }
            return null;
        }

        public static void UpdateHotelPoint(int Id, float point)
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"UPDATE Hotel
                                SET Point = @Point
                                WHERE HotelID = @ID";
                command.CommandText = query;
                command.Parameters.AddWithValue("@ID", Id);
                command.Parameters.AddWithValue("@Point", point);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ErrorLog("Update Hotel Point Error ");
                ErrorLog(ex.Message);
            }

        }

        public static void InsertHotel(Hotel hotel)
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"insert into Hotel 
                                 values(@accountID, @name, @img1, @img2, @img3, @img4, @img5, 
                                        @address, @district, @city, @conntry, 
                                        @Star, @point, @discription, @latitude, @longitude)";
                
                command.CommandText = query;

                command.Parameters.AddWithValue("@accountID", hotel.AccountID);
                command.Parameters.AddWithValue("@name", hotel.Name);

                command.Parameters.AddWithValue("@img1", hotel.Img1);
                command.Parameters.AddWithValue("@img2", hotel.Img2);
                command.Parameters.AddWithValue("@img3", hotel.Img3);
                command.Parameters.AddWithValue("@img4", hotel.Img4);
                command.Parameters.AddWithValue("@img5", hotel.Img5);


                command.Parameters.AddWithValue("@address", hotel.Address);
                command.Parameters.AddWithValue("@district", hotel.District);
                command.Parameters.AddWithValue("@city", hotel.City);
                command.Parameters.AddWithValue("@conntry", hotel.Country);

                command.Parameters.AddWithValue("@Star", hotel.Star);
                command.Parameters.AddWithValue("@point", hotel.Point);

                command.Parameters.AddWithValue("@discription", hotel.Description);
                command.Parameters.AddWithValue("@latitude", hotel.Latitude);
                command.Parameters.AddWithValue("@longitude", hotel.Longitude);

                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ErrorLog("Insert Hotel Error");
                ErrorLog(ex.Message);
            }
        }
        public static void UpdateHotel(Hotel hotel)
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"UPDATE Hotel
                    SET Name = @name, Img1 = @img1, Img2 = @img2, Img3 = @img3, Img4 = @img4, Img5 = @img5, Address = @address,
                    District = @district, City = @city, Country = @conntry,Star = @Star, Description = @discription,
                    Latitude = @latitude, Longitude = @longitude 
                    WHERE HotelID = @hotelID";

                command.CommandText = query;

                command.Parameters.AddWithValue("@hotelID", hotel.HotelID);
                command.Parameters.AddWithValue("@name", hotel.Name);

                command.Parameters.AddWithValue("@img1", hotel.Img1);
                command.Parameters.AddWithValue("@img2", hotel.Img2);
                command.Parameters.AddWithValue("@img3", hotel.Img3);
                command.Parameters.AddWithValue("@img4", hotel.Img4);
                command.Parameters.AddWithValue("@img5", hotel.Img5);


                command.Parameters.AddWithValue("@address", hotel.Address);
                command.Parameters.AddWithValue("@district", hotel.District);
                command.Parameters.AddWithValue("@city", hotel.City);
                command.Parameters.AddWithValue("@conntry", hotel.Country);

                command.Parameters.AddWithValue("@Star", hotel.Star);

                command.Parameters.AddWithValue("@discription", hotel.Description);
                command.Parameters.AddWithValue("@latitude", hotel.Latitude);
                command.Parameters.AddWithValue("@longitude", hotel.Longitude);

                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ErrorLog(ex.Message);
            }
        }
        public static void DeleteHotelByID(int hotelID)
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"delete Hotel where Hotel.HotelID = @hotelID";

                command.CommandText = query;

                command.Parameters.AddWithValue("@hotelID", hotelID);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ErrorLog("Delete Hotel Error");
                ErrorLog(ex.Message);
            }
        }
        public static List<Hotel> GetHotelByAccountID(int id)
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"Select *
                                from Hotel
                                Where AccountID= @ID ";
                command.CommandText = query;
                command.Parameters.AddWithValue("@ID", id);
                using var reader = command.ExecuteReader();
                List<Hotel> list = new List<Hotel>();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Hotel hotel = new Hotel();
                        hotel.HotelID = reader.GetInt32(0);
                        hotel.AccountID = reader.GetInt32(1);
                        hotel.Name = reader.GetString(2);
                        hotel.Img1 = reader.GetString(3);
                        hotel.Img2 = reader.GetString(4);
                        hotel.Img3 = reader.GetString(5);
                        hotel.Img4 = reader.GetString(6);
                        hotel.Img5 = reader.GetString(7);
                        hotel.Address = reader.GetString(8);
                        hotel.District = reader.GetString(9);
                        hotel.City = reader.GetString(10);
                        hotel.Country = reader.GetString(11);
                        hotel.Star = reader.GetDouble(12);
                        hotel.Point = Math.Round(reader.GetDouble(13), 1);
                        hotel.Description = reader.GetString(14);
                        hotel.Latitude = reader.GetDouble(15);
                        hotel.Longitude = reader.GetDouble(16);
                        list.Add(hotel);
                    }


                    return list;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get Hotel By Account ID Error ");
                ErrorLog(ex.Message);
            }
            return null;
        }
        public static int GetHotelIDByLast()
        {
            try
            {
                int RoomTypeID = 0;
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"select top(1) HotelID 
                                from Hotel
                                order by HotelID Desc;";
                command.CommandText = query;
                using var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        RoomTypeID = reader.GetInt32(0);
                    }
                    return RoomTypeID;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get Last HotelID Error ");
                ErrorLog(ex.Message);
            }
            return -1;
        }


        public static int GetFacilitiesIDByName(string Name)
        {
            try
            {
                int DeviceID = 0;
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"select  *
                                from Facilities
                                where FacilitiesName = @Name";
                command.CommandText = query;
                command.Parameters.AddWithValue("@Name", Name);
                using var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        DeviceID = reader.GetInt32(0);
                    }
                    return DeviceID;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get Facilities Error ");
                ErrorLog(ex.Message);
            }
            return -1;
        }

        public static void InsertFacilities(List<String> room)
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = "";
                query += $" insert into Facilities(FacilitiesName) values ";
                for (int i = 0; i < room.Count; i++)
                {
                    if (i == room.Count - 1 && i == 0)
                    {
                        query += $" (@Name{i}) ";
                    }
                    else if (i == 0)
                    {
                        query += $" (@Name{i}) ";
                    }
                    else if (i == room.Count - 1)
                    {
                        query += $", (@Name{i}); ";
                    }
                    else
                    {
                        query += $", (@Name{i}) ";
                    }
                }
                
                command.CommandText = query;
                for (int i = 0; i < room.Count; i++)
                {
                    command.Parameters.AddWithValue($"@Name{i}", room[i]);
                }

                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ErrorLog("Insert Facilities Type Error ");
                ErrorLog(ex.Message);
            }
        }

        public static void InsertHotelFacilities(List<Facilities> room, int HotelID)
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = "";
                query += $" insert into HotelFacilities(HotelID,FacilitiesID) values ";
                for (int i = 0; i < room.Count; i++)
                {
                    if (i == room.Count - 1 && i == 0)
                    {
                        query += $" (@RoomID{i}, @DeviceID{i}) ";
                    }
                    else if (i == 0)
                    {
                        query += $" (@RoomID{i}, @DeviceID{i}) ";
                    }
                    else if (i == room.Count - 1)
                    {
                        query += $", (@RoomID{i}, @DeviceID{i}); ";
                    }
                    else
                    {
                        query += $", (@RoomID{i}, @DeviceID{i}) ";
                    }
                }

                
                command.CommandText = query;
                for (int i = 0; i < room.Count; i++)
                {
                    command.Parameters.AddWithValue($"@RoomID{i}", HotelID);
                    command.Parameters.AddWithValue($"@DeviceID{i}", room[i].FacilitiesID);
                }

                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ErrorLog("Insert Hotel Facilities Error ");
                ErrorLog(ex.Message);
            }
        }
        public static void deleteFacilitiesByHotelID(int id)
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = "";
                query += $" DELETE FROM HotelFacilities where HotelID = @hotelID; ";
                command.CommandText = query;
                command.Parameters.AddWithValue($"@hotelID", id);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ErrorLog("delete hotel Facilities Error ");
                ErrorLog(ex.Message);
            }
        }

    }
}
