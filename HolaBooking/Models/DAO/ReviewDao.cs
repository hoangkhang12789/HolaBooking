﻿/*
 *Author : Nguyễn Hoàng Khang
*/
using System.Data.SqlClient;
using static HolaBooking.Models.DataConnection;
using static HolaBooking.src.Logger;
using HolaBooking.src;
namespace HolaBooking.Models.DAO
{
    public class ReviewDao
    {
       

        public static List<Review> GetAllReviewByHotelID(int id)
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"Select ReviewID ,r.AccountID, HotelID ,Cleanliness,Facilities,Location,Service,Text,Time,Firstname,Lastname
                               From Review r ,Profile p
                               Where r.AccountID = p.AccountID and r.HotelID = @Id
                               ORDER BY Time DESC;";
                command.CommandText = query;
                command.Parameters.AddWithValue("@Id", id);
                using var reader = command.ExecuteReader();
                List<Review> list = new List<Review>();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Review review = new Review();
                        review.ReviewID = reader.GetInt32(0);
                        review.AccountID = reader.GetInt32(1);
                        review.HotelID = reader.GetInt32(2);
                        review.Cleanliness = reader.GetInt32(3);
                        review.Facilities = reader.GetInt32(4);
                        review.Location = reader.GetInt32(5);
                        review.Service = reader.GetInt32(6);
                        review.Text = reader.GetString(7);
                        review.Time = reader.GetDateTime(8);
                        review.FirstName = reader.GetString(9);
                        review.LastName = reader.GetString(10);
                        review.Point = (review.Cleanliness + review.Facilities + review.Location + review.Service) / 4;
                        list.Add(review);
                    }


                    return list;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get All Review By HotelID Error ");
                ErrorLog(ex.Message);
            }
            return null;

        }
        public static void CreateAndUpdateReview(int accountID, int hotelID, String Text, int Cleanliness, int Facilitiesss, int Location, int Service)
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"IF EXISTS (SELECT * FROM Review WHERE AccountID = @accountID  and HotelID = @HotelID)
                                 BEGIN
                                 UPDATE Review
                                 SET Cleanliness =@Clean,Facilities = @Fac , Location = @Loca , Service =@Ser,Text =@text , Time =@time
                                 WHERE  AccountID = @accountID  and HotelID = @HotelID
                                 END
                                 ELSE
                                 BEGIN
                                 INSERT INTO Review(AccountID,HotelID,Cleanliness,Facilities,Location,Service,Text,Time)
                                 VALUES (@accountID,@HotelID,@Clean,@Fac,@Loca,@Ser,@text,@time)
                                 END";
                command.CommandText = query;
                command.Parameters.AddWithValue("@accountID", accountID);
                command.Parameters.AddWithValue("@HotelID", hotelID);
                command.Parameters.AddWithValue("@Clean", Cleanliness);
                command.Parameters.AddWithValue("@Fac", Facilitiesss);
                command.Parameters.AddWithValue("@Loca", Location);
                command.Parameters.AddWithValue("@Ser", Service);
                command.Parameters.AddWithValue("@text", Text);
                command.Parameters.AddWithValue("@time", DateTime.Now);
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                ErrorLog("Create Review Error ");
                ErrorLog(ex.Message);
            }

        }
        public static void DeleteReviewByReviewID(int ID)
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"DELETE FROM Review WHERE ReviewID = @ID ;";
                command.CommandText = query;
                command.Parameters.AddWithValue("@ID", ID);
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                ErrorLog("Delete Review Error ");
                ErrorLog(ex.Message);
            }
        }
    }
}
