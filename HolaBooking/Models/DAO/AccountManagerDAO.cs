﻿/*
 *Author : Huỳnh Minh Nhật
*/
using System.Data;
using System.Data.SqlClient;
using static HolaBooking.Models.DataConnection;
using static HolaBooking.src.Logger;

namespace HolaBooking.Models.DAO
{
    public class AccountManagerDAO
    {
        

        public static List<Profile> GetAllProfile()
        {
            try
            {
                List<Profile> list = new List<Profile>();
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"select *
                                from [Profile]";
                command.CommandText = query;
                using var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Profile p = new Profile();
                        p.AccountID = reader.GetInt32(0);
                        p.Firstname = reader.GetString(1);
                        p.Lastname = reader.GetString(2);
                        p.Avatar = reader.GetString(3);
                        try
                        {
                            p.Email = reader.GetString(4);
                        }
                        catch (Exception)
                        {
                            p.Email = "";
                        }
                        try
                        {
                            p.Phone = reader.GetString(5);
                        }
                        catch (Exception)
                        {
                            p.Phone = "";
                        }
                        list.Add(p);
                    }
                    return list;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get All Profile Error ");
                ErrorLog(ex.Message);
            }
            return null;
        }

        public static List<AccountUser> GetAllAccount()
        {
            try
            {
                List<AccountUser> list = new List<AccountUser>();
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"select *
                                from Account";
                command.CommandText = query;
                using var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        AccountUser a = new AccountUser();
                        a.Id = reader.GetInt32(0);
                        a.UserName = reader.GetString(1);
                        a.PermissionID = reader.GetInt32(3);
                        if (reader.GetBoolean(4))
                        {
                            a.Enable = 1;
                        }
                        else
                        {
                            a.Enable = 0;
                        }
                        list.Add(a);
                    }
                    return list;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get All AccountUser Error ");
                ErrorLog(ex.Message);
            }
            return null;
        }

        public static void ChangeAccountEnable(int ID, bool isEnable)
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = "";
                query += @"update Account
                            set [Enable] = @Enable
                            where AccountID = @id";
                command.CommandText = query;
                command.Parameters.AddWithValue("@id", ID);
                command.Parameters.AddWithValue("@Enable", isEnable);

                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ErrorLog("Change Account Enable Error ");
                ErrorLog(ex.Message);
            }
        }

        public static List<Hotel> GetListHotel()
        {
            try
            {
                List<Hotel> list = new List<Hotel>();
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"select *
                                from Hotel";
                command.CommandText = query;
                using var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Hotel h = new Hotel();
                        h.HotelID = reader.GetInt32(0);
                        h.AccountID = reader.GetInt32(1);
                        h.Name = reader.GetString(2);
                        h.Img1 = reader.GetString(3);
                        h.Img2 = reader.GetString(4);
                        h.Img3 = reader.GetString(5);
                        h.Img4 = reader.GetString(6);
                        h.Img5 = reader.GetString(7);
                        h.Address = reader.GetString(8);
                        h.District = reader.GetString(9);
                        h.City = reader.GetString(10);
                        h.Country = reader.GetString(11);
                        h.Star = reader.GetDouble(12);
                        h.Point = reader.GetDouble(13);
                        h.Description = reader.GetString(14);
                        list.Add(h);

                    }

                    return list;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get List Hotel Error ");
                ErrorLog(ex.Message);
            }
            return null;
        }

        public static List<Room> GetRoomBookingByAccountID(int ID, int status)
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"Select r.*
                                From Room r, Booking b
                                Where r.RoomID = b.RoomID
	                                and b.AccountID = @ID
                                    and b.BookingStatusID = @status";
                command.CommandText = query;
                command.Parameters.AddWithValue("@ID", ID);
                command.Parameters.AddWithValue("@status", status);
                using var reader = command.ExecuteReader();
                List<Room> list = new List<Room>();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Room room = new Room();
                        room.RoomId = reader.GetInt32(0);
                        room.HotelID = reader.GetInt32(1);
                        room.RooomType = reader.GetInt32(2)+"";
                        room.Image1 = reader.GetString(3);
                        room.Image2 = reader.GetString(4);
                        room.Image3 = reader.GetString(5);
                        room.Image4 = reader.GetString(6);
                        room.Image5 = reader.GetString(7);
                        room.RoomSize = reader.GetDouble(8);
                        room.Price = reader.GetDouble(9);
                        room.Discount = reader.GetInt32(10);
                        room.Status = reader.GetBoolean(11);
                        room.Bed = 0;
                        room.BedName = "";
                        room.Sleep = 0;

                        list.Add(room);
                    }

                    return list;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get list Room Booking By AccountID Error ");
                ErrorLog(ex.Message);
            }
            return null;
        }

        public static List<Hotel> GetHotelBookingByAccountID(int ID, int status)
        {
            try
            {
                List<Hotel> list = new List<Hotel>();
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"Select h.*
                                From Room r, Booking b, Hotel h
                                Where r.RoomID = b.RoomID
	                                and r.HotelID = h.HotelID
	                                and b.AccountID = @ID
	                                and b.BookingStatusID = @status";
                command.CommandText = query;
                command.Parameters.AddWithValue("@ID", ID);
                command.Parameters.AddWithValue("@status", status);
                using var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Hotel h = new Hotel();
                        h.HotelID = reader.GetInt32(0);
                        h.AccountID = reader.GetInt32(1);
                        h.Name = reader.GetString(2);
                        h.Img1 = reader.GetString(3);
                        h.Img2 = reader.GetString(4);
                        h.Img3 = reader.GetString(5);
                        h.Img4 = reader.GetString(6);
                        h.Img5 = reader.GetString(7);
                        h.Address = reader.GetString(8);
                        h.District = reader.GetString(9);
                        h.City = reader.GetString(10);
                        h.Country = reader.GetString(11);
                        h.Star = reader.GetDouble(12);
                        h.Point = reader.GetDouble(13);
                        h.Description = reader.GetString(14);
                        list.Add(h);

                    }

                    return list;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get Hotel Booking By AccountID Error ");
                ErrorLog(ex.Message);
            }
            return null;
        }


    }
}
