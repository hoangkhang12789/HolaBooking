﻿using System.Data.SqlClient;
using static HolaBooking.Models.DataConnection;
using static HolaBooking.src.Logger;
using HolaBooking.src;
namespace HolaBooking.Models.DAO
{
    public class BookingDao
    {


        public static void UpdateBookingComplete(int BookingID, String link)
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"UPDATE Booking
                                SET BookingStatusID= 2,QRCode = @link
                                WHERE BookingID= @id";
                command.CommandText = query;
                command.Parameters.AddWithValue("@id", BookingID);
                command.Parameters.AddWithValue("@link", link);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ErrorLog("Update Booking Error ");
                ErrorLog(ex.Message);
            }
        }
        public static Booking GetBookingByID(int BookingID)
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"Select * 
                                 From Booking
                                 Where Booking.BookingID = @id";
                command.CommandText = query;
                command.Parameters.AddWithValue("@id", BookingID);
                using var reader = command.ExecuteReader();
                Booking booking = new Booking();
                if (reader.HasRows)
                {
                    reader.Read();
                    booking.BookingID = reader.GetInt32(0);
                    booking.RoomID = reader.GetInt32(1);
                    booking.AccountID = reader.GetInt32(2);
                    booking.CheckIn = reader.GetString(3);
                    booking.CheckOut = reader.GetString(4);
                    return booking;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get Booking By ID Error ");
                ErrorLog(ex.Message);
            }
            return null;
        }

        public static Object InsertBooking(int RoomID, int AccountID, string CheckIn, string CheckOut, float Price)
        {
            try
            {

                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"INSERT into Booking ([RoomID], [AccountID], [CheckIn], [CheckOut], [BookingStatusID], [Price]) 
                                values (@RoomID, @AccountID, @CheckIn, @CheckOut, 1, @Price);
                                
                                select BookingID
                                from Booking
                                where RoomID = @RoomID and AccountID = @AccountID and CheckIn =  @CheckIn and CheckOut= @CheckOut and BookingStatusID =1 and Price = @Price";
                command.CommandText = query;
                command.Parameters.AddWithValue("@RoomID", RoomID);
                command.Parameters.AddWithValue("@AccountID", AccountID);
                command.Parameters.AddWithValue("@CheckIn", CheckIn);
                command.Parameters.AddWithValue("@CheckOut", CheckOut);
                command.Parameters.AddWithValue("@Price", Price);

                var bookingId = command.ExecuteScalar();
                return bookingId;
            }
            catch (Exception ex)
            {
                ErrorLog("Insert Booking Error ");
                ErrorLog(ex.Message);
            }
            return null;
        }
        public static List<Booking> GetALlBooking()
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"Select * 
                                 From Booking
                                ";
                command.CommandText = query;
                using var reader = command.ExecuteReader();
                List<Booking> list = new List<Booking>();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Booking booking = new Booking();
                        booking.BookingID = reader.GetInt32(0);
                        booking.RoomID = reader.GetInt32(1);
                        booking.AccountID = reader.GetInt32(2);
                        booking.CheckIn = reader.GetString(3);
                        booking.CheckOut = reader.GetString(4);
                        booking.BookingStatusID = reader.GetInt32(5);
                        list.Add(booking);
                    }

                    return list;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get All Booking Error ");
                ErrorLog(ex.Message);
            }
            return null;
        }
        public static List<Booking> GetALlBookingByAccountID(int id)
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"Select *
                                from Booking 
                                Where AccountID= @ID
                                ";
                command.CommandText = query;
                command.Parameters.AddWithValue("@ID", id);
                using var reader = command.ExecuteReader();
                List<Booking> list = new List<Booking>();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Booking booking = new Booking();
                        booking.BookingID = reader.GetInt32(0);
                        booking.RoomID = reader.GetInt32(1);
                        booking.AccountID = reader.GetInt32(2);
                        booking.CheckIn = reader.GetString(3);
                        booking.CheckOut = reader.GetString(4);
                        booking.BookingStatusID = reader.GetInt32(5);
                        list.Add(booking);
                    }

                    return list;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get All Booking By Account ID Error ");
                ErrorLog(ex.Message);
            }
            return null;
        }

        public static List<Booking> GetALlBookingByHotelOfAccountID(int id)
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"Select b.BookingID,b.RoomID , b.AccountID,b.CheckIn,b.CheckOut,b.BookingStatusID,b.Price,b.QRCode
                                From Booking b ,(Select RoomID
                                from Room r ,(Select HotelID
                                from Hotel
                                Where AccountID = @ID) h
                                Where r.HotelID = h.HotelID) r
                                Where b.RoomID = r.RoomID

                                ";
                command.CommandText = query;
                command.Parameters.AddWithValue("@ID", id);
                using var reader = command.ExecuteReader();
                List<Booking> list = new List<Booking>();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Booking booking = new Booking();
                        booking.BookingID = reader.GetInt32(0);
                        booking.RoomID = reader.GetInt32(1);
                        booking.AccountID = reader.GetInt32(2);
                        booking.CheckIn = reader.GetString(3);
                        booking.CheckOut = reader.GetString(4);
                        booking.BookingStatusID = reader.GetInt32(5);
                        list.Add(booking);
                    }

                    return list;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get ALl Booking By Hotel Of AccountID Error ");
                ErrorLog(ex.Message);
            }
            return null;
        }

        public static List<Booking> GetMyBookingByStatusID(int id, int status)
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"Select * 
                                From Booking b
                                where b.BookingStatusID = @status
	                                and b.AccountID = @ID";

                command.CommandText = query;
                command.Parameters.AddWithValue("@ID", id);
                command.Parameters.AddWithValue("@status", status);
                using var reader = command.ExecuteReader();
                List<Booking> list = new List<Booking>();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Booking booking = new Booking();
                        booking.BookingID = reader.GetInt32(0);
                        booking.RoomID = reader.GetInt32(1);
                        booking.AccountID = reader.GetInt32(2);
                        booking.CheckIn = reader.GetString(3);
                        booking.CheckOut = reader.GetString(4);
                        booking.BookingStatusID = reader.GetInt32(5);
                        
                        if (booking.BookingStatusID == 2)
                        {
                            if (reader.GetString(7) != null&& (reader.GetString(7).Length>0))
                            {
                                String link = reader.GetString(7);
                             
                                booking.QRCode = Src.QRCode.QRCode.ReadQrCode(link);

                            }
                        }
                        

                        list.Add(booking);
                    }

                    return list;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get Booking By StatusID Error ");
                ErrorLog(ex.Message);
            }
            return null;
        }

        public static void updateBooking(int bookingID, int bookingStatusID)
        {
            try
            {

                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"update Booking
                                    Set Booking.BookingStatusID = @bookingStatusID
                                    where BookingID = @bookingID";
                command.CommandText = query;
                command.Parameters.AddWithValue("@bookingID", bookingID);
                command.Parameters.AddWithValue("@bookingStatusID", bookingStatusID);

                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                ErrorLog("Update Booking is Error ");
                ErrorLog(ex.Message);
            }
        }
    }
}