﻿/*
 *Author : Huỳnh Minh Nhật
*/
using System.Data;
using System.Data.SqlClient;
using static HolaBooking.Models.DataConnection;
using static HolaBooking.src.Logger;


namespace HolaBooking.Models.DAO
{
    public class SearchDAO
    {
        

        public static List<Hotel> GetListByName(string name)
        {
            try
            {
                name = '%' + name + '%';
                List<Hotel> list = new List<Hotel>();
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"select * from Hotel
                                where [Name] like @name
	                                or [Address] like @name
	                                or City like @name
	                                or Country like @name";
                command.CommandText = query;
                command.Parameters.AddWithValue("@name", name);
                using var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Hotel h = new Hotel();
                        h.HotelID = reader.GetInt32(0);
                        h.AccountID = reader.GetInt32(1);
                        h.Name = reader.GetString(2);
                        h.Img1 = reader.GetString(3);
                        h.Img2 = reader.GetString(4);
                        h.Img3 = reader.GetString(5);
                        h.Img4 = reader.GetString(6);
                        h.Img5 = reader.GetString(7);
                        h.Address = reader.GetString(8);
                        h.District = reader.GetString(9);
                        h.City = reader.GetString(10);
                        h.Country = reader.GetString(11);
                        h.Star = reader.GetDouble(12);
                        h.Point = reader.GetDouble(13);
                        h.Description = reader.GetString(14);
                        list.Add(h);

                    }

                    return list;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get List Hotel Error ");
                ErrorLog(ex.Message);
            }
            return null;
        }

        public static List<Hotel> GetListByRoomType(int room)
        {
            try
            {
                List<Hotel> list = new List<Hotel>();
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"select Hotel.*
                                from Room, Hotel
                                where Room.HotelID = Hotel.HotelID
	                                and Room.RoomTypeID = @type";
                command.CommandText = query;
                command.Parameters.AddWithValue("@type", room);
                using var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Hotel h = new Hotel();
                        h.HotelID = reader.GetInt32(0);
                        h.AccountID = reader.GetInt32(1);
                        h.Name = reader.GetString(2);
                        h.Img1 = reader.GetString(3);
                        h.Img2 = reader.GetString(4);
                        h.Img3 = reader.GetString(5);
                        h.Img4 = reader.GetString(6);
                        h.Img5 = reader.GetString(7);
                        h.Address = reader.GetString(8);
                        h.District = reader.GetString(9);
                        h.City = reader.GetString(10);
                        h.Country = reader.GetString(11);
                        h.Star = reader.GetDouble(12);
                        h.Point = reader.GetDouble(13);
                        h.Description = reader.GetString(14);
                        list.Add(h);

                    }

                    return list;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get List Hotel Error ");
                ErrorLog(ex.Message);
            }
            return null;
        }

        public static List<Hotel> GetListByNameOrType(string name, int type)
        {
            try
            {
                name = '%' + name + '%';
                List<Hotel> list = new List<Hotel>();
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"select * from (select Hotel.*
                                from Room, Hotel
                                where Room.HotelID = Hotel.HotelID
	                                and Room.RoomTypeID = @type) A
                                where [Name] like @name
	                                or [Address] like @name
	                                or City like @name
	                                or Country like @name";
                command.CommandText = query;
                command.Parameters.AddWithValue("@name", name);
                command.Parameters.AddWithValue("@type", type);
                using var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Hotel h = new Hotel();
                        h.HotelID = reader.GetInt32(0);
                        h.AccountID = reader.GetInt32(1);
                        h.Name = reader.GetString(2);
                        h.Img1 = reader.GetString(3);
                        h.Img2 = reader.GetString(4);
                        h.Img3 = reader.GetString(5);
                        h.Img4 = reader.GetString(6);
                        h.Img5 = reader.GetString(7);
                        h.Address = reader.GetString(8);
                        h.District = reader.GetString(9);
                        h.City = reader.GetString(10);
                        h.Country = reader.GetString(11);
                        h.Star = reader.GetDouble(12);
                        h.Point = reader.GetDouble(13);
                        h.Description = reader.GetString(14);
                        list.Add(h);

                    }
                    return list;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get List Hotel by name or type  Error ");
                ErrorLog(ex.Message);
            }
            return null;
        }

        public static List<Hotel> GetPagingListByNameOrType(string name, int type, int page)
        {
            try
            {
                name = '%' + name + '%';
                page = (page - 1) * 4;
                List<Hotel> list = new List<Hotel>();
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"select * from (select Hotel.*
                                from Room, Hotel
                                where Room.HotelID = Hotel.HotelID
	                                and Room.RoomTypeID = 1) A
                                where [Name] like '%%'
	                                or [Address] like '%%'
	                                or City like '%%'
	                                or Country like '%%'
                                ORDER BY HotelID
                                OFFSET @page ROWS FETCH NEXT 4 ROWS ONLY;";
                command.CommandText = query;
                command.Parameters.AddWithValue("@name", name);
                command.Parameters.AddWithValue("@type", type);
                command.Parameters.AddWithValue("@page", page);
                using var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Hotel h = new Hotel();
                        h.HotelID = reader.GetInt32(0);
                        h.AccountID = reader.GetInt32(1);
                        h.Name = reader.GetString(2);
                        h.Img1 = reader.GetString(3);
                        h.Img2 = reader.GetString(4);
                        h.Img3 = reader.GetString(5);
                        h.Img4 = reader.GetString(6);
                        h.Img5 = reader.GetString(7);
                        h.Address = reader.GetString(8);
                        h.District = reader.GetString(9);
                        h.City = reader.GetString(10);
                        h.Country = reader.GetString(11);
                        h.Star = reader.GetDouble(12);
                        h.Point = reader.GetDouble(13);
                        h.Description = reader.GetString(14);
                        list.Add(h);

                    }

                    return list;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get Paging List Hotel by name or type Error ");
                ErrorLog(ex.Message);
            }
            return null;
        }

        public static List<Hotel> GetPagingListByName(int page, string name)
        {
            try
            {
                name = '%' + name + '%';
                page = (page - 1) * 4;
                List<Hotel> list = new List<Hotel>();
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"select * from Hotel h
                                where [Name] Like @name
	                                or h.City Like @name
	                                or [Address] Like @name
	                                or h.Country Like @name
                                ORDER BY h.HotelID
                                OFFSET @page ROWS FETCH NEXT 4 ROWS ONLY;";
                command.CommandText = query;
                command.Parameters.AddWithValue("@name", name);
                command.Parameters.AddWithValue("@page", page);
                using var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Hotel h = new Hotel();
                        h.HotelID = reader.GetInt32(0);
                        h.AccountID = reader.GetInt32(1);
                        h.Name = reader.GetString(2);
                        h.Img1 = reader.GetString(3);
                        h.Img2 = reader.GetString(4);
                        h.Img3 = reader.GetString(5);
                        h.Img4 = reader.GetString(6);
                        h.Img5 = reader.GetString(7);
                        h.Address = reader.GetString(8);
                        h.District = reader.GetString(9);
                        h.City = reader.GetString(10);
                        h.Country = reader.GetString(11);
                        h.Star = reader.GetDouble(12);
                        h.Point = reader.GetDouble(13);
                        h.Description = reader.GetString(14);
                        list.Add(h);

                    }

                    return list;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get List Hotel Error ");
                ErrorLog(ex.Message);
            }
            return null;
        }

        public static List<Facilities> GetFacilitiesList()
        {
            try
            {
                List<Facilities> list = new List<Facilities>();
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"select h.HotelID,f.FacilitiesID, f.FacilitiesName
                                from HotelFacilities h, Facilities f
                                where h.FacilitiesID = f.FacilitiesID";
                command.CommandText = query;
                using var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Facilities f = new Facilities();
                        f.HotelID = reader.GetInt32(0);
                        f.FacilitiesID = reader.GetInt32(1);
                        f.FacilitiesName = reader.GetString(2);
                        list.Add(f);
                    }
                    return list;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get List Facilities Error ");
                ErrorLog(ex.Message);
            }
            return null;
        }

        public static List<Room> GetHotelRoomPriceList()
        {
            try
            {
                List<Room> list = new List<Room>();
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"Select RoomID,HotelID,t.Name,Img1,Img2,Img3,Img4,Img5,RoomSize,Price,Discount,Status,t.Bed,t.BedName,t.Sleep
                                 From Room r, RoomType t
                                 Where r.RoomTypeID = t.RoomTypeID";
                command.CommandText = query;
                using var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Room room = new Room();
                        room.RoomId = reader.GetInt32(0);
                        room.HotelID = reader.GetInt32(1);
                        room.RooomType = reader.GetString(2);
                        room.Image1 = reader.GetString(3);
                        room.Image2 = reader.GetString(4);
                        room.Image3 = reader.GetString(5);
                        room.Image4 = reader.GetString(6);
                        room.Image5 = reader.GetString(7);
                        room.RoomSize = reader.GetDouble(8);
                        room.Price = reader.GetDouble(9);
                        room.Discount = reader.GetInt32(10);
                        room.Status = reader.GetBoolean(11);
                        room.Bed = reader.GetInt32(12);
                        room.BedName = reader.GetString(13);
                        room.Sleep = reader.GetInt32(14);

                        list.Add(room);
                    }
                    return list;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get List Room price Error ");
                ErrorLog(ex.Message);
            }
            return null;
        }

        public static List<Hotel> GetListByFilterPrice(string name, int type, List<String> filter)
        {
            try
            {
                name = '%' + name + '%';
                List<Hotel> list = new List<Hotel>();
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"select * from (select Hotel.*
                                from Room, Hotel
                                where Room.HotelID = Hotel.HotelID
	                                and Room.RoomTypeID = @type
	                                and Price <= @max
	                                and Price >= @min";
                if (filter.Count >= 3)
                {
                    query += $" and (";
                    for (int i = 2; i < filter.Count; i++)
                    {
                        if (i == 2 && i == filter.Count - 1)
                        {
                            query += $"Star = @star{i})";
                        }
                        else if (i == 2)
                        {
                            query += $"Star = @star{i}";
                        }
                        else if (i == filter.Count - 1)
                        {
                            query += $" or Star = @star{i})";
                        }
                        else
                        {
                            query += $" or Star = @star{i}";
                        }
                    }

                }
                query += @" ) A
                                where [Name] like @name
	                                or [Address] like @name
	                                or City like @name
	                                or Country like @name";
                
                command.CommandText = query;
                command.Parameters.AddWithValue("@name", name);
                command.Parameters.AddWithValue("@type", type);
                command.Parameters.AddWithValue("@max", filter[1]);
                command.Parameters.AddWithValue("@min", filter[0]);
                if (filter.Count >= 3)
                {
                    for (int i = 2; i < filter.Count; i++)
                    {
                        command.Parameters.AddWithValue($"@star{i}", filter[i]);
                    }

                }
                using var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Hotel h = new Hotel();
                        h.HotelID = reader.GetInt32(0);
                        h.AccountID = reader.GetInt32(1);
                        h.Name = reader.GetString(2);
                        h.Img1 = reader.GetString(3);
                        h.Img2 = reader.GetString(4);
                        h.Img3 = reader.GetString(5);
                        h.Img4 = reader.GetString(6);
                        h.Img5 = reader.GetString(7);
                        h.Address = reader.GetString(8);
                        h.District = reader.GetString(9);
                        h.City = reader.GetString(10);
                        h.Country = reader.GetString(11);
                        h.Star = reader.GetDouble(12);
                        h.Point = reader.GetDouble(13);
                        h.Description = reader.GetString(14);
                        h.Latitude = reader.GetDouble(15);
                        h.Longitude = reader.GetDouble(16);
                        list.Add(h);

                    }
                    return list;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get List Hotel by filter  Error ");
                ErrorLog(ex.Message);
            }
            return null;
        }

        public static List<Hotel> GetAllByFilterPrice(string name, List<String> filter)
        {
            try
            {
                name = '%' + name + '%';
                List<Hotel> list = new List<Hotel>();
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"select * from (select Hotel.*
                                from Room, Hotel
                                where Room.HotelID = Hotel.HotelID
	                                and Price <= @max
	                                and Price >= @min";
                if (filter.Count >= 3)
                {
                    query += $" and (";
                    for (int i = 2; i < filter.Count; i++)
                    {
                        if (i == 2 && i == filter.Count - 1)
                        {
                            query += $"Star = @star{i})";
                        }
                        else if (i == 2)
                        {
                            query += $"Star = @star{i}";
                        }
                        else if (i == filter.Count - 1)
                        {
                            query += $" or Star = @star{i})";
                        }
                        else
                        {
                            query += $" or Star = @star{i}";
                        }
                    }

                }
                query += @" ) A
                                where [Name] like @name
	                                or [Address] like @name
	                                or City like @name
	                                or Country like @name";
                
                command.CommandText = query;
                command.Parameters.AddWithValue("@name", name);
                command.Parameters.AddWithValue("@max", filter[1]);
                command.Parameters.AddWithValue("@min", filter[0]);
                if (filter.Count >= 3)
                {
                    for (int i = 2; i < filter.Count; i++)
                    {
                        command.Parameters.AddWithValue($"@star{i}", filter[i]);
                    }

                }
                using var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Hotel h = new Hotel();
                        h.HotelID = reader.GetInt32(0);
                        h.AccountID = reader.GetInt32(1);
                        h.Name = reader.GetString(2);
                        h.Img1 = reader.GetString(3);
                        h.Img2 = reader.GetString(4);
                        h.Img3 = reader.GetString(5);
                        h.Img4 = reader.GetString(6);
                        h.Img5 = reader.GetString(7);
                        h.Address = reader.GetString(8);
                        h.District = reader.GetString(9);
                        h.City = reader.GetString(10);
                        h.Country = reader.GetString(11);
                        h.Star = reader.GetDouble(12);
                        h.Point = reader.GetDouble(13);
                        h.Description = reader.GetString(14);
                        h.Latitude = reader.GetDouble(15);
                        h.Longitude = reader.GetDouble(16);
                        list.Add(h);

                    }
                    return list;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get All Hotel by filter  Error ");
                ErrorLog(ex.Message);
            }
            return null;
        }

        public static List<Facilities> GetAllFacilities()
        {
            try
            {
                List<Facilities> list = new List<Facilities>();
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"select  *
                                from Facilities";
                command.CommandText = query;
                using var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Facilities f = new Facilities();
                        f.HotelID = 1;
                        f.FacilitiesID = reader.GetInt32(0);
                        f.FacilitiesName = reader.GetString(1);
                        list.Add(f);
                    }
                    return list;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get List Facilities Error ");
                ErrorLog(ex.Message);
            }
            return null;
        }

        public static List<Service> GetAllServiceList()
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"select *
                                from [Service]";
                command.CommandText = query;
                using var reader = command.ExecuteReader();
                List<Service> list = new List<Service>();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Service service = new Service();
                        service.RoomId = 1;
                        service.ServiceId = reader.GetInt32(0);
                        service.ServiceName = reader.GetString(1);
                        service.ServiceDefault = true;
                        service.ServicePrice = reader.GetDouble(2);
                        list.Add(service);
                    }
                    return list;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get All Service List Error ");
                ErrorLog(ex.Message);
            }
            return null;


        }

        public static List<Device> GetAllDeviceList()
        {
            try
            {
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"select *
                                from Device";
                command.CommandText = query;
                using var reader = command.ExecuteReader();
                List<Device> list = new List<Device>();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Device device = new Device();
                        device.RoomId = 1;
                        device.DeviceID = reader.GetInt32(0);
                        device.DeviceName = reader.GetString(1);
                        list.Add(device);
                    }
                    return list;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get All Device List Error ");
                ErrorLog(ex.Message);
            }
            return null;


        }


        public static List<Hotel> GetAllFilterLists(string name, int type, int page, List<String> fltStars,
            List<String> fltFaci, List<String> fltServi, bool isType, bool isPaging, bool isFaci, bool isRoom)
        {
            try
            {
                name = '%' + name + '%';
                if (isPaging)
                {
                    page = (page - 1) * 4;
                }
                List<Hotel> list = new List<Hotel>();
                using SqlCommand command = new SqlCommand();
                command.Connection = SQL;
                String query = @"select h.*
                                from(select A.HotelID from (
	                                select h.*
	                                from Hotel h
	                                 ";
                if (isType || isRoom)
                {
                    query = @"select h.*
                                from(select A.HotelID from (
	                                select h.*, r.RoomID
	                                from Room r, Hotel h
	                                where r.HotelID = h.HotelID 
                                        and Price <= @max
	                                    and Price >= @min";
                }
                if (isType)
                {
                    query += @" and r.RoomTypeID = @type";
                }
                if (fltStars.Count >= 3)
                {
                    if (isType || isRoom)
                    {
                        query += $" and (";
                    }
                    else
                    {
                        query += $" where (";
                    }
                    for (int i = 2; i < fltStars.Count; i++)
                    {
                        if (i == 2 && i == fltStars.Count - 1)
                        {
                            query += $"Star = @star{i})";
                        }
                        else if (i == 2)
                        {
                            query += $"Star = @star{i}";
                        }
                        else if (i == fltStars.Count - 1)
                        {
                            query += $" or Star = @star{i})";
                        }
                        else
                        {
                            query += $" or Star = @star{i}";
                        }
                    }

                }
                query += @" ) A ";
                if (isFaci)
                {
                    query += @" , HotelFacilities c";
                }
                if (fltServi.Count >= 1)
                {
                    query += @" , RoomService s";

                }
                query += @" where";
                if (isFaci)
                {
                    query += @" A.HotelID = c.HotelID";
                }
                if (fltServi.Count >= 1)
                {
                    if (isFaci)
                    {
                        query += @" and A.RoomID = s.RoomID";
                    }
                    else
                    {
                        query += @" A.RoomID = s.RoomID";
                    }
                }
                //faci
                if (fltFaci.Count >= 1)
                {
                    query += $" and (";
                    for (int i = 0; i < fltFaci.Count; i++)
                    {
                        if (i == 0 && i == fltFaci.Count - 1)
                        {
                            query += $"FacilitiesID = @faci{i})";
                        }
                        else if (i == 0)
                        {
                            query += $"FacilitiesID = @faci{i}";
                        }
                        else if (i == fltFaci.Count - 1)
                        {
                            query += $" or FacilitiesID = @faci{i})";
                        }
                        else
                        {
                            query += $" or FacilitiesID = @faci{i}";
                        }
                    }

                }
                //Servi
                if (fltServi.Count >= 1)
                {
                    query += $" and (";
                    for (int i = 0; i < fltServi.Count; i++)
                    {
                        if (i == 0 && i == fltServi.Count - 1)
                        {
                            query += $"ServiceID = @sevi{i})";
                        }
                        else if (i == 0)
                        {
                            query += $"ServiceID = @sevi{i}";
                        }
                        else if (i == fltServi.Count - 1)
                        {
                            query += $" or ServiceID = @sevi{i})";
                        }
                        else
                        {
                            query += $" or ServiceID = @sevi{i}";
                        }
                    }

                }
                if (isFaci || fltServi.Count >= 1)
                {
                    query += @" and ";
                }
                query += @" ([Name] like @name
                                    or [Address] like @name
                                    or City like @name
                                    or Country like @name)
                            INTERSECT
                            select t.HotelID
                            from Hotel t) B, Hotel h
                            where h.HotelID = B.HotelID";
                if (isPaging)
                {
                    query += @" ORDER BY HotelID
                                OFFSET @page ROWS FETCH NEXT 4 ROWS ONLY";
                }
                command.CommandText = query;
                command.Parameters.AddWithValue("@name", name);
                if (isType || isRoom)
                {
                    command.Parameters.AddWithValue("@max", fltStars[1]);
                    command.Parameters.AddWithValue("@min", fltStars[0]);
                }
                if (isType)
                {
                    command.Parameters.AddWithValue("@type", type);
                }
                if (isPaging)
                {
                    command.Parameters.AddWithValue("@page", page);
                }
                if (fltStars.Count >= 3)
                {
                    for (int i = 2; i < fltStars.Count; i++)
                    {
                        command.Parameters.AddWithValue($"@star{i}", fltStars[i]);
                    }
                }
                if (fltFaci.Count >= 1)
                {
                    for (int i = 0; i < fltFaci.Count; i++)
                    {
                        command.Parameters.AddWithValue($"@faci{i}", fltFaci[i]);
                    }

                }
                if (fltServi.Count >= 1)
                {
                    for (int i = 0; i < fltServi.Count; i++)
                    {
                        command.Parameters.AddWithValue($"@sevi{i}", fltServi[i]);
                    }

                }
                using var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Hotel h = new Hotel();
                        h.HotelID = reader.GetInt32(0);
                        h.AccountID = reader.GetInt32(1);
                        h.Name = reader.GetString(2);
                        h.Img1 = reader.GetString(3);
                        h.Img2 = reader.GetString(4);
                        h.Img3 = reader.GetString(5);
                        h.Img4 = reader.GetString(6);
                        h.Img5 = reader.GetString(7);
                        h.Address = reader.GetString(8);
                        h.District = reader.GetString(9);
                        h.City = reader.GetString(10);
                        h.Country = reader.GetString(11);
                        h.Star = reader.GetDouble(12);
                        h.Point = reader.GetDouble(13);
                        h.Description = reader.GetString(14);
                        h.Latitude = reader.GetDouble(15);
                        h.Longitude = reader.GetDouble(16);
                        list.Add(h);

                    }
                    return list;
                }
            }
            catch (Exception ex)
            {
                ErrorLog("Get List Hotel by filter  Error ");
                ErrorLog(ex.Message);
            }
            return null;
        }
    }
}
