﻿/*
 *Author : Nguyễn Hoàng Khang
*/
namespace HolaBooking.Models
{
    public class AccountUser
    {
        public int Id { get; set; }
        public string ?UserName { get; set; }
        public string ?Password { get; set; }
        public int PermissionID { get; set; }
        public int Enable { get; set; }

    }
}
