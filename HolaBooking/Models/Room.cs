﻿namespace HolaBooking.Models
{
    public class Room
    {
        public int RoomId { get; set; }
        public int HotelID { get; set; }
        public String RooomType { get; set; }
        public String? Image1 { get; set; }
        public String? Image2 { get; set; }
        public String? Image3 { get; set; }
        public String? Image4 { get; set; }
        public String? Image5 { get; set; }
        public double RoomSize { get; set; }
        public Double Price { get; set; }
        public int Discount { get; set; }
        public Boolean Status { get; set; }
        public String? BedName { get; set; }
        public int Bed { get; set; }
        public int Sleep { get; set; }
        public List<Device>? ListDevices { get; set; }
        public List<Service>? ListServices { get; set; }
    }
}
