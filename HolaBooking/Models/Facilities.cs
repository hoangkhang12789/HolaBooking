﻿namespace HolaBooking.Models
{
    public class Facilities
    {
        public int HotelID { get; set; }
        public int FacilitiesID { get; set; }
        public String? FacilitiesName { get; set; }

    }
}
