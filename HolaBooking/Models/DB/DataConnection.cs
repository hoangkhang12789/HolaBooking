﻿/*
 *Author : Nguyễn Hoàng Khang
*/
using System.Data.SqlClient;
using static HolaBooking.src.Logger;
namespace HolaBooking.Models
{
    public class DataConnection
    {
        public static SqlConnection? SQL;
        public static void Connection(String ConnectionString)
        {
            try
            {
                
                SQL = new SqlConnection(ConnectionString);
                SQL.Open();
                SuccessLog("---------------------------------------------- ");
                SuccessLog(@$"------------Connect is {SQL.State} ------------------");
                SuccessLog(@$"----------- Connect to {SQL.Database} ----------- ");
                SuccessLog(@$"----------- Version: {SQL.ServerVersion} --------------");
                SuccessLog("---------------------------------------------- ");
               
            }
            catch (Exception ex)
            {
                ErrorLog("---------------------------------------------- ");
                ErrorLog("Connection failed");
                ErrorLog(ex.Message);
                ErrorLog("---------------------------------------------- ");
            }
        }
        public static void CloseConnection()
        {
            try
            {
                SQL?.Close();
                SuccessLog("---------------------------------------------- ");
                SuccessLog(@$"------------- Connect is {SQL?.State} -------------- ");
                SuccessLog("---------------------------------------------- ");
            }
            catch (SqlException ex)
            {
                ErrorLog("---------------------------------------------- ");
                ErrorLog("Close failed");
                ErrorLog(ex.Message);
                ErrorLog("---------------------------------------------- ");
            }
        }


        
    }
}
