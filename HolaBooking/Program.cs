using HolaBooking.Models;
using HolaBooking.Controllers;
using HolaBooking.SrcMomoPayment;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();
builder.Services.AddDistributedMemoryCache();
builder.Services.AddControllers(options =>
{
    options.Filters.Add(new ActionFilter());
});
builder.Services.AddSession(options =>
{
    options.IdleTimeout = TimeSpan.FromHours(1);
    options.Cookie.HttpOnly = true;
    options.Cookie.IsEssential = true;
});
var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{

    app.UseExceptionHandler("/error");
    
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}else{
    app.UseStatusCodePagesWithRedirects("/error");

}
app.UseHttpsRedirection();


app.UseStaticFiles();
app.UseSession();
app.UseRouting();
app.UseAuthorization();
app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");
MomoConnection.MomoConfig(builder.Configuration.GetConnectionString("MomoPartnerCode"),
                                builder.Configuration.GetConnectionString("MomoAccessKey"),
                                builder.Configuration.GetConnectionString("MomoSerectkey"));
DataConnection.Connection(builder.Configuration.GetConnectionString("DefaultConnection"));

app.Run();
DataConnection.CloseConnection();