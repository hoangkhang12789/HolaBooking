﻿create database HolaBooking
go

use HolaBooking
go

create table Permission (
	PermissionID int identity(1,1) primary key,
	Name nvarchar(50)
);
go

INSERT [dbo].[Permission] ([Name]) VALUES (N'Admin');
INSERT [dbo].[Permission] ([Name]) VALUES (N'Hotelier');
INSERT [dbo].[Permission] ([Name]) VALUES (N'Customer');

create table Account (
	AccountID int identity(1,1) primary key,
	Username varchar(50) unique,
	Password nvarchar(100),
	PermissionID int,
	Enable bit,
	foreign key (PermissionID) references Permission(PermissionID)
);
go

create table Profile (
	AccountID int primary key,
	Firstname nvarchar(20),
	Lastname nvarchar(20),
	Avatar nvarchar(100),
	Email nvarchar(50),
	Phone nvarchar(20),
	foreign key (AccountID) references Account(AccountID)
);
go

create table Hotel (
	HotelID int identity(1,1) primary key,
	AccountID int,
	Name nvarchar(50),
	Img1 nvarchar(100),
	Img2 nvarchar(100),
	Img3 nvarchar(100),
	Img4 nvarchar(100),
	Img5 nvarchar(100),
	Address nvarchar(100),
	District nvarchar(20),
	City nvarchar(20),
	Country nvarchar(20),
	Star float,
	Point float,
	Description ntext,
	Latitude float,
	Longitude float,
	foreign key (AccountID) references Account(AccountID)
);
go

create table RoomType (
	RoomTypeID int identity(1,1) primary key,
	Name nvarchar(50),
	BedName nvarchar(50),
	Bed int,
	Sleep int
);
go

INSERT [dbo].[RoomType] ([Name], [BedName], [Bed], [Sleep]) VALUES (N'Standard Room', N'Standard', N'1', N'1');
INSERT [dbo].[RoomType] ([Name], [BedName], [Bed], [Sleep]) VALUES (N'Superior Room', N'Queen size', N'2', N'2');
INSERT [dbo].[RoomType] ([Name], [BedName], [Bed], [Sleep]) VALUES (N'Deluxe Room', N'King size', N'2', N'2');

create table Room (
	RoomID int identity(1,1) primary key,
	HotelID int,
	RoomTypeID int,
	Img1 nvarchar(100),
	Img2 nvarchar(100),
	Img3 nvarchar(100),
	Img4 nvarchar(100),
	Img5 nvarchar(100),
	RoomSize float,
	Price float,
	Discount int,
	Status bit,
	foreign key (HotelID) references Hotel(HotelID) on delete cascade,
	foreign key (RoomTypeID) references RoomType(RoomTypeID)
);
go

create table Facilities (
	FacilitiesID int identity(1,1) primary key,
	FacilitiesName nvarchar(50)
);
go

create table Device (
	DeviceID int identity(1,1) primary key,
	DeviceName nvarchar(50)
);
go

create table Service (
	ServiceID int identity(1,1) primary key,
	ServiceName nvarchar(50),
	Price float
);
go

create table HotelFacilities (
	HotelID int,
	FacilitiesID int,
	primary key (HotelID, FacilitiesID),
	foreign key (HotelID) references Hotel(HotelID) on delete cascade,
	foreign key (FacilitiesID) references Facilities(FacilitiesID) on delete cascade
);
go

create table RoomDevice (
	RoomID int,
	DeviceID int,
	primary key (RoomID, DeviceID),
	foreign key (RoomID) references Room(RoomID) on delete cascade,
	foreign key (DeviceID) references Device(DeviceID) on delete cascade
);
go

create table RoomService (
	RoomID int,
	ServiceID int,
	ServiceDefault bit,
	primary key (RoomID, ServiceID),
	foreign key (RoomID) references Room(RoomID) on delete cascade,
	foreign key (ServiceID) references Service(ServiceID) on delete cascade
);
go

create table BookingStatus (
	BookingStatusID int identity(1,1) primary key,
	Name nvarchar(50)
);
go

INSERT [dbo].[BookingStatus] ([Name]) VALUES (N'Upcoming');
INSERT [dbo].[BookingStatus] ([Name]) VALUES (N'Completed');
INSERT [dbo].[BookingStatus] ([Name]) VALUES (N'Cancelled');

create table Booking (
	BookingID int identity(1,1) primary key,
	RoomID int,
	AccountID int,
	CheckIn nvarchar(50),
	CheckOut nvarchar(50),
	BookingStatusID int,
	Price float,
	QRCode nvarchar(100),
	foreign key (RoomID) references Room(RoomID) on delete cascade,
	foreign key (AccountID) references Account(AccountID),
	foreign key (BookingStatusID) references BookingStatus(BookingStatusID)
);
go

create table BookingService (
	BookingID int,
	ServiceID int,
	primary key (BookingID, ServiceID),
	foreign key (BookingID) references Booking(BookingID) on delete cascade,
	foreign key (ServiceID) references Service(ServiceID) on delete cascade
)
go

create table Review (
	ReviewID int identity(1,1) primary key,
	AccountID int,
	HotelID int,
	Cleanliness int,
	Facilities int,
	Location int,
	Service int,
	Text ntext,
	Time datetime,
	foreign key (AccountID) references Account(AccountID),
	foreign key (HotelID) references Hotel(HotelID) on delete cascade
);
go

/**********======Add data Account======*********/

INSERT [dbo].[Account] ([Username], [Password], [PermissionID], [Enable])
VALUES ('holabooking', N'$2a$11$E8QuNDVknFzbcjh68Z5DSuqQZbsLKMI/sOrwuSffkWLsdKUt/Utpi', N'1', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID], [Enable]) 
VALUES ('hoangkhang123', N'$2a$11$4ME0S8KSxv2zxMMZneXbmOn.cosya4.5zwbrDzYBznXGUDqHUhg/O', N'1', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID], [Enable]) 
VALUES ('huuthang123', N'$2a$11$fuq65/T9iQ6uTijCODSvI.sOBKGp4wlxh7k1VTGf5pQqhU0lblWWq', N'1', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID], [Enable]) 
VALUES ('vandoi123', N'$2a$11$x4J4LdpT44hwtaZGEZ6CQ.gvBrDMM87mAEz4b1mXxqSKc9T.sY1x6', N'1', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID], [Enable]) 
VALUES ('tannguyen123', N'$2a$11$e.nWVXn3nnZVITYsFR9KSuLSRE9xU.c3FsQ6vfE28H/iry47pkzbu', N'1', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID], [Enable]) 
VALUES ('tuanhung123', N'$2a$11$C54A3Wr0iC8/VxOJBOeAzuogFAH0ul/SxTA626vBJERb494iUP0Yi', N'1', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID], [Enable]) 
VALUES ('minhnhat123', N'$2a$11$O/aqo.zZ.yalyhYpx.TsK.S7HZm3w/lat3MppkAN4RBOX3FkFAX5a', N'3', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID], [Enable]) 
VALUES ('anhvu123', N'$2a$11$E8QuNDVknFzbcjh68Z5DSuqQZbsLKMI/sOrwuSffkWLsdKUt/Utpi', N'3', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID], [Enable]) 
VALUES ('luylda123', N'$2a$11$E8QuNDVknFzbcjh68Z5DSuqQZbsLKMI/sOrwuSffkWLsdKUt/Utpi', N'3', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID], [Enable]) 
VALUES ('thanhtruc123', N'$2a$11$E8QuNDVknFzbcjh68Z5DSuqQZbsLKMI/sOrwuSffkWLsdKUt/Utpi', N'3', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID], [Enable]) 
VALUES ('khanhduy123', N'$2a$11$E8QuNDVknFzbcjh68Z5DSuqQZbsLKMI/sOrwuSffkWLsdKUt/Utpi', N'2', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID], [Enable]) 
VALUES ('thanhcong123', N'$2a$11$E8QuNDVknFzbcjh68Z5DSuqQZbsLKMI/sOrwuSffkWLsdKUt/Utpi', N'2', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID], [Enable]) 
VALUES ('chanhtong123', N'$2a$11$E8QuNDVknFzbcjh68Z5DSuqQZbsLKMI/sOrwuSffkWLsdKUt/Utpi', N'2', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID], [Enable]) 
VALUES ('minhkhoi123', N'$2a$11$E8QuNDVknFzbcjh68Z5DSuqQZbsLKMI/sOrwuSffkWLsdKUt/Utpi', N'2', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID], [Enable]) 
VALUES ('gialoc123', N'$2a$11$E8QuNDVknFzbcjh68Z5DSuqQZbsLKMI/sOrwuSffkWLsdKUt/Utpi', N'2', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID], [Enable]) 
VALUES ('tannnguyen123', N'$2a$11$E8QuNDVknFzbcjh68Z5DSuqQZbsLKMI/sOrwuSffkWLsdKUt/Utpi', N'2', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID], [Enable]) 
VALUES ('duykhang123', N'$2a$11$E8QuNDVknFzbcjh68Z5DSuqQZbsLKMI/sOrwuSffkWLsdKUt/Utpi', N'2', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID], [Enable]) 
VALUES ('congson123', N'$2a$11$E8QuNDVknFzbcjh68Z5DSuqQZbsLKMI/sOrwuSffkWLsdKUt/Utpi', N'2', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID], [Enable]) 
VALUES ('trungthanh123', N'$2a$11$E8QuNDVknFzbcjh68Z5DSuqQZbsLKMI/sOrwuSffkWLsdKUt/Utpi', N'2', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID], [Enable]) 
VALUES ('ngocnhi123', N'$2a$11$E8QuNDVknFzbcjh68Z5DSuqQZbsLKMI/sOrwuSffkWLsdKUt/Utpi', N'2', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID], [Enable]) 
VALUES ('phuocsang123', N'$2a$11$E8QuNDVknFzbcjh68Z5DSuqQZbsLKMI/sOrwuSffkWLsdKUt/Utpi', N'2', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID], [Enable]) 
VALUES ('vinhthuan123', N'$2a$11$E8QuNDVknFzbcjh68Z5DSuqQZbsLKMI/sOrwuSffkWLsdKUt/Utpi', N'2', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID], [Enable]) 
VALUES ('vanndoi123', N'$2a$11$E8QuNDVknFzbcjh68Z5DSuqQZbsLKMI/sOrwuSffkWLsdKUt/Utpi', N'2', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID], [Enable]) 
VALUES ('vantoan123', N'$2a$11$E8QuNDVknFzbcjh68Z5DSuqQZbsLKMI/sOrwuSffkWLsdKUt/Utpi', N'2', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID], [Enable]) 
VALUES ('tuannhung123', N'$2a$11$E8QuNDVknFzbcjh68Z5DSuqQZbsLKMI/sOrwuSffkWLsdKUt/Utpi', N'2', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID], [Enable]) 
VALUES ('vanphu123', N'$2a$11$E8QuNDVknFzbcjh68Z5DSuqQZbsLKMI/sOrwuSffkWLsdKUt/Utpi', N'2', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID], [Enable]) 
VALUES ('thanhtai123', N'$2a$11$E8QuNDVknFzbcjh68Z5DSuqQZbsLKMI/sOrwuSffkWLsdKUt/Utpi', N'2', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID], [Enable]) 
VALUES ('thanhliem123', N'$2a$11$E8QuNDVknFzbcjh68Z5DSuqQZbsLKMI/sOrwuSffkWLsdKUt/Utpi', N'2', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID], [Enable]) 
VALUES ('minhtriet123', N'$2a$11$E8QuNDVknFzbcjh68Z5DSuqQZbsLKMI/sOrwuSffkWLsdKUt/Utpi', N'2', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID], [Enable]) 
VALUES ('quynhnhu123', N'$2a$11$E8QuNDVknFzbcjh68Z5DSuqQZbsLKMI/sOrwuSffkWLsdKUt/Utpi', N'2', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID], [Enable]) 
VALUES ('siben123', N'$2a$11$E8QuNDVknFzbcjh68Z5DSuqQZbsLKMI/sOrwuSffkWLsdKUt/Utpi', N'2', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID], [Enable]) 
VALUES ('suzuhara123', N'$2a$11$E8QuNDVknFzbcjh68Z5DSuqQZbsLKMI/sOrwuSffkWLsdKUt/Utpi', N'2', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID], [Enable]) 
VALUES ('utsunomiya123', N'$2a$11$E8QuNDVknFzbcjh68Z5DSuqQZbsLKMI/sOrwuSffkWLsdKUt/Utpi', N'2', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID], [Enable]) 
VALUES ('misaki123', N'$2a$11$E8QuNDVknFzbcjh68Z5DSuqQZbsLKMI/sOrwuSffkWLsdKUt/Utpi', N'2', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID], [Enable]) 
VALUES ('sakurai123', N'$2a$11$E8QuNDVknFzbcjh68Z5DSuqQZbsLKMI/sOrwuSffkWLsdKUt/Utpi', N'2', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID], [Enable]) 
VALUES ('hara123', N'$2a$11$E8QuNDVknFzbcjh68Z5DSuqQZbsLKMI/sOrwuSffkWLsdKUt/Utpi', N'2', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID], [Enable]) 
VALUES ('amami123', N'$2a$11$E8QuNDVknFzbcjh68Z5DSuqQZbsLKMI/sOrwuSffkWLsdKUt/Utpi', N'2', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID], [Enable]) 
VALUES ('hatano123', N'$2a$11$E8QuNDVknFzbcjh68Z5DSuqQZbsLKMI/sOrwuSffkWLsdKUt/Utpi', N'2', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID], [Enable]) 
VALUES ('hoaiyen123', N'$2a$11$E8QuNDVknFzbcjh68Z5DSuqQZbsLKMI/sOrwuSffkWLsdKUt/Utpi', N'2', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID], [Enable]) 
VALUES ('thulan123', N'$2a$11$E8QuNDVknFzbcjh68Z5DSuqQZbsLKMI/sOrwuSffkWLsdKUt/Utpi', N'2', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID], [Enable]) 
VALUES ('camnhiem123', N'$2a$11$E8QuNDVknFzbcjh68Z5DSuqQZbsLKMI/sOrwuSffkWLsdKUt/Utpi', N'2', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID], [Enable]) 
VALUES ('holabooking2', N'$2a$11$E8QuNDVknFzbcjh68Z5DSuqQZbsLKMI/sOrwuSffkWLsdKUt/Utpi', N'2', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID], [Enable]) 
VALUES ('holabooking3', N'$2a$11$E8QuNDVknFzbcjh68Z5DSuqQZbsLKMI/sOrwuSffkWLsdKUt/Utpi', N'3', N'1');

go

/**********======Add data Proifile======*********/

INSERT [dbo].[Profile] ([AccountID], [Firstname], [Lastname], [Avatar], [Email], [Phone]) 
VALUES (N'1', N'Hola', N'Admin', N'/image/Profile/Hola.png', N'holabooking@gmail.com', N'0123456789');
INSERT [dbo].[Profile] ([AccountID], [Firstname], [Lastname], [Avatar], [Email], [Phone]) 
VALUES (N'2', N'Hoàng Khang', N'Nguyễn', N'/image/Profile/Khang.jpg', N'hoangkhang123@gmail.com', N'0707123456');
INSERT [dbo].[Profile] ([AccountID], [Firstname], [Lastname], [Avatar], [Email], [Phone]) 
VALUES (N'3', N'Hữu Thắng', N'Lê', N'/image/Profile/Thang.jpg', N'huuthang123@gmail.com', N'0369260600');
INSERT [dbo].[Profile] ([AccountID], [Firstname], [Lastname], [Avatar], [Email], [Phone]) 
VALUES (N'4', N'Văn Đời', N'Nguyễn', N'/image/Profile/Doi.jpg', N'vandoi123@gmail.com', N'0369875412');
INSERT [dbo].[Profile] ([AccountID], [Firstname], [Lastname], [Avatar], [Email], [Phone]) 
VALUES (N'5', N'Tấn Nguyên', N'Bùi', N'/image/Profile/Nguyen.jpg', N'tannguyen123@gmail.com', N'0785496321');
INSERT [dbo].[Profile] ([AccountID], [Firstname], [Lastname], [Avatar], [Email], [Phone]) 
VALUES (N'6', N'Tuấn Hưng', N'Lê Cao', N'/image/Profile/Hung.jpg', N'tuanhung123@gmail.com', N'0903548874');
INSERT [dbo].[Profile] ([AccountID], [Firstname], [Lastname], [Avatar], [Email], [Phone]) 
VALUES (N'7', N'Minh Nhật', N'Huỳnh', N'/image/Profile/Nhat.jpg', N'minhnhat123@gmail.com', N'0795323245');
INSERT [dbo].[Profile] ([AccountID], [Firstname], [Lastname], [Avatar], [Email], [Phone]) 
VALUES (N'8', N'Anh Vũ', N'Nguyễn', N'/image/Profile/avatar5.png', N'anhvu123@gmail.com', N'0123456785');
INSERT [dbo].[Profile] ([AccountID], [Firstname], [Lastname], [Avatar], [Email], [Phone]) 
VALUES (N'9', N'Luyl Đa', N'Quách', N'/image/Profile/avatar6.png', N'luylda123@gmail.com', N'0123456784');
INSERT [dbo].[Profile] ([AccountID], [Firstname], [Lastname], [Avatar], [Email], [Phone]) 
VALUES (N'10', N'Thanh Trúc', N'Bùi Thị', N'/image/Profile/avatar7.png', N'thanhtruc123@gmail.com', N'0123456783');
INSERT [dbo].[Profile] ([AccountID], [Firstname], [Lastname], [Avatar], [Email], [Phone]) 
VALUES (N'11', N'Khánh Duy', N'Nguyễn', N'/image/Profile/avatar8.png', N'khanhduy123@gmail.com', N'0123459857');
INSERT [dbo].[Profile] ([AccountID], [Firstname], [Lastname], [Avatar], [Email], [Phone]) 
VALUES (N'12', N'Thành Công', N'Phạm', N'/image/Profile/avatar9.png', N'thanhcong123@gmail.com', N'0123457410');
INSERT [dbo].[Profile] ([AccountID], [Firstname], [Lastname], [Avatar], [Email], [Phone]) 
VALUES (N'13', N'Chánh Tông', N'Nguyễn', N'/image/Profile/avatar10.png', N'chanhtong123@gmail.com', N'0123459630');
INSERT [dbo].[Profile] ([AccountID], [Firstname], [Lastname], [Avatar], [Email], [Phone]) 
VALUES (N'14', N'Minh Khôi', N'La', N'/image/Profile/avatar11.png', N'minhkhoi123@gmail.com', N'0123445630');
INSERT [dbo].[Profile] ([AccountID], [Firstname], [Lastname], [Avatar], [Email], [Phone]) 
VALUES (N'15', N'Gia Lộc', N'Ôn', N'/image/Profile/avatar12.png', N'gialoc123@gmail.com', N'0127531630');
INSERT [dbo].[Profile] ([AccountID], [Firstname], [Lastname], [Avatar], [Email], [Phone]) 
VALUES (N'16', N'Tấn Nguyên', N'Bùi', N'/image/Profile/avatar13.png', N'tannguyen123@gmail.com', N'0123951230');
INSERT [dbo].[Profile] ([AccountID], [Firstname], [Lastname], [Avatar], [Email], [Phone]) 
VALUES (N'17', N'Duy Khang', N'Trương', N'/image/Profile/avatar14.png', N'duykhang123@gmail.com', N'0123357830');
INSERT [dbo].[Profile] ([AccountID], [Firstname], [Lastname], [Avatar], [Email], [Phone]) 
VALUES (N'18', N'Công Sơn', N'Trịnh', N'/image/Profile/avatar15.png', N'congson123@gmail.com', N'0123454530');
INSERT [dbo].[Profile] ([AccountID], [Firstname], [Lastname], [Avatar], [Email], [Phone]) 
VALUES (N'19', N'Trung Thành', N'Lâm', N'/image/Profile/avatar16.png', N'trungthanh123@gmail.com', N'0123951530');
INSERT [dbo].[Profile] ([AccountID], [Firstname], [Lastname], [Avatar], [Email], [Phone]) 
VALUES (N'20', N'Ngọc Nhi', N'Lâm', N'/image/Profile/avatar17.png', N'ngocnhi123@gmail.com', N'0123753230');
INSERT [dbo].[Profile] ([AccountID], [Firstname], [Lastname], [Avatar], [Email], [Phone]) 
VALUES (N'21', N'Phước Sang', N'Hà', N'/image/Profile/avatar18.png', N'phuocsang123@gmail.com', N'0123754130');
INSERT [dbo].[Profile] ([AccountID], [Firstname], [Lastname], [Avatar], [Email], [Phone]) 
VALUES (N'22', N'Vĩnh Thuận', N'Ca', N'/image/Profile/avatar19.png', N'vinhthuan123@gmail.com', N'0123459512');
INSERT [dbo].[Profile] ([AccountID], [Firstname], [Lastname], [Avatar], [Email], [Phone]) 
VALUES (N'23', N'Văn Đời', N'Huỳnh', N'/image/Profile/avatar20.png', N'vandoi123@gmail.com', N'0123457531');
INSERT [dbo].[Profile] ([AccountID], [Firstname], [Lastname], [Avatar], [Email], [Phone]) 
VALUES (N'24', N'Văn Toàn', N'Nguyễn', N'/image/Profile/avatar21.png', N'vantoan123@gmail.com', N'0123475341');
INSERT [dbo].[Profile] ([AccountID], [Firstname], [Lastname], [Avatar], [Email], [Phone]) 
VALUES (N'25', N'Tuấn Hưng', N'Lê Cao', N'/image/Profile/avatar22.png', N'tuanhung123@gmail.com', N'0123475469');
INSERT [dbo].[Profile] ([AccountID], [Firstname], [Lastname], [Avatar], [Email], [Phone]) 
VALUES (N'26', N'Văn Phú', N'Nguyễn', N'/image/Profile/avatar23.png', N'vanphu123@gmail.com', N'0127412531');
INSERT [dbo].[Profile] ([AccountID], [Firstname], [Lastname], [Avatar], [Email], [Phone]) 
VALUES (N'27', N'Thành Tài', N'Nguyễn', N'/image/Profile/avatar24.png', N'thanhtai123@gmail.com', N'0127412531');
INSERT [dbo].[Profile] ([AccountID], [Firstname], [Lastname], [Avatar], [Email], [Phone]) 
VALUES (N'28', N'Thanh Liêm', N'Trần', N'/image/Profile/avatar25.png', N'thanhliem123@gmail.com', N'0123458521');
INSERT [dbo].[Profile] ([AccountID], [Firstname], [Lastname], [Avatar], [Email], [Phone]) 
VALUES (N'29', N'Minh Triết', N'Trương', N'/image/Profile/avatar26.png', N'minhtriet123@gmail.com', N'0125127531');
INSERT [dbo].[Profile] ([AccountID], [Firstname], [Lastname], [Avatar], [Email], [Phone]) 
VALUES (N'30', N'Quỳnh Như', N'Nguyễn', N'/image/Profile/avatar27.png', N'quynhnhu123@gmail.com', N'0123751431');
INSERT [dbo].[Profile] ([AccountID], [Firstname], [Lastname], [Avatar], [Email], [Phone]) 
VALUES (N'31', N'Sĩ Ben', N'Nguyễn', N'/image/Profile/avatar28.png', N'siben123@gmail.com', N'0125275531');
INSERT [dbo].[Profile] ([AccountID], [Firstname], [Lastname], [Avatar], [Email], [Phone]) 
VALUES (N'32', N'Emiri ', N'Suzuhara', N'/image/Profile/avatar29.png', N'suzuhara123@gmail.com', N'0124132431');
INSERT [dbo].[Profile] ([AccountID], [Firstname], [Lastname], [Avatar], [Email], [Phone]) 
VALUES (N'33', N'Shion ', N'Utsunomiya', N'/image/Profile/avatar30.png', N'utsunomiya123@gmail.com', N'0125651531');
INSERT [dbo].[Profile] ([AccountID], [Firstname], [Lastname], [Avatar], [Email], [Phone]) 
VALUES (N'34', N'Rola ', N'Misaki', N'/image/Profile/avatar31.png', N'misaki123@gmail.com', N'0127536531');
INSERT [dbo].[Profile] ([AccountID], [Firstname], [Lastname], [Avatar], [Email], [Phone]) 
VALUES (N'35', N'Ria ', N'Sakurai', N'/image/Profile/avatar32.png', N'sakurai123@gmail.com', N'0124569531');
INSERT [dbo].[Profile] ([AccountID], [Firstname], [Lastname], [Avatar], [Email], [Phone]) 
VALUES (N'36', N'Saori ', N'Hara', N'/image/Profile/avatar33.png', N'hara123@gmail.com', N'0125255731');
INSERT [dbo].[Profile] ([AccountID], [Firstname], [Lastname], [Avatar], [Email], [Phone]) 
VALUES (N'37', N'Tsubasa ', N'Amami', N'/image/Profile/avatar34.png', N'amami123@gmail.com', N'0125275531');
INSERT [dbo].[Profile] ([AccountID], [Firstname], [Lastname], [Avatar], [Email], [Phone]) 
VALUES (N'38', N'Yui ', N'Hatano', N'/image/Profile/avatar35.png', N'hatano123@gmail.com', N'0129512331');
INSERT [dbo].[Profile] ([AccountID], [Firstname], [Lastname], [Avatar], [Email], [Phone]) 
VALUES (N'39', N'Hoài Yến', N'Lê Võ', N'/image/Profile/avatar36.png', N'hoaiyen123@gmail.com', N'0795413192');
INSERT [dbo].[Profile] ([AccountID], [Firstname], [Lastname], [Avatar], [Email], [Phone]) 
VALUES (N'40', N'Thu Lan', N'Lê Thị', N'/image/Profile/avatar37.png', N'thulan123@gmail.com', N'0123584639');
INSERT [dbo].[Profile] ([AccountID], [Firstname], [Lastname], [Avatar], [Email], [Phone]) 
VALUES (N'41', N'Cẩm Nhiêm', N'Nguyễn', N'/image/Profile/avatar38.png', N'camnhiem123@gmail.com', N'0126589434');
INSERT [dbo].[Profile] ([AccountID], [Firstname], [Lastname], [Avatar], [Email], [Phone]) 
VALUES (N'42', N'Hola', N'Hotelier', N'/image/Profile/Hola.png', N'holabooking@gmail.com', N'0123456789');
INSERT [dbo].[Profile] ([AccountID], [Firstname], [Lastname], [Avatar], [Email], [Phone]) 
VALUES (N'43', N'Hola', N'Customer', N'/image/Profile/Hola.png', N'holabooking@gmail.com', N'0123456789');


/**********======Add data Hotel======*********/

INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude])
VALUES (N'42', N'Blue Fingers Masteri An Phu', N'/image/Hotel/hotel1.jpg', N'/image/Hotel/hotel2.jpg', N'/image/Hotel/hotel3.jpg', N'/image/Hotel/hotel4.jpg', N'/image/Hotel/hotel5.jpg', N'179 Xa lộ Hà Nội Masteri An Phu', N'District 2', N'Ho Chi Minh', N'Vietnam', N'4', N'9.2', N'Located 4 km from Landmark 81, Blue Fingers Masteri An Phu offers an outdoor swimming pool, a fitness centre and air-conditioned accommodation with a balcony and free WiFi. The apartment provides guests with a patio, garden views, a seating area, satellite flat-screen TV, a fully equipped kitchen with a microwave and a fridge, and a private bathroom with bidet and slippers. For added convenience, the property can provide towels and bed linen for an extra charge.', N'10.8035357', N'106.7475951');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'17', N'Aristo Saigon Hotel', N'/image/Hotel/hotel6.jpg', N'/image/Hotel/hotel7.jpg', N'/image/Hotel/hotel8.jpg', N'/image/Hotel/hotel9.jpg', N'/image/Hotel/hotel10.jpg', N'3A Vo Van Tan', N'District 3', N'Ho Chi Minh', N'Vietnam', N'4', N'8.1', N'Aristo Hotel is located in Ho Chi Minh City, 2 km from Ben Thanh Market. Offering rooms with free WiFi, it features a spa, a gym and a restaurant. Aristo Hotel is a 20-minute drive from Tan Son Nhat International Airport. The Reunification Palace is only 200 m away, while Notre Dame Cathedral is 400 m away. All rooms are air conditioned and come with a flat-screen satellite TV, a minibar and a seating area. Private bathrooms come with a bathtub and a hairdryer. Services provided by the hotel include currency exchange, laundry and car rentals. Both local and international meals and beverages are served at DeliLah Restaurant for guests’ convenience.', N'10.7809747', N'106.6947094');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'42', N'Le Petit Paris Dalat Hotel', N'/image/Hotel/hotel11.jpg', N'/image/Hotel/hotel12.jpg', N'/image/Hotel/hotel12.jpg', N'/image/Hotel/hotel14.jpg', N'/image/Hotel/hotel15.jpg', N'5 Mai Hac De', N'', N'Da Lat', N'Vietnam', N'3', N'8.3', N'Featuring a beautiful courtyard garden, Le Petit Paris Dalat Hotel offers peaceful and elegant accommodation with free wired internet access in the guestrooms. It operates a 24-hour front desk, houses a restaurant and provides complimentary parking space on site. Fitted with tiled flooring, rooms include a clothes rack, personal safe, a cable TV, minibar and telephone. The attached bathroom comes with shower facility, slippers, towels and hairdryer. At Le Petit Paris Dalat Hotel, guests may rent a bicycle/car to explore the area and visit nearby attractions. Friendly staff can assist with luggage storage, airport transfers and laundry/ironing services. The hotel is about 1.9 km from Xuan Huong Lake and 2.2 km from Dalat Flower Gardens. Truc Lam Temple is 5.1 km away, while Buon Ma Thuot Airport is approximately 87 km away.', N'11.9465187', N'108.429099');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'19', N'Nhat Nga Hotel', N'/image/Hotel/hotel16.jpg', N'/image/Hotel/hotel17.jpg', N'/image/Hotel/hotel18.jpg', N'/image/Hotel/hotel19.jpg', N'/image/Hotel/hotel20.jpg', N'570 Nguyen An Ninh', N'', N'Vung Tau', N'Vietnam', N'2', N'8.2', N'features accommodation with a bar, free private parking, a shared lounge and a garden. This 2-star hotel offers room service and a concierge service. The accommodation provides a 24-hour front desk, airport transfers, a shared kitchen and free WiFi throughout the property. Complete with a private bathroom fitted with slippers, the rooms at the hotel have a flat-screen TV and air conditioning, and certain rooms have a seating area. All rooms will provide guests with a wardrobe and a kettle. Nhat Nga Hotel offers a terrace. Guests at the accommodation will be able to enjoy activities in and around Vung Tau, like cycling.', N'10.3601122', N'107.0972806');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'20', N'Somerset Feliz Ho Chi Minh City', N'/image/Hotel/hotel21.jpg', N'/image/Hotel/hotel22.jpg', N'/image/Hotel/hotel23.jpg', N'/image/Hotel/hotel24.jpg', N'/image/Hotel/hotel25.jpg', N'No. 1 Phan Van Dang Street', N'District 2', N'Ho Chi Minh', N'Vietnam', N'4', N'9.3', N'Providing an outdoor swimming pool, Somerset Feliz Ho Chi Minh City is located in the District 2 district of Ho Chi Minh City, 4.2 km from Landmark 81 and 6 km from Nha Rong Wharf.The air-conditioned units feature a living room with a flat-screen TV, a kitchen with a fridge, a washing machine, a safety deposit box, and a private bathroom with bathrobes and slippers.Vietnam History Museum is 6 km from the apartment, while Saigon Opera House is 6 km away. The nearest airport is Tan Son Nhat International Airport, 12 km from Somerset Feliz Ho Chi Minh City.', N'10.7768675', N'106.7550019');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'21', N'Aloha aparthotel - City centre', N'/image/Hotel/hotel26.jpg', N'/image/Hotel/hotel27.jpg', N'/image/Hotel/hotel28.jpg', N'/image/Hotel/hotel29.jpg', N'/image/Hotel/hotel30.jpg', N'283/42 CMT8, P12', N'District 10', N'Ho Chi Minh', N'Vietnam', N'3', N'9.5', N'Aloha aparthotel - City centre is situated in the District 10 district of Ho Chi Minh City, 1.4 km from War Remnants Museum and 1.4 km from Tao Dan Park.Accommodation is fitted with air conditioning, a fully equipped kitchen, a flat-screen TV and a private bathroom with shower, slippers and a hairdryer. A microwave, a fridge and stovetop are also available, as well as a kettle.Popular points of interest near the aparthotel include Tan Dinh Market, Reunification Palace and Ben Thanh Street Food Market. The nearest airport is Tan Son Nhat International Airport, 5 km from Aloha aparthotel - City centre.', N'10.7779631', N'106.6783721');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'22', N'The Art - Thang 5 House Da Lat', N'/image/Hotel/hotel31.jpg', N'/image/Hotel/hotel32.jpg', N'/image/Hotel/hotel33.jpg', N'/image/Hotel/hotel34.jpg', N'/image/Hotel/hotel35.jpg', N'A31, Khu Quy Hoạch, Phan Đình Phùng', N'', N'Da Lat', N'Vietnam', N'2', N'9.1', N'Situated in Da Lat, within 2.5 km of Lam Vien Square and 2.7 km of Xuan Huong Lake, The Art - Thang 5 House Da Lat features accommodation with a garden and free WiFi throughout the property as well as free private parking for guests who drive. Boasting family rooms, this property also provides guests with a terrace. The accommodation offers a 24-hour front desk, room service and luggage storage for guests.', N'11.9487095', N'108.4355849');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'23', N'The 1954 House', N'/image/Hotel/hotel36.jpg', N'/image/Hotel/hotel37.jpg', N'/image/Hotel/hotel38.jpg', N'/image/Hotel/hotel39.jpg', N'/image/Hotel/hotel40.jpg', N'134 Hà Huy Tập', N'', N'Da Lat', N'Vietnam', N'3', N'9.3', N'Located in Da Lat in the Lam Dong region, with Lam Vien Square nearby, The 1954 House provides accommodation with free WiFi and free private parking.All units feature a flat-screen TV, a private bathroom and a fully equipped kitchen.A terrace can be found at the holiday home, along with a garden.Yersin Park Da Lat is 2 km from The 1954 House, while Xuan Huong Lake is 2.1 km away. The nearest airport is Lien Khuong Airport, 28 km from the accommodation.', N'11.9313682', N'108.4398898');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'24', N'HOMEY - Feels like home', N'/image/Hotel/hotel41.jpg', N'/image/Hotel/hotel42.jpg', N'/image/Hotel/hotel43.jpg', N'/image/Hotel/hotel44.jpg', N'/image/Hotel/hotel45.jpg', N'Đường Mạc Đĩnh Chi 17/1', N'', N'Da Lat', N'Vietnam', N'3', N'9.5', N'Set in Da Lat, 2.8 km from Lam Vien Square, HOMEY - Feels like home offers accommodation with a bar, free private parking, a garden and a terrace. Located around 3 km from Xuan Huong Lake, the guest house with free WiFi is also 3.1 km away from Yersin Park Da Lat. The guest house has family rooms.At the guest house the rooms are equipped with a seating area, a flat-screen TV with cable channels, a kitchen, a dining area and a private bathroom with slippers, a shower and a hairdryer. Every room comes with a kettle, while certain rooms also offer a patio and others also feature garden views. At HOMEY - Feels like home each room is fitted with bed linen and towels.', N'11.9402332', N'108.4296744');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'42', N'Apartment OSC Vung Tau', N'/image/Hotel/hotel46.jpg', N'/image/Hotel/hotel47.jpg', N'/image/Hotel/hotel48.jpg', N'/image/Hotel/hotel49.jpg', N'/image/Hotel/hotel50.jpg', N'110 Võ Thị Sáu, tầng 15, Block A', N'', N'Vung Tau', N'Vietnam', N'3', N'8.4', N'This property is 10 minutes walk from the beach. Situated in Vung Tau, Apartment OSC Vung Tau near Christ of Vung Tau, Vung Tau Lighthouse and Nghinh Phong Cape. It takes 3 minutes to drive, or 8 minutes on foot, to the nearest beach. The units comprise of master bedrooms, a living room and kitchen. The apartment offers an outdoor pool.', N'10.3433888', N'107.0900721');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'26', N'Pink Host', N'/image/Hotel/hotel51.jpg', N'/image/Hotel/hotel52.jpg', N'/image/Hotel/hotel53.jpg', N'/image/Hotel/hotel54.jpg', N'/image/Hotel/hotel55.jpg', N'30 Bàu Sen 1', N'', N'Vung Tau', N'Vietnam', N'1', N'8.9', N'Pink Host is located in Vung Tau, 1.8 km from Christ of Vung Tau and 2.3 km from Nghinh Phong Cape. This 1-star hotel offers a 24-hour front desk, room service and free WiFi. The hotel features family rooms.Guest rooms in the hotel are equipped with a kettle. Each room comes with air conditioning and a flat-screen TV, and certain rooms at Pink Host have a balcony.', N'10.3384571', N'107.0846962');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'27', N'Minh Hải Hotel', N'/image/Hotel/hotel56.jpg', N'/image/Hotel/hotel57.jpg', N'/image/Hotel/hotel58.jpg', N'/image/Hotel/hotel59.jpg', N'/image/Hotel/hotel60.jpg', N'03, La văn cầu, Đường Thuỳ vân', N'', N'Vung Tau', N'Vietnam', N'1', N'9.0', N'This property is 1 minute walk from the beach. Set within 1.8 km of Christ of Vung Tau in Vung Tau, Minh Hải Hotel provides a terrace and a shared lounge. Located around 1.8 km from Vung Tau Lighthouse, the hotel with free WiFi is also 2.4 km away from Nghinh Phong Cape. The property features a 24-hour front desk.At the hotel, all rooms include a desk. Featuring a private bathroom, certain rooms at Minh Hải Hotel also provide guests with a balcony. All guest rooms at the accommodation include air conditioning and a wardrobe.', N'10.3375316', N'107.0910144');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'42', N'CT Morning Hostel', N'/image/Hotel/hotel61.jpg', N'/image/Hotel/hotel62.jpg', N'/image/Hotel/hotel63.jpg', N'/image/Hotel/hotel64.jpg', N'/image/Hotel/hotel65.jpg', N'81 Lý Hồng Thanh', N'', N'Can Tho', N'Vietnam', N'2', N'9.1', N'Featuring a terrace, CT Morning Hostel is set in Can Tho in the Can Tho Municipality region, less than 1 km from Vincom Plaza Hung Vuong and 1.8 km from Ninh Kieu Pier. This 2-star hotel offers room service and a shared lounge. The accommodation offers a 24-hour front desk, airport transfers, a shared kitchen and free WiFi throughout the property.The units come with air conditioning, a flat-screen TV with cable channels, a kettle, a bidet, slippers and a wardrobe. The hotel features certain units that feature a patio, and all rooms are equipped with a private bathroom with a shower and free toiletries. At CT Morning Hostel rooms include bed linen and towels.', N'10.0452578', N'105.7843104');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'29', N'Little Dalat Coffee and Homestay', N'/image/Hotel/hotel66.jpg', N'/image/Hotel/hotel67.jpg', N'/image/Hotel/hotel68.jpg', N'/image/Hotel/hotel69.jpg', N'/image/Hotel/hotel70.jpg', N'Đường 4, Khu Dân cư Hồng Phát', N'', N'Can Tho', N'Vietnam', N'3', N'8.6', N'Set 3.8 km from Lotte Mart Can Tho, Little Dalat Coffee and Homestay offers accommodation with a restaurant, a garden and a 24-hour front desk for your convenience. The apartment features both WiFi and private parking free of charge.Offering a patio, some units are air conditioned and feature a dining area and a seating area with a cable flat-screen TV. Some units also have a kitchenette equipped with a fridge, an oven, and a stovetop. à la carte and Asian breakfast options are available every morning at Little Dalat Coffee and Homestay. A sun terrace is available for guests to use at the accommodation.', N'10.0262958', N'105.7486895');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'30', N'Đại Quang Hotel', N'/image/Hotel/hotel71.jpg', N'/image/Hotel/hotel72.jpg', N'/image/Hotel/hotel73.jpg', N'/image/Hotel/hotel74.jpg', N'/image/Hotel/hotel75.jpg', N'10B Đường số 3 KDC Hồng Phát', N'Ninh Kiều', N'Can Tho', N'Vietnam', N'1', N'9.2', N'Boasting a garden, Đại Quang Hotel is situated in Can Tho in the Can Tho Municipality region, 3.8 km from Lotte Mart Can Tho and 4.1 km from Vincom Plaza Xuan Khanh. The property is around 5 km from Cai Rang Floating Market, 6 km from Vincom Plaza Hung Vuong and 6 km from Ninh Kieu Pier. The accommodation features room service, and currency exchange for guests.', N'10.0262958', N'105.7486895');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'31', N'Can Tho Ecolodge', N'/image/Hotel/hotel76.jpg', N'/image/Hotel/hotel77.jpg', N'/image/Hotel/hotel78.jpg', N'/image/Hotel/hotel79.jpg', N'/image/Hotel/hotel80.jpg', N'542, Area No.3, Ba Lang Ward', N'Cai Rang', N'Can Tho', N'Vietnam', N'4', N'8.7', N'Can Tho Ecolodge features free bicycles, an outdoor swimming pool, a garden and bar in Can Tho. Among the facilities of this property are a restaurant, a 24-hour front desk and room service, along with free WiFi. The rooms are equipped with a balcony with a garden view. All guest rooms at the resort are equipped with a seating area. Each room is equipped with a private bathroom with a bath, a hair dryer and free toiletries. Guest rooms at Can Tho Ecolodge include air conditioning and a desk. A buffet breakfast is available daily at the accommodation.', N'9.9832675', N'105.7380511');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'32', N'Silverland Yen Hotel', N'/image/Hotel/hotel81.jpg', N'/image/Hotel/hotel82.jpg', N'/image/Hotel/hotel83.jpg', N'/image/Hotel/hotel84.jpg', N'/image/Hotel/hotel85.jpg', N'73-75 Thu Khoa Huan Street, Ben Thanh Ward', N'Quan 1', N'Ho Chi Minh', N'Vietnam', N'4', N'8.9', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'10.7746564', N'106.696226');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'33', N'Mai House Saigon Hotel', N'/image/Hotel/hotel86.jpg', N'/image/Hotel/hotel87.jpg', N'/image/Hotel/hotel88.jpg', N'/image/Hotel/hotel89.jpg', N'/image/Hotel/hotel90.jpg', N'01 Ngo Thoi Nhiem Stress, Vo Thi Sau Ward', N'Quan 3', N'Ho Chi Minh', N'Vietnam', N'5', N'9.2', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'10.7822684', N'106.691911');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'34', N'Cozrum Homes Premier Residences', N'/image/Hotel/hotel91.jpg', N'/image/Hotel/hotel92.jpg', N'/image/Hotel/hotel93.jpg', N'/image/Hotel/hotel94.jpg', N'/image/Hotel/hotel95.jpg', N'', N'Binh Thanh', N'Ho Chi Minh', N'Vietnam', N'4', N'8.7', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'10.8105831', N'106.7091422');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'35', N'Central Palace Hotel', N'/image/Hotel/hotel96.jpg', N'/image/Hotel/hotel97.jpg', N'/image/Hotel/hotel98.jpg', N'/image/Hotel/hotel99.jpg', N'/image/Hotel/hotel100.jpg', N'39-39A Nguyen Trung Truc Street, Ben Thanh Ward', N'Quan 1', N'Ho Chi Minh', N'Vietnam', N'4', N'8.5', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'10.7755449', N'106.6970977');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'16', N'Silverland Sakyo Hotel', N'/image/Hotel/hotel101.jpg', N'/image/Hotel/hotel102.jpg', N'/image/Hotel/hotel103.jpg', N'/image/Hotel/hotel104.jpg', N'/image/Hotel/hotel105.jpg', N'10A Le Thanh Ton Street', N'Quan 1', N'Ho Chi Minh', N'Vietnam', N'4', N'8.6', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'10.781647', N'106.7054278');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'17', N'Infinity Pool Signature - GYM&Pool&Netflix 1', N'/image/Hotel/hotel106.jpg', N'/image/Hotel/hotel107.jpg', N'/image/Hotel/hotel108.jpg', N'/image/Hotel/hotel109.jpg', N'/image/Hotel/hotel110.jpg', N'', N'Quan 4', N'Ho Chi Minh', N'Vietnam', N'5', N'9.6', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'10.7578263', N'106.7012968');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'18', N'Hôtel Colline', N'/image/Hotel/hotel111.jpg', N'/image/Hotel/hotel112.jpg', N'/image/Hotel/hotel113.jpg', N'/image/Hotel/hotel114.jpg', N'/image/Hotel/hotel115.jpg', N'10 Phan Boi Chau Street, Ward 1', N'', N'Da Lat', N'Vietnam', N'4', N'8.6', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'11.9439433', N'108.4380946');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'19', N'La Fleur Premium Central Apartment Dalat', N'/image/Hotel/hotel116.jpg', N'/image/Hotel/hotel117.jpg', N'/image/Hotel/hotel118.jpg', N'/image/Hotel/hotel119.jpg', N'/image/Hotel/hotel120.jpg', N'Dalat City Center', N'', N'Da Lat', N'Vietnam', N'5', N'9.7', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'11.9438638', N'108.4380906');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'20', N'Ladalat Hotel', N'/image/Hotel/hotel121.jpg', N'/image/Hotel/hotel122.jpg', N'/image/Hotel/hotel123.jpg', N'/image/Hotel/hotel124.jpg', N'/image/Hotel/hotel125.jpg', N'106A Mai Anh Dao Street, Ward 8', N'', N'Da Lat', N'Vietnam', N'5', N'8.4', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'119.777512', N'108.4511382');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'21', N'Ana Mandara Villas Dalat Resort & Spa', N'/image/Hotel/hotel126.jpg', N'/image/Hotel/hotel127.jpg', N'/image/Hotel/hotel128.jpg', N'/image/Hotel/hotel129.jpg', N'/image/Hotel/hotel130.jpg', N'Le Lai Street, Ward 5', N'', N'Da Lat', N'Vietnam', N'5', N'8.7', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'10.3756812', N'107.062802');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'22', N'Golf Valley Hotel', N'/image/Hotel/hotel131.jpg', N'/image/Hotel/hotel132.jpg', N'/image/Hotel/hotel133.jpg', N'/image/Hotel/hotel134.jpg', N'/image/Hotel/hotel135.jpg', N'94 Bui Thi Xuan, Dalat City Center', N'', N'Da Lat', N'Vietnam', N'4', N'8.7', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'11.9473708', N'108.4405161');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'23', N'Le Recit Boutique Hotel de Dalat', N'/image/Hotel/hotel136.jpg', N'/image/Hotel/hotel137.jpg', N'/image/Hotel/hotel138.jpg', N'/image/Hotel/hotel139.jpg', N'/image/Hotel/hotel140.jpg', N'117 Bui Thi Xuan Street, Ward 02', N'', N'Da Lat', N'Vietnam', N'3', N'9.2', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'11.9473819', N'108.4402382');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'24', N'Fusion Suites Vung Tau', N'/image/Hotel/hotel141.jpg', N'/image/Hotel/hotel142.jpg', N'/image/Hotel/hotel143.jpg', N'/image/Hotel/hotel144.jpg', N'/image/Hotel/hotel145.jpg', N'02 Truong Cong Dinh, Phuong 2', N'', N'Vung Tau', N'Vietnam', N'4', N'8.4', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'10.3415582', N'107.075169');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'25', N'ibis Styles Vung Tau', N'/image/Hotel/hotel146.jpg', N'/image/Hotel/hotel147.jpg', N'/image/Hotel/hotel148.jpg', N'/image/Hotel/hotel149.jpg', N'/image/Hotel/hotel150.jpg', N'117 Thuy Van Thang Tam Ward, Thang Tam', N'', N'Vung Tau', N'Vietnam', N'3', N'8.9', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'10.3364525', N'107.0903641');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'26', N'Kieu Anh Hotel', N'/image/Hotel/hotel151.jpg', N'/image/Hotel/hotel152.jpg', N'/image/Hotel/hotel153.jpg', N'/image/Hotel/hotel154.jpg', N'/image/Hotel/hotel155.jpg', N'257 Le Hong Phong Street, Thang Tam', N'', N'Vung Tau', N'Vietnam', N'3', N'7.2', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'10.3499486', N'107.091905');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'27', N'Malibu Hotel', N'/image/Hotel/hotel156.jpg', N'/image/Hotel/hotel157.jpg', N'/image/Hotel/hotel158.jpg', N'/image/Hotel/hotel159.jpg', N'/image/Hotel/hotel160.jpg', N'263 Le Hong Phong, Thang Tam Ward', N'', N'Vung Tau', N'Vietnam', N'4', N'8.8', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'10.3474874', N'107.0952518');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'28', N'Luxury Xuân s Homestay Vung Tau', N'/image/Hotel/hotel161.jpg', N'/image/Hotel/hotel162.jpg', N'/image/Hotel/hotel163.jpg', N'/image/Hotel/hotel164.jpg', N'/image/Hotel/hotel165.jpg', N'Phuong 2', N'', N'Vung Tau', N'Vietnam', N'5', N'9.5', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'10.3340595', N'107.0851934');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'29', N'Melia Ho Tram Beach Resort', N'/image/Hotel/hotel166.jpg', N'/image/Hotel/hotel167.jpg', N'/image/Hotel/hotel168.jpg', N'/image/Hotel/hotel169.jpg', N'/image/Hotel/hotel170.jpg', N'Coastal Road, Ho Tram Hamlet, Phuoc Thuan Commune', N'Xuyen Moc', N'Vung Tau', N'Vietnam', N'5', N'8.8', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'10.4725294', N'107.4362693');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'30', N'Vinpearl Hotel Can Tho', N'/image/Hotel/hotel171.jpg', N'/image/Hotel/hotel172.jpg', N'/image/Hotel/hotel173.jpg', N'/image/Hotel/hotel174.jpg', N'/image/Hotel/hotel175.jpg', N'No.209, 30/4 Street', N'Ninh Kieu', N'Can Tho', N'Vietnam', N'5', N'9.2', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'10.024937', N'105.774633');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'31', N'Muong Thanh Luxury Can Tho Hotel', N'/image/Hotel/hotel176.jpg', N'/image/Hotel/hotel177.jpg', N'/image/Hotel/hotel178.jpg', N'/image/Hotel/hotel179.jpg', N'/image/Hotel/hotel180.jpg', N'Area E1, Cai Khe River Islet, Cai Khe Ward', N'', N'Can Tho', N'Vietnam', N'5', N'8.2', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'10.0485897', N'105.7879371');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'32', N'Apple Hotel', N'/image/Hotel/hotel181.jpg', N'/image/Hotel/hotel182.jpg', N'/image/Hotel/hotel183.jpg', N'/image/Hotel/hotel184.jpg', N'/image/Hotel/hotel185.jpg', N'431, 30/4 Street, Hung Loi Ward', N'Ninh Kieu', N'Can Tho', N'Vietnam', N'3', N'8.7', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'10.0213532', N'105.7700757');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'33', N'Ninh Kieu Riverside Hotel', N'/image/Hotel/hotel186.jpg', N'/image/Hotel/hotel187.jpg', N'/image/Hotel/hotel188.jpg', N'/image/Hotel/hotel189.jpg', N'/image/Hotel/hotel190.jpg', N'02 Hai Ba Trung St.', N'Ninh Kieu', N'Can Tho', N'Vietnam', N'4', N'8.3', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'10.0354858', N'105.7891881');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'34', N'TTC Hotel Can Tho', N'/image/Hotel/hotel191.jpg', N'/image/Hotel/hotel192.jpg', N'/image/Hotel/hotel193.jpg', N'/image/Hotel/hotel194.jpg', N'/image/Hotel/hotel195.jpg', N'2 Hai Ba Trung St.', N'Ninh Kieu', N'Can Tho', N'Vietnam', N'4', N'8.5', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'10.035853', N'105.789867');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'35', N'Iris Hotel Can Tho', N'/image/Hotel/hotel196.jpg', N'/image/Hotel/hotel197.jpg', N'/image/Hotel/hotel198.jpg', N'/image/Hotel/hotel199.jpg', N'/image/Hotel/hotel200.jpg', N'224, 30/4 street, Xuan Khanh Ward', N'Ninh Kieu', N'Can Tho', N'Vietnam', N'4', N'8.1', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'10.0306164', N'105.7673942');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'16', N'Acoustic Hotel & Spa', N'/image/Hotel/hotel201.jpg', N'/image/Hotel/hotel202.jpg', N'/image/Hotel/hotel203.jpg', N'/image/Hotel/hotel204.jpg', N'/image/Hotel/hotel205.jpg', N'39 Tho Nhuom', N'Hoan Kiem', N'Ha Noi', N'Vietnam', N'4', N'9.4', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'21.0276333', N'105.844568');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'17', N'Hanoi La Siesta Premium Hang Be', N'/image/Hotel/hotel206.jpg', N'/image/Hotel/hotel207.jpg', N'/image/Hotel/hotel208.jpg', N'/image/Hotel/hotel209.jpg', N'/image/Hotel/hotel210.jpg', N'27 Hang Be, Old Quarter', N'', N'Ha Noi', N'Vietnam', N'4', N'9.4', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'21.0331202', N'105.8540981');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'18', N'Scent Premium Hotel', N'/image/Hotel/hotel211.jpg', N'/image/Hotel/hotel212.jpg', N'/image/Hotel/hotel213.jpg', N'/image/Hotel/hotel214.jpg', N'/image/Hotel/hotel215.jpg', N'31 Hang Hanh', N'Hoan Kiem', N'Ha Noi', N'Vietnam', N'4', N'9.7', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'21.0313052', N'105.8501633');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'19', N'MOON West Lake Serviced Apartment', N'/image/Hotel/hotel216.jpg', N'/image/Hotel/hotel217.jpg', N'/image/Hotel/hotel218.jpg', N'/image/Hotel/hotel219.jpg', N'/image/Hotel/hotel220.jpg', N'', N'Tay Ho', N'Ha Noi', N'Vietnam', N'4', N'8.4', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'21.0811211', N'105.8180306');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'20', N'Imperial Hotel & Spa', N'/image/Hotel/hotel221.jpg', N'/image/Hotel/hotel222.jpg', N'/image/Hotel/hotel223.jpg', N'/image/Hotel/hotel224.jpg', N'/image/Hotel/hotel225.jpg', N'44 Hang Hanh Street', N'Hoan Kiem', N'Ha Noi', N'Vietnam', N'4', N'9.5', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'21.0311291', N'105.8498472');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'21', N'Solaria Hanoi Hotel', N'/image/Hotel/hotel226.jpg', N'/image/Hotel/hotel227.jpg', N'/image/Hotel/hotel228.jpg', N'/image/Hotel/hotel229.jpg', N'/image/Hotel/hotel230.jpg', N'22 Phố Báo Khánh', N'Hoan Kiem', N'Ha Noi', N'Vietnam', N'4', N'9.4', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'21.030185', N'105.8503479');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'22', N'IREST APARTMENT', N'/image/Hotel/hotel231.jpg', N'/image/Hotel/hotel232.jpg', N'/image/Hotel/hotel233.jpg', N'/image/Hotel/hotel234.jpg', N'/image/Hotel/hotel235.jpg', N'', N'Dong Da', N'Ha Noi', N'Vietnam', N'4', N'9.0', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'21.0180725', N'105.8299495');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'23', N'La Sinfonia del Rey Hotel and Spa', N'/image/Hotel/hotel236.jpg', N'/image/Hotel/hotel237.jpg', N'/image/Hotel/hotel238.jpg', N'/image/Hotel/hotel239.jpg', N'/image/Hotel/hotel240.jpg', N'Hang Dau', N'Hoan Kiem', N'Ha Noi', N'Vietnam', N'4', N'9.4', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'21.0312796', N'105.8539261');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'24', N'Hanoi Center Silk Hotel', N'/image/Hotel/hotel241.jpg', N'/image/Hotel/hotel242.jpg', N'/image/Hotel/hotel243.jpg', N'/image/Hotel/hotel244.jpg', N'/image/Hotel/hotel245.jpg', N'22A Ta Hien Street, old quarter', N'Hoan Kiem', N'Ha Noi', N'Vietnam', N'3', N'9.0', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'21.0344384', N'105.8522746');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'25', N'Movenpick Hotel Hanoi', N'/image/Hotel/hotel246.jpg', N'/image/Hotel/hotel247.jpg', N'/image/Hotel/hotel248.jpg', N'/image/Hotel/hotel249.jpg', N'/image/Hotel/hotel250.jpg', N'83a Lý Thường Kiệt', N'Hoan Kiem', N'Ha Noi', N'Vietnam', N'5', N'8.4', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'21.0246838', N'105.8442813');

/**************************************************************************************************************************/

INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'26', N'Dusit Princess Moonrise Beach Resort', N'/image/Hotel/hotel251.jpg', N'/image/Hotel/hotel252.jpg', N'/image/Hotel/hotel253.jpg', N'/image/Hotel/hotel254.jpg', N'/image/Hotel/hotel255.jpg', N'Tran Hung Dao Street, Group 2, Cua Lap Village', N'', N'Phu Quoc Island', N'Vietnam', N'5', N'3.5', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'27', N'Green Bay Phu Quoc Resort and Spa', N'/image/Hotel/hotel256.jpg', N'/image/Hotel/hotel257.jpg', N'/image/Hotel/hotel258.jpg', N'/image/Hotel/hotel259.jpg', N'/image/Hotel/hotel260.jpg', N'Cua Can Hamlet, Cua Can', N'', N'Phu Quoc Island', N'Vietnam', N'4', N'8.9', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'28', N'Crowne Plaza Phu Quoc Starbay', N'/image/Hotel/hotel261.jpg', N'/image/Hotel/hotel262.jpg', N'/image/Hotel/hotel263.jpg', N'/image/Hotel/hotel264.jpg', N'/image/Hotel/hotel265.jpg', N'Khu Du Lich Bai Dai, Ganh Dau Commune', N'', N'Phu Quoc Island', N'Vietnam', N'5', N'8.9', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'29', N'Mövenpick Resort Waverly Phu Quoc', N'/image/Hotel/hotel266.jpg', N'/image/Hotel/hotel267.jpg', N'/image/Hotel/hotel268.jpg', N'/image/Hotel/hotel269.jpg', N'/image/Hotel/hotel270.jpg', N'Ong Lang Hamlet Cua Duong Village', N'', N'Phu Quoc Island', N'Vietnam', N'5', N'8.7', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'30', N'Paralia Khem Beach Phu Quoc Hotel', N'/image/Hotel/hotel271.jpg', N'/image/Hotel/hotel272.jpg', N'/image/Hotel/hotel273.jpg', N'/image/Hotel/hotel274.jpg', N'/image/Hotel/hotel275.jpg', N'D107-115, Sun Premier Village Khem Beach Resort Complex, Bai Khem', N'', N'Phu Quoc Island', N'Vietnam', N'4', N'9.0', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'31', N'Dana Marina Boutique Hotel', N'/image/Hotel/hotel276.jpg', N'/image/Hotel/hotel277.jpg', N'/image/Hotel/hotel278.jpg', N'/image/Hotel/hotel279.jpg', N'/image/Hotel/hotel280.jpg', N'47- 49 Vo Van Kiet, Phuoc My', N'', N'Da Nang', N'Vietnam', N'4', N'4.5', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'32', N'Furama Resort Danang', N'/image/Hotel/hotel281.jpg', N'/image/Hotel/hotel282.jpg', N'/image/Hotel/hotel283.jpg', N'/image/Hotel/hotel284.jpg', N'/image/Hotel/hotel285.jpg', N'105 Vo Nguyen Giap St., Khue My Ward, Ngu Hanh Son Dist.', N'', N'Da Nang', N'Vietnam', N'5', N'8.5', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'33', N'Altara Suites by Ri-Yaz', N'/image/Hotel/hotel286.jpg', N'/image/Hotel/hotel287.jpg', N'/image/Hotel/hotel288.jpg', N'/image/Hotel/hotel289.jpg', N'/image/Hotel/hotel290.jpg', N'120 Vo Nguyen Giap Street, Phuoc My Ward', N'Son Tra', N'Da Nang', N'Vietnam', N'5', N'8.8', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'34', N'Muong Thanh Luxury Song Han Hotel', N'/image/Hotel/hotel291.jpg', N'/image/Hotel/hotel292.jpg', N'/image/Hotel/hotel293.jpg', N'/image/Hotel/hotel294.jpg', N'/image/Hotel/hotel295.jpg', N'115 Nguyen Van Linh St., Nam Duong Ward', N'Hai Chau', N'Da Nang', N'Vietnam', N'5', N'7.9', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'35', N'Mangala Zen Garden & Luxury Apartments', N'/image/Hotel/hotel296.jpg', N'/image/Hotel/hotel297.jpg', N'/image/Hotel/hotel298.jpg', N'/image/Hotel/hotel299.jpg', N'/image/Hotel/hotel300.jpg', N'Hoa Hai', N'', N'Da Nang', N'Vietnam', N'4', N'8.6', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');

INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'16', N'Liberty Central Nha Trang Hotel', N'/image/Hotel/hotel301.jpg', N'/image/Hotel/hotel302.jpg', N'/image/Hotel/hotel303.jpg', N'/image/Hotel/hotel304.jpg', N'/image/Hotel/hotel305.jpg', N'9 Biet Thu Street, Loc Tho', N'', N'Nha Trang', N'Vietnam', N'4', N'8.7', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'17', N'Holi Panorama Nha Trang', N'/image/Hotel/hotel306.jpg', N'/image/Hotel/hotel307.jpg', N'/image/Hotel/hotel308.jpg', N'/image/Hotel/hotel309.jpg', N'/image/Hotel/hotel310.jpg', N'02 Nguyen Thi Minh Khai, Loc Tho', N'', N'Nha Trang', N'Vietnam', N'4', N'8.6', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'18', N'An Lam Retreats Ninh Van Bay', N'/image/Hotel/hotel311.jpg', N'/image/Hotel/hotel312.jpg', N'/image/Hotel/hotel313.jpg', N'/image/Hotel/hotel314.jpg', N'/image/Hotel/hotel315.jpg', N'Ninh Van Bay, Ninh Van Bay', N'', N'Nha Trang', N'Vietnam', N'5', N'9.1', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'19', N'Muong Thanh Luxury Nha Trang Hotel', N'/image/Hotel/hotel316.jpg', N'/image/Hotel/hotel317.jpg', N'/image/Hotel/hotel318.jpg', N'/image/Hotel/hotel319.jpg', N'/image/Hotel/hote320.jpg', N'60 Tran Phu Street, Loc Tho', N'', N'Nha Trang', N'Vietnam', N'5', N'8.6', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'20', N'Panorama Luxstay Nha Trang', N'/image/Hotel/hotel321.jpg', N'/image/Hotel/hotel322.jpg', N'/image/Hotel/hotel323.jpg', N'/image/Hotel/hotel324.jpg', N'/image/Hotel/hotel325.jpg', N'Nguyen Thi Minh Khai 02 , Loc Tho', N'', N'Nha Trang', N'Vietnam', N'5', N'8.6', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'21', N'Muong Thanh Holiday Mui Ne Hotel', N'/image/Hotel/hotel326.jpg', N'/image/Hotel/hotel327.jpg', N'/image/Hotel/hotel328.jpg', N'/image/Hotel/hotel329.jpg', N'/image/Hotel/hotel330.jpg', N'54 Huynh Thuc Khang', N'', N'Phan Thiet', N'Vietnam', N'4', N'6.5', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'22', N'TTC Hotel - Phan Thiet', N'/image/Hotel/hotel331.jpg', N'/image/Hotel/hotel332.jpg', N'/image/Hotel/hotel333.jpg', N'/image/Hotel/hotel334.jpg', N'/image/Hotel/hotel335.jpg', N'Doi Duong area, Le Loi street, Hung Long ward', N'', N'Phan Thiet', N'Vietnam', N'4', N'8.2', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'23', N'Centara Mirage Resort Mui Ne', N'/image/Hotel/hotel336.jpg', N'/image/Hotel/hotel337.jpg', N'/image/Hotel/hotel338.jpg', N'/image/Hotel/hotel339.jpg', N'/image/Hotel/hotel340.jpg', N'Huỳnh Thúc Kháng', N'', N'Phan Thiet', N'Vietnam', N'5', N'8.7', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'24', N'The Cliff Residence', N'/image/Hotel/hotel341.jpg', N'/image/Hotel/hotel342.jpg', N'/image/Hotel/hotel343.jpg', N'/image/Hotel/hotel344.jpg', N'/image/Hotel/hotel345.jpg', N'Mui Ne', N'', N'Phan Thiet', N'Vietnam', N'4', N'8.9', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'25', N'The Cliff Resort', N'/image/Hotel/hotel346.jpg', N'/image/Hotel/hotel347.jpg', N'/image/Hotel/hotel348.jpg', N'/image/Hotel/hotel349.jpg', N'/image/Hotel/hotel350.jpg', N'Zone 5, Phu Hai Ward, Binh Thuan Province', N'', N'Phan Thiet', N'Vietnam', N'4', N'8.8', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');

INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'26', N'La Siesta Hoi An Resort & Spa', N'/image/Hotel/hotel351.jpg', N'/image/Hotel/hotel352.jpg', N'/image/Hotel/hotel353.jpg', N'/image/Hotel/hotel354.jpg', N'/image/Hotel/hotel355.jpg', N'134 Hung Vuong, Thanh Ha', N'', N'Hoi An', N'Vietnam', N'5', N'9.2', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'27', N'Laluna Hoi An Riverside Hotel & Spa', N'/image/Hotel/hotel356.jpg', N'/image/Hotel/hotel357.jpg', N'/image/Hotel/hotel358.jpg', N'/image/Hotel/hotel359.jpg', N'/image/Hotel/hotel360.jpg', N'12 Nguyen Du street,, Cam Pho', N'', N'Hoi An', N'Vietnam', N'4', N'9.0', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'28', N'Allegro Hoi An . A Little Luxury Hotel & Spa', N'/image/Hotel/hotel361.jpg', N'/image/Hotel/hotel362.jpg', N'/image/Hotel/hotel363.jpg', N'/image/Hotel/hotel364.jpg', N'/image/Hotel/hotel365.jpg', N'02 - 86 Tran Hung Dao Street', N'', N'Hoi An', N'Vietnam', N'5', N'9.3', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'29', N'Hoi An Heart Lodge', N'/image/Hotel/hotel366.jpg', N'/image/Hotel/hotel367.jpg', N'/image/Hotel/hotel368.jpg', N'/image/Hotel/hotel369.jpg', N'/image/Hotel/hotel370.jpg', N'Son Phong', N'', N'Hoi An', N'Vietnam', N'4', N'9.5', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'30', N'Victoria Hoi An Beach Resort & Spa', N'/image/Hotel/hotel371.jpg', N'/image/Hotel/hotel372.jpg', N'/image/Hotel/hotel373.jpg', N'/image/Hotel/hotel374.jpg', N'/image/Hotel/hotel375.jpg', N'Cua Dai Beach, Cua Dai', N'', N'Hoi An', N'Vietnam', N'4', N'8.8', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'31', N'Villa Hue', N'/image/Hotel/hotel376.jpg', N'/image/Hotel/hotel377.jpg', N'/image/Hotel/hotel378.jpg', N'/image/Hotel/hotel379.jpg', N'/image/Hotel/hotel380.jpg', N'04 Tran Quang Khai Street', N'', N'Hue', N'Vietnam', N'4', N'8.8', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'32', N'Alba Spa Hotel', N'/image/Hotel/hotel381.jpg', N'/image/Hotel/hotel382.jpg', N'/image/Hotel/hotel383.jpg', N'/image/Hotel/hotel384.jpg', N'/image/Hotel/hotel385.jpg', N'29 Tran Quang Khai Street', N'', N'Hue', N'Vietnam', N'4', N'8.9', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'33', N'Imperial Hotel Hue', N'/image/Hotel/hotel386.jpg', N'/image/Hotel/hotel387.jpg', N'/image/Hotel/hotel388.jpg', N'/image/Hotel/hotel389.jpg', N'/image/Hotel/hotel390.jpg', N'8 Hung Vuong Street', N'', N'Hue', N'Vietnam', N'5', N'8.7', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'34', N'Muong Thanh Holiday Hue Hotel', N'/image/Hotel/hotel391.jpg', N'/image/Hotel/hotel392.jpg', N'/image/Hotel/hotel393.jpg', N'/image/Hotel/hotel394.jpg', N'/image/Hotel/hotel395.jpg', N'38 Le Loi Street', N'', N'Hue', N'Vietnam', N'4', N'8.3', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'35', N'Senna Hue Hotel', N'/image/Hotel/hotel396.jpg', N'/image/Hotel/hotel397.jpg', N'/image/Hotel/hotel398.jpg', N'/image/Hotel/hotel399.jpg', N'/image/Hotel/hotel400.jpg', N'7 Nguyen Tri Phuong', N'', N'Hue', N'Vietnam', N'5', N'9.0', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');

INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'16', N'Bayview Park Hotel', N'/image/Hotel/hotel401.jpg', N'/image/Hotel/hotel402.jpg', N'/image/Hotel/hotel403.jpg', N'/image/Hotel/hotel404.jpg', N'/image/Hotel/hotel405.jpg', N'1118 Roxas Boulevard Cor., United Nations Avenue, Ermita', N'', N'Manila', N'Philippines', N'3', N'8.0', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'17', N'Crimson Hotel Filinvest City Manila', N'/image/Hotel/hotel406.jpg', N'/image/Hotel/hotel407.jpg', N'/image/Hotel/hotel408.jpg', N'/image/Hotel/hotel409.jpg', N'/image/Hotel/hotel410.jpg', N'2609 Civic Drive, Entrata Urban Complex , Filinvest', N'', N'Manila', N'Philippines', N'5', N'8.4', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'18', N'Sofitel Philippine Plaza Manila', N'/image/Hotel/hotel411.jpg', N'/image/Hotel/hotel412.jpg', N'/image/Hotel/hotel413.jpg', N'/image/Hotel/hotel414.jpg', N'/image/Hotel/hotel415.jpg', N'CCP Complex, Roxas Boulevard, Pasay', N'', N'Manila', N'Philippines', N'5', N'8.4', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'19', N'Condo Studio Luxe at Princeton Residences', N'/image/Hotel/hotel416.jpg', N'/image/Hotel/hotel417.jpg', N'/image/Hotel/hotel418.jpg', N'/image/Hotel/hotel419.jpg', N'/image/Hotel/hotel420.jpg', N'Quezon', N'', N'Manila', N'Philippines', N'4', N'8.3', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'20', N'Holiday Inn Express Manila Newport City', N'/image/Hotel/hotel421.jpg', N'/image/Hotel/hotel422.jpg', N'/image/Hotel/hotel423.jpg', N'/image/Hotel/hotel424.jpg', N'/image/Hotel/hotel425.jpg', N'1 JASMINE DRIVE, Pasay', N'', N'Manila', N'Philippines', N'3', N'7.4', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'21', N'Hotel RegentMarine The Blue', N'/image/Hotel/hotel426.jpg', N'/image/Hotel/hotel427.jpg', N'/image/Hotel/hotel428.jpg', N'/image/Hotel/hotel429.jpg', N'/image/Hotel/hotel430.jpg', N'20, Seobudu 2-gil, Downtown Jeju', N'', N'Jeju Island', N'South Korea', N'4', N'8.6', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'22', N'The Grand Sumorum', N'/image/Hotel/hotel431.jpg', N'/image/Hotel/hotel432.jpg', N'/image/Hotel/hotel433.jpg', N'/image/Hotel/hotel434.jpg', N'/image/Hotel/hotel435.jpg', N'118, Maksukpo-ro, Seogwipo', N'', N'Jeju Island', N'South Korea', N'4', N'8.4', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'23', N'Hotel Toscana', N'/image/Hotel/hotel436.jpg', N'/image/Hotel/hotel437.jpg', N'/image/Hotel/hotel438.jpg', N'/image/Hotel/hotel439.jpg', N'/image/Hotel/hotel440.jpg', N'158-7, Yongheung-ro 66beon-gil, Seogwipo', N'', N'Jeju Island', N'South Korea', N'4', N'8.7', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'24', N'LOTTE City Hotel Jeju', N'/image/Hotel/hotel441.jpg', N'/image/Hotel/hotel442.jpg', N'/image/Hotel/hotel443.jpg', N'/image/Hotel/hotel444.jpg', N'/image/Hotel/hotel445.jpg', N'83 Doryeong-ro, Jeju-si, Downtown Jeju', N'', N'Jeju Island', N'South Korea', N'4', N'8.8', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'25', N'Shilla Stay Jeju', N'/image/Hotel/hotel446.jpg', N'/image/Hotel/hotel447.jpg', N'/image/Hotel/hotel448.jpg', N'/image/Hotel/hotel449.jpg', N'/image/Hotel/hotel450.jpg', N'274-16 Yeon-dong, Jeju-si, Downtown Jeju', N'', N'Jeju Island', N'South Korea', N'4', N'8.5', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');

INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'26', N'Courtyard by Marriott Phuket Town', N'/image/Hotel/hotel451.jpg', N'/image/Hotel/hotel452.jpg', N'/image/Hotel/hotel453.jpg', N'/image/Hotel/hotel454.jpg', N'/image/Hotel/hotel455.jpg', N'1 Soi Surin, Talat Yai', N'', N'Phuket', N'Thailand', N'4', N'8.8', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'27', N'Phuket Marriott Resort and Spa', N'/image/Hotel/hotel456.jpg', N'/image/Hotel/hotel457.jpg', N'/image/Hotel/hotel458.jpg', N'/image/Hotel/hotel459.jpg', N'/image/Hotel/hotel460.jpg', N'92, 92/1 Moo 3, Sakoo, Talang, Nai Yang Beach', N'', N'Phuket', N'Thailand', N'5', N'8.9', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'28', N'The Lantern Resort Patong', N'/image/Hotel/hotel461.jpg', N'/image/Hotel/hotel462.jpg', N'/image/Hotel/hotel463.jpg', N'/image/Hotel/hotel464.jpg', N'/image/Hotel/hotel465.jpg', N'181/4 Soi Sansabai, Rat-U-Thit 200 Pee Road, Patong', N'', N'Phuket', N'Thailand', N'4', N'8.3', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'29', N'Andamantra Resort and Villa Phuket', N'/image/Hotel/hotel466.jpg', N'/image/Hotel/hotel467.jpg', N'/image/Hotel/hotel468.jpg', N'/image/Hotel/hotel469.jpg', N'/image/Hotel/hotel470.jpg', N'290/1 Prabaramee Road, Patong', N'', N'Phuket', N'Thailand', N'4', N'7.8', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'30', N'Kalima Resort & Spa', N'/image/Hotel/hotel471.jpg', N'/image/Hotel/hotel472.jpg', N'/image/Hotel/hotel473.jpg', N'/image/Hotel/hotel474.jpg', N'/image/Hotel/hotel475.jpg', N'338/1 Prabaramee Road, Patong, Kathu, Patong', N'', N'Phuket', N'Thailand', N'5', N'8.3', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'31', N'Shibuya Stream Excel Hotel Tokyu', N'/image/Hotel/hotel476.jpg', N'/image/Hotel/hotel477.jpg', N'/image/Hotel/hotel478.jpg', N'/image/Hotel/hotel479.jpg', N'/image/Hotel/hotel480.jpg', N'3-21-3, Shibuya, Shibuya', N'', N'Tokyo', N'Japan', N'4', N'8.9', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'32', N'Shibuya Excel Hotel Tokyu', N'/image/Hotel/hotel481.jpg', N'/image/Hotel/hotel482.jpg', N'/image/Hotel/hotel483.jpg', N'/image/Hotel/hotel484.jpg', N'/image/Hotel/hotel485.jpg', N'1-12-2 Dougenzaka, Shibuya', N'', N'Tokyo', N'Japan', N'4', N'8.5', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'33', N'Sotetsu Fresa Inn Tokyo-Tamachi', N'/image/Hotel/hotel486.jpg', N'/image/Hotel/hotel487.jpg', N'/image/Hotel/hotel488.jpg', N'/image/Hotel/hotel489.jpg', N'/image/Hotel/hotel490.jpg', N'3-14-21 Shibaura, Minato-ku, Shinbashi', N'', N'Tokyo', N'Japan', N'3', N'8.3', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'34', N'Shinagawa Prince Hotel', N'/image/Hotel/hotel491.jpg', N'/image/Hotel/hotel492.jpg', N'/image/Hotel/hotel493.jpg', N'/image/Hotel/hotel494.jpg', N'/image/Hotel/hotel495.jpg', N'4-10-30 Takanawa, Minato-ku, Shinagawa', N'', N'Tokyo', N'Japan', N'4', N'8.0', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'35', N'Daiwa Roynet Hotel Tokyo Ariake', N'/image/Hotel/hotel496.jpg', N'/image/Hotel/hotel497.jpg', N'/image/Hotel/hotel498.jpg', N'/image/Hotel/hotel499.jpg', N'/image/Hotel/hotel500.jpg', N'3-7-3 Ariake, Koto, Odaiba', N'', N'Tokyo', N'Japan', N'4', N'8.9', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');




INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'16', N'Hyatt Regency Danang Resort and Spa', N'/image/Hotel/hotel501.jpg', N'/image/Hotel/hotel502.jpg', N'/image/Hotel/hotel503.jpg', N'/image/Hotel/hotel504.jpg', N'/image/Hotel/hotel505.jpg', N'Hoa Hai Ward, Ngu Hanh Son', N'', N'Da Nang', N'Vietnam', N'5', N'9.9', N'The beachfront Hyatt Regency Danang Resort and Spa sits on the picturesque coast of Da Nang City and is a 5-minute drive from Marble Mountains. Offering stunning views of the Vietnam is East Sea, this luxurious hideaway resorts boasts 3 in-house dining options, an outdoor swimming pool and a day spa.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'17', N'InterContinental Danang Sun Peninsula Resort', N'/image/Hotel/hotel506.jpg', N'/image/Hotel/hotel507.jpg', N'/image/Hotel/hotel508.jpg', N'/image/Hotel/hotel509.jpg', N'/image/Hotel/hotel510.jpg', N'Bai Sac, Son Tra Peninsula', N'', N'Da Nang', N'Vietnam', N'5', N'9.9', N'Boasting several fine dining options like the acclaimed La Maison 1888, InterContinental Danang Sun Peninsula Resort provides a luxurious retreat with private beach, a large outdoor swimming pool and a spa. Spectacular views of the ocean can be enjoyed from the resort.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'18', N'Abogo Resort Villas Luxury Da Nang', N'/image/Hotel/hotel511.jpg', N'/image/Hotel/hotel512.jpg', N'/image/Hotel/hotel513.jpg', N'/image/Hotel/hotel514.jpg', N'/image/Hotel/hotel515.jpg', N'107 Võ Nguyên Giáp, Ngũ Hành Sơn', N'', N'Da Nang', N'Vietnam', N'5', N'9.8', N'This property is 1 minute walk from the beach. With river views, Abogo Resort Villas Luxury Da Nang is located in Da Nang and has a restaurant, a 24-hour front desk, bar, garden, outdoor pool and children is playground. There is a casino on site and guests can enjoy the on-site snack bar.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'19', N'Vinpearl Resort & Spa Da Nang', N'/image/Hotel/hotel516.jpg', N'/image/Hotel/hotel517.jpg', N'/image/Hotel/hotel518.jpg', N'/image/Hotel/hotel519.jpg', N'/image/Hotel/hotel520.jpg', N'Truong Sa, Ngu Hanh Son', N'', N'Da Nang', N'Vietnam', N'5', N'9.7', N'This property is 2 minutes walk from the beach. Featuring modern neoclassical architecture, Vinpearl Da Nang Ocean Resort and Villas offers elegant villas in Da Nang. The property has 4 on-site restaurants, outdoor swimming pool and bar. Guests can indulge with rejuvenating massage or enjoy a sauna session at the spa, as well as have a good sweat at the gym.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'20', N'Abogo Resort Villas Ocean Da Nang', N'/image/Hotel/hotel521.jpg', N'/image/Hotel/hotel522.jpg', N'/image/Hotel/hotel523.jpg', N'/image/Hotel/hotel524.jpg', N'/image/Hotel/hotel525.jpg', N'Đường Trường Sa, Phường Hòa Hải, Quận Ngũ Hành Sơn', N'', N'Da Nang', N'Vietnam', N'5', N'9.8', N'Offering lake or pool views, each unit comes with a kitchen, a satellite flat-screen TV and Blu-ray player, desk, a washing machine and a private bathroom with hot tub and bathrobes. All units are air conditioned and include a seating and/or dining area.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'21', N'Pullman Danang Beach Resort', N'/image/Hotel/hotel526.jpg', N'/image/Hotel/hotel527.jpg', N'/image/Hotel/hotel528.jpg', N'/image/Hotel/hotel529.jpg', N'/image/Hotel/hotel530.jpg', N'101 Vo Nguyen Giap Street, Khue My Ward', N'', N'Da Nang', N'Vietnam', N'5', N'9.6', N'Pullman Danang Beach Resort is situated on Bac My An Beach, 3 km from downtown Da Nang. It offers an outdoor pool and spa. Free WiFi and parking are provided. Pullman Danang Beach Resort is about 8 km from Da Nang International Airport. It provides two-way airport transfer services at an additional charge.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'22', N'Premier Village Danang Resort Managed By Accor', N'/image/Hotel/hotel531.jpg', N'/image/Hotel/hotel532.jpg', N'/image/Hotel/hotel533.jpg', N'/image/Hotel/hotel534.jpg', N'/image/Hotel/hotel535.jpg', N'Vo Nguyen Giap street', N'', N'Da Nang', N'Vietnam', N'4', N'9.9', N'This property is 1 minute walk from the beach. Welcoming guests with an outdoor infinity pool and a private beach area, Premier Village Danang Resort Managed by Accor is a luxurious accommodation nestled in the heart of Da Nang. Guests enjoy free WiFi access.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'23', N'CANTHO ECO RESORT', N'/image/Hotel/hotel536.jpg', N'/image/Hotel/hotel537.jpg', N'/image/Hotel/hotel538.jpg', N'/image/Hotel/hotel539.jpg', N'/image/Hotel/hotel540.jpg', N'Km7, Quốc Lộ 61C, Nhơn Nghĩa, Phong Điền', N'', N'Can Tho', N'Vietnam', N'4', N'9.8', N'Situated in Can Tho, West Hotel offers elegant and stylish accommodation with free WiFi access in its public areas. Operating a 24-hour front desk, it features an indoor pool, fitness centre and free parking on site. The hotel is just 100 m from Ninh Kieu Pier and 300 m from Can Tho Museum. Cai Rang Floating Market is 6.2 km away, while Rach Gia Airport is approximately 72 km away.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'24', N'West Hotel', N'/image/Hotel/hotel541.jpg', N'/image/Hotel/hotel542.jpg', N'/image/Hotel/hotel543.jpg', N'/image/Hotel/hotel544.jpg', N'/image/Hotel/hotel545.jpg', N'88-90-92 Hai Ba Trung', N'', N'Can Tho', N'Vietnam', N'3', N'9.7', N'Situated in Can Tho, West Hotel offers elegant and stylish accommodation with free WiFi access in its public areas. Operating a 24-hour front desk, it features an indoor pool, fitness centre and free parking on site. The hotel is just 100 m from Ninh Kieu Pier and 300 m from Can Tho Museum. Cai Rang Floating Market is 6.2 km away, while Rach Gia Airport is approximately 72 km away.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'25', N'Lucky n Lush Apartment', N'/image/Hotel/hotel546.jpg', N'/image/Hotel/hotel547.jpg', N'/image/Hotel/hotel558.jpg', N'/image/Hotel/hotel549.jpg', N'/image/Hotel/hotel550.jpg', N'2 Đường 3/4', N'', N'Da Lat', N'Vietnam', N'4', N'9.6', N'Located 400 m from Lam Vien Square, Lucky n Lush Apartment provides accommodation with a shared lounge, a garden and a 24-hour front desk for your convenience. Both WiFi and private parking are available at the apartment free of charge. All units come with a seating area, a TV and a private bathroom with slippers, bidet and shower. A microwave and fridge are also provided, as well as a kettle.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'26', N'Terracotta Hotel & Resort Dalat', N'/image/Hotel/hotel551.jpg', N'/image/Hotel/hotel552.jpg', N'/image/Hotel/hotel553.jpg', N'/image/Hotel/hotel554.jpg', N'/image/Hotel/hotel555.jpg', N'Zone 7.9, Tuyen Lam Lake Tourist Area, Ward 3', N'', N'Da Lat', N'Vietnam', N'5', N'9.9', N'Terracotta Hotel & Resort Dalat offers rooms and villas with free WiFi. Featuring an indoor pool, spa and fitness centre, the resort is located next to Tuyen Lam Lake. Terracotta Hotel & Resort Dalat is 900 m from Truc Lam Temple. Lien Khuong Airport is 18 km away.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'27', N'Lotte Hotel Hanoi', N'/image/Hotel/hotel556.jpg', N'/image/Hotel/hotel557.jpg', N'/image/Hotel/hotel558.jpg', N'/image/Hotel/hotel559.jpg', N'/image/Hotel/hotel560.jpg', N'54 Lieu Giai Street, Ba Dinh', N'', N'Ha Noi', N'Vietnam', N'5', N'9.8', N'Strategically located in Hanoi, Lotte Hotel Hanoi offers modern and luxurious accommodation with free WiFi access throughout the property. It features an outdoor pool, indoor pool, a fitness centre, sauna facility and free parking on site. Guests enjoy free early check-in and late check-out.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'28', N'Hotel de lOpera Hanoi', N'/image/Hotel/hotel561.jpg', N'/image/Hotel/hotel562.jpg', N'/image/Hotel/hotel563.jpg', N'/image/Hotel/hotel564.jpg', N'/image/Hotel/hotel565.jpg', N'29 Trang Tien Street, Hoan Kiem', N'', N'Ha Noi', N'Vietnam', N'5', N'9.9', N'Offering 5-star luxury, the iconic Hotel de lOpera Hanoi - MGallery is a 5-minute walk from Hanoi Opera House. Inspired by 19th Century France, this stylish hotel features an indoor pool and fitness facilities. Elegant rooms are fitted with European-style designer furnishings and feature walls with artwork. Each room has a flat-screen TV and comes with free Wi-Fi. Luxurious bathrooms offer shower areas.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'29', N'Somerset Ho Chi Minh City', N'/image/Hotel/hotel566.jpg', N'/image/Hotel/hotel567.jpg', N'/image/Hotel/hotel568.jpg', N'/image/Hotel/hotel569.jpg', N'/image/Hotel/hotel570.jpg', N'8A Nguyen Binh Khiem', N'', N'Ho Chi Minh', N'Vietnam', N'5', N'9.7', N'Conveniently located in the business and shopping district, Somerset Ho Chi Minh City offers resort-style serviced apartments with free high-speed internet access and an outdoor swimming pool with spa tub. Somerset Ho Chi Minh City is close to a wide variety of entertainment and dining options while city attractions including the Zoo and Botanic Gardens are easily accessible.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'30', N'Sofitel Saigon Plaza', N'/image/Hotel/hotel571.jpg', N'/image/Hotel/hotel572.jpg', N'/image/Hotel/hotel573.jpg', N'/image/Hotel/hotel574.jpg', N'/image/Hotel/hotel575.jpg', N'17 Le Duan Boulevard', N'', N'Ho Chi Minh', N'Vietnam', N'5', N'9.6', N'Sofitel Saigon Plaza offers luxurious accommodation, a fitness centre and swimming pool at the heart of Ho Chi Minh City. Near several shopping and entertainment options it is also close to major attractions, such as, Notre Dame Cathedral and Reunification Palace. Rooms at Sofitel Saigon Plaza offer views of the city. Decorated in a contemporary style, they are also fitted with a marble bathroom and free high-speed internet access. Room service is also provided 24-hours daily.', N'', N'');




INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'31', N'', N'/image/Hotel/hotel576.jpg', N'/image/Hotel/hotel577.jpg', N'/image/Hotel/hotel578.jpg', N'/image/Hotel/hotel579.jpg', N'/image/Hotel/hotel580.jpg', N'316 Zhengqi Road', N'', N'Taitung', N'Taiwan', N'4', N'9.5', N'The car parking and the Wi-Fi are always free, so you can stay in touch and come and go as you please. Strategically situated in Taitung City, allowing you access and proximity to local attractions and sights. Dont leave before paying a visit to the famous Taitung Forest Park. Rated with 4.5 stars, this high-quality property provides guests with access to restaurant and fitness center on-site.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'32', N'', N'/image/Hotel/hotel581.jpg', N'/image/Hotel/hotel582.jpg', N'/image/Hotel/hotel583jpg', N'/image/Hotel/hotel584.jpg', N'/image/Hotel/hotel585.jpg', N'No. 66 , Lien Hang Rord', N'', N'Taitung', N'Taiwan', N'5', N'8.0', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'33', N'', N'/image/Hotel/hotel586.jpg', N'/image/Hotel/hotel587.jpg', N'/image/Hotel/hotel588.jpg', N'/image/Hotel/hotel589.jpg', N'/image/Hotel/hotel590.jpg', N'Ximer Arpora Bardez, Baga', N'', N'Goa', N'India', N'5', N'8.5', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'34', N'', N'/image/Hotel/hotel591.jpg', N'/image/Hotel/hotel592.jpg', N'/image/Hotel/hotel593.jpg', N'/image/Hotel/hotel594.jpg', N'/image/Hotel/hotel595.jpg', N'Tivai Vaddo, Calangute', N'', N'Goa', N'India', N'4', N'7.0', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'35', N'', N'/image/Hotel/hotel596.jpg', N'/image/Hotel/hotel597.jpg', N'/image/Hotel/hotel598.jpg', N'/image/Hotel/hotel599.jpg', N'/image/Hotel/hotel600.jpg', N'476 Baniyas Road, Deira', N'', N'Dubai', N'United Arab Emirates', N'5', N'8.7', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'16', N'', N'/image/Hotel/hotel601.jpg', N'/image/Hotel/hotel602.jpg', N'/image/Hotel/hotel603.jpg', N'/image/Hotel/hotel604.jpg', N'/image/Hotel/hotel605.jpg', N'Sheikh Zayed Road, World Trade Centre DIFC', N'', N'Dubai', N'United Arab Emirates', N'4', N'8.6', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'17', N'', N'/image/Hotel/hotel606.jpg', N'/image/Hotel/hotel607.jpg', N'/image/Hotel/hotel608.jpg', N'/image/Hotel/hotel609.jpg', N'/image/Hotel/hotel610.jpg', N'No.33, Jalan Seladang, Taman Abad', N'', N'Johor Bahru', N'Malaysia', N'4', N'7.0', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'18', N'', N'/image/Hotel/hotel611.jpg', N'/image/Hotel/hotel612.jpg', N'/image/Hotel/hotel613.jpg', N'/image/Hotel/hotel614.jpg', N'/image/Hotel/hotel615.jpg', N'Jalan Tun Abdul Razak', N'', N'Johor Bahru', N'Malaysia', N'4', N'8.5', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'19', N'', N'/image/Hotel/hotel616.jpg', N'/image/Hotel/hotel617.jpg', N'/image/Hotel/hotel618.jpg', N'/image/Hotel/hotel619.jpg', N'/image/Hotel/hotel620.jpg', N'100 Minories', N'', N'London', N'United Kingdom', N'5', N'9.1', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');
INSERT [dbo].[Hotel] ([AccountID], [Name], [Img1], [Img2], [Img3], [Img4], [Img5], [Address], [District], [City], [Country], [Star], [Point], [Description], [Latitude], [Longitude]) 
VALUES (N'20', N'', N'/image/Hotel/hotel621.jpg', N'/image/Hotel/hotel622.jpg', N'/image/Hotel/hotel623.jpg', N'/image/Hotel/hotel624.jpg', N'/image/Hotel/hotel625.jpg', N'35-39 Inverness Terrace, Bayswater, Hyde Park', N'', N'London', N'United Kingdom', N'3', N'8.1', N'Due to the ongoing Coronavirus (COVID-19) situation, this property can only accept bookings from fully vaccinated guests. Vaccination certificates and/or other documents as required by law must be presented at check-in. Please contact the property for more information.', N'', N'');

/**********======Add data Room======*********/

INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'1', N'1', N'/image/Room/room1.jpg', N'/image/Room/room2.jpg', N'/image/Room/room3.jpg', N'/image/Room/room4.jpg', N'/image/Room/room5.jpg', N'80', N'120', N'20', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'1', N'2', N'/image/Room/room6.jpg', N'/image/Room/room7.jpg', N'/image/Room/room8.jpg', N'/image/Room/room9.jpg', N'/image/Room/room10.jpg', N'80', N'180', N'15', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'2', N'1', N'/image/Room/room11.jpg', N'/image/Room/room12.jpg', N'/image/Room/room13.jpg', N'/image/Room/room14.jpg', N'/image/Room/room15.jpg', N'28', N'150', N'30', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'2', N'3', N'/image/Room/room16.jpg', N'/image/Room/room17.jpg', N'/image/Room/room18.jpg', N'/image/Room/room19.jpg', N'/image/Room/room20.jpg', N'55', N'250', N'25', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'3', N'1', N'/image/Room/room21.jpg', N'/image/Room/room22.jpg', N'/image/Room/room23.jpg', N'/image/Room/room24.jpg', N'/image/Room/room25.jpg', N'16', N'80', N'25', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'3', N'3', N'/image/Room/room26.jpg', N'/image/Room/room27.jpg', N'/image/Room/room28.jpg', N'/image/Room/room29.jpg', N'/image/Room/room30.jpg', N'35', N'130', N'25', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'4', N'1', N'/image/Room/room31.jpg', N'/image/Room/room32.jpg', N'/image/Room/room33.jpg', N'/image/Room/room34.jpg', N'/image/Room/room35.jpg', N'16', N'40', N'5', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'4', N'2', N'/image/Room/room36.jpg', N'/image/Room/room37.jpg', N'/image/Room/room38.jpg', N'/image/Room/room39.jpg', N'/image/Room/room40.jpg', N'16', N'80', N'5', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'5', N'3', N'/image/Room/room41.jpg', N'/image/Room/room42.jpg', N'/image/Room/room43.jpg', N'/image/Room/room44.jpg', N'/image/Room/room45.jpg', N'25', N'100', N'20', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'5', N'2', N'/image/Room/room46.jpg', N'/image/Room/room47.jpg', N'/image/Room/room48.jpg', N'/image/Room/room49.jpg', N'/image/Room/room50.jpg', N'30', N'80', N'15', N'0');


/********************************************************************/

INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'6', N'1', N'/image/Room/room1.jpg', N'/image/Room/room2.jpg', N'/image/Room/room3.jpg', N'/image/Room/room4.jpg', N'/image/Room/room5.jpg', N'80', N'120', N'20', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'7', N'2', N'/image/Room/room6.jpg', N'/image/Room/room7.jpg', N'/image/Room/room8.jpg', N'/image/Room/room9.jpg', N'/image/Room/room10.jpg', N'80', N'180', N'15', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'8', N'1', N'/image/Room/room11.jpg', N'/image/Room/room12.jpg', N'/image/Room/room13.jpg', N'/image/Room/room14.jpg', N'/image/Room/room15.jpg', N'28', N'150', N'30', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'9', N'3', N'/image/Room/room16.jpg', N'/image/Room/room17.jpg', N'/image/Room/room18.jpg', N'/image/Room/room19.jpg', N'/image/Room/room20.jpg', N'55', N'250', N'25', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'10', N'1', N'/image/Room/room21.jpg', N'/image/Room/room22.jpg', N'/image/Room/room23.jpg', N'/image/Room/room24.jpg', N'/image/Room/room25.jpg', N'16', N'80', N'25', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'11', N'3', N'/image/Room/room26.jpg', N'/image/Room/room27.jpg', N'/image/Room/room28.jpg', N'/image/Room/room29.jpg', N'/image/Room/room30.jpg', N'35', N'130', N'25', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'12', N'1', N'/image/Room/room31.jpg', N'/image/Room/room32.jpg', N'/image/Room/room33.jpg', N'/image/Room/room34.jpg', N'/image/Room/room35.jpg', N'16', N'40', N'5', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'13', N'2', N'/image/Room/room36.jpg', N'/image/Room/room37.jpg', N'/image/Room/room38.jpg', N'/image/Room/room39.jpg', N'/image/Room/room40.jpg', N'16', N'80', N'5', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'14', N'3', N'/image/Room/room41.jpg', N'/image/Room/room42.jpg', N'/image/Room/room43.jpg', N'/image/Room/room44.jpg', N'/image/Room/room45.jpg', N'25', N'100', N'20', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'15', N'2', N'/image/Room/room46.jpg', N'/image/Room/room47.jpg', N'/image/Room/room48.jpg', N'/image/Room/room49.jpg', N'/image/Room/room50.jpg', N'30', N'80', N'15', N'0');

INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'16', N'1', N'/image/Room/room1.jpg', N'/image/Room/room2.jpg', N'/image/Room/room3.jpg', N'/image/Room/room4.jpg', N'/image/Room/room5.jpg', N'80', N'120', N'20', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'17', N'2', N'/image/Room/room6.jpg', N'/image/Room/room7.jpg', N'/image/Room/room8.jpg', N'/image/Room/room9.jpg', N'/image/Room/room10.jpg', N'80', N'180', N'15', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'18', N'1', N'/image/Room/room11.jpg', N'/image/Room/room12.jpg', N'/image/Room/room13.jpg', N'/image/Room/room14.jpg', N'/image/Room/room15.jpg', N'28', N'150', N'30', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'19', N'3', N'/image/Room/room16.jpg', N'/image/Room/room17.jpg', N'/image/Room/room18.jpg', N'/image/Room/room19.jpg', N'/image/Room/room20.jpg', N'55', N'250', N'25', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'20', N'1', N'/image/Room/room21.jpg', N'/image/Room/room22.jpg', N'/image/Room/room23.jpg', N'/image/Room/room24.jpg', N'/image/Room/room25.jpg', N'16', N'80', N'25', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'21', N'3', N'/image/Room/room26.jpg', N'/image/Room/room27.jpg', N'/image/Room/room28.jpg', N'/image/Room/room29.jpg', N'/image/Room/room30.jpg', N'35', N'130', N'25', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'22', N'1', N'/image/Room/room31.jpg', N'/image/Room/room32.jpg', N'/image/Room/room33.jpg', N'/image/Room/room34.jpg', N'/image/Room/room35.jpg', N'16', N'40', N'5', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'23', N'2', N'/image/Room/room36.jpg', N'/image/Room/room37.jpg', N'/image/Room/room38.jpg', N'/image/Room/room39.jpg', N'/image/Room/room40.jpg', N'16', N'80', N'5', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'24', N'3', N'/image/Room/room41.jpg', N'/image/Room/room42.jpg', N'/image/Room/room43.jpg', N'/image/Room/room44.jpg', N'/image/Room/room45.jpg', N'25', N'100', N'20', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'25', N'2', N'/image/Room/room46.jpg', N'/image/Room/room47.jpg', N'/image/Room/room48.jpg', N'/image/Room/room49.jpg', N'/image/Room/room50.jpg', N'30', N'80', N'15', N'0');

INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'26', N'1', N'/image/Room/room1.jpg', N'/image/Room/room2.jpg', N'/image/Room/room3.jpg', N'/image/Room/room4.jpg', N'/image/Room/room5.jpg', N'80', N'120', N'20', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'27', N'2', N'/image/Room/room6.jpg', N'/image/Room/room7.jpg', N'/image/Room/room8.jpg', N'/image/Room/room9.jpg', N'/image/Room/room10.jpg', N'80', N'180', N'15', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'28', N'1', N'/image/Room/room11.jpg', N'/image/Room/room12.jpg', N'/image/Room/room13.jpg', N'/image/Room/room14.jpg', N'/image/Room/room15.jpg', N'28', N'150', N'30', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'29', N'3', N'/image/Room/room16.jpg', N'/image/Room/room17.jpg', N'/image/Room/room18.jpg', N'/image/Room/room19.jpg', N'/image/Room/room20.jpg', N'55', N'250', N'25', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'30', N'1', N'/image/Room/room21.jpg', N'/image/Room/room22.jpg', N'/image/Room/room23.jpg', N'/image/Room/room24.jpg', N'/image/Room/room25.jpg', N'16', N'80', N'25', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'31', N'3', N'/image/Room/room26.jpg', N'/image/Room/room27.jpg', N'/image/Room/room28.jpg', N'/image/Room/room29.jpg', N'/image/Room/room30.jpg', N'35', N'130', N'25', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'32', N'1', N'/image/Room/room31.jpg', N'/image/Room/room32.jpg', N'/image/Room/room33.jpg', N'/image/Room/room34.jpg', N'/image/Room/room35.jpg', N'16', N'40', N'5', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'33', N'2', N'/image/Room/room36.jpg', N'/image/Room/room37.jpg', N'/image/Room/room38.jpg', N'/image/Room/room39.jpg', N'/image/Room/room40.jpg', N'16', N'80', N'5', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'34', N'3', N'/image/Room/room41.jpg', N'/image/Room/room42.jpg', N'/image/Room/room43.jpg', N'/image/Room/room44.jpg', N'/image/Room/room45.jpg', N'25', N'100', N'20', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'35', N'2', N'/image/Room/room46.jpg', N'/image/Room/room47.jpg', N'/image/Room/room48.jpg', N'/image/Room/room49.jpg', N'/image/Room/room50.jpg', N'30', N'80', N'15', N'0');

INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'36', N'1', N'/image/Room/room1.jpg', N'/image/Room/room2.jpg', N'/image/Room/room3.jpg', N'/image/Room/room4.jpg', N'/image/Room/room5.jpg', N'80', N'120', N'20', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'37', N'2', N'/image/Room/room6.jpg', N'/image/Room/room7.jpg', N'/image/Room/room8.jpg', N'/image/Room/room9.jpg', N'/image/Room/room10.jpg', N'80', N'180', N'15', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'38', N'1', N'/image/Room/room11.jpg', N'/image/Room/room12.jpg', N'/image/Room/room13.jpg', N'/image/Room/room14.jpg', N'/image/Room/room15.jpg', N'28', N'150', N'30', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'39', N'3', N'/image/Room/room16.jpg', N'/image/Room/room17.jpg', N'/image/Room/room18.jpg', N'/image/Room/room19.jpg', N'/image/Room/room20.jpg', N'55', N'250', N'25', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'40', N'1', N'/image/Room/room21.jpg', N'/image/Room/room22.jpg', N'/image/Room/room23.jpg', N'/image/Room/room24.jpg', N'/image/Room/room25.jpg', N'16', N'80', N'25', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'41', N'3', N'/image/Room/room26.jpg', N'/image/Room/room27.jpg', N'/image/Room/room28.jpg', N'/image/Room/room29.jpg', N'/image/Room/room30.jpg', N'35', N'130', N'25', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'42', N'1', N'/image/Room/room31.jpg', N'/image/Room/room32.jpg', N'/image/Room/room33.jpg', N'/image/Room/room34.jpg', N'/image/Room/room35.jpg', N'16', N'40', N'5', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'43', N'2', N'/image/Room/room36.jpg', N'/image/Room/room37.jpg', N'/image/Room/room38.jpg', N'/image/Room/room39.jpg', N'/image/Room/room40.jpg', N'16', N'80', N'5', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'44', N'3', N'/image/Room/room41.jpg', N'/image/Room/room42.jpg', N'/image/Room/room43.jpg', N'/image/Room/room44.jpg', N'/image/Room/room45.jpg', N'25', N'100', N'20', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'45', N'2', N'/image/Room/room46.jpg', N'/image/Room/room47.jpg', N'/image/Room/room48.jpg', N'/image/Room/room49.jpg', N'/image/Room/room50.jpg', N'30', N'80', N'15', N'0');

INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'46', N'1', N'/image/Room/room1.jpg', N'/image/Room/room2.jpg', N'/image/Room/room3.jpg', N'/image/Room/room4.jpg', N'/image/Room/room5.jpg', N'80', N'120', N'20', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'47', N'2', N'/image/Room/room6.jpg', N'/image/Room/room7.jpg', N'/image/Room/room8.jpg', N'/image/Room/room9.jpg', N'/image/Room/room10.jpg', N'80', N'180', N'15', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'48', N'1', N'/image/Room/room11.jpg', N'/image/Room/room12.jpg', N'/image/Room/room13.jpg', N'/image/Room/room14.jpg', N'/image/Room/room15.jpg', N'28', N'150', N'30', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'49', N'3', N'/image/Room/room16.jpg', N'/image/Room/room17.jpg', N'/image/Room/room18.jpg', N'/image/Room/room19.jpg', N'/image/Room/room20.jpg', N'55', N'250', N'25', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'50', N'1', N'/image/Room/room21.jpg', N'/image/Room/room22.jpg', N'/image/Room/room23.jpg', N'/image/Room/room24.jpg', N'/image/Room/room25.jpg', N'16', N'80', N'25', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'51', N'3', N'/image/Room/room26.jpg', N'/image/Room/room27.jpg', N'/image/Room/room28.jpg', N'/image/Room/room29.jpg', N'/image/Room/room30.jpg', N'35', N'130', N'25', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'52', N'1', N'/image/Room/room31.jpg', N'/image/Room/room32.jpg', N'/image/Room/room33.jpg', N'/image/Room/room34.jpg', N'/image/Room/room35.jpg', N'16', N'40', N'5', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'53', N'2', N'/image/Room/room36.jpg', N'/image/Room/room37.jpg', N'/image/Room/room38.jpg', N'/image/Room/room39.jpg', N'/image/Room/room40.jpg', N'16', N'80', N'5', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'54', N'3', N'/image/Room/room41.jpg', N'/image/Room/room42.jpg', N'/image/Room/room43.jpg', N'/image/Room/room44.jpg', N'/image/Room/room45.jpg', N'25', N'100', N'20', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'55', N'2', N'/image/Room/room46.jpg', N'/image/Room/room47.jpg', N'/image/Room/room48.jpg', N'/image/Room/room49.jpg', N'/image/Room/room50.jpg', N'30', N'80', N'15', N'0');

INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'56', N'1', N'/image/Room/room1.jpg', N'/image/Room/room2.jpg', N'/image/Room/room3.jpg', N'/image/Room/room4.jpg', N'/image/Room/room5.jpg', N'80', N'120', N'20', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'57', N'2', N'/image/Room/room6.jpg', N'/image/Room/room7.jpg', N'/image/Room/room8.jpg', N'/image/Room/room9.jpg', N'/image/Room/room10.jpg', N'80', N'180', N'15', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'58', N'1', N'/image/Room/room11.jpg', N'/image/Room/room12.jpg', N'/image/Room/room13.jpg', N'/image/Room/room14.jpg', N'/image/Room/room15.jpg', N'28', N'150', N'30', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'59', N'3', N'/image/Room/room16.jpg', N'/image/Room/room17.jpg', N'/image/Room/room18.jpg', N'/image/Room/room19.jpg', N'/image/Room/room20.jpg', N'55', N'250', N'25', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'60', N'1', N'/image/Room/room21.jpg', N'/image/Room/room22.jpg', N'/image/Room/room23.jpg', N'/image/Room/room24.jpg', N'/image/Room/room25.jpg', N'16', N'80', N'25', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'61', N'3', N'/image/Room/room26.jpg', N'/image/Room/room27.jpg', N'/image/Room/room28.jpg', N'/image/Room/room29.jpg', N'/image/Room/room30.jpg', N'35', N'130', N'25', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'62', N'1', N'/image/Room/room31.jpg', N'/image/Room/room32.jpg', N'/image/Room/room33.jpg', N'/image/Room/room34.jpg', N'/image/Room/room35.jpg', N'16', N'40', N'5', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'63', N'2', N'/image/Room/room36.jpg', N'/image/Room/room37.jpg', N'/image/Room/room38.jpg', N'/image/Room/room39.jpg', N'/image/Room/room40.jpg', N'16', N'80', N'5', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'64', N'3', N'/image/Room/room41.jpg', N'/image/Room/room42.jpg', N'/image/Room/room43.jpg', N'/image/Room/room44.jpg', N'/image/Room/room45.jpg', N'25', N'100', N'20', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'65', N'2', N'/image/Room/room46.jpg', N'/image/Room/room47.jpg', N'/image/Room/room48.jpg', N'/image/Room/room49.jpg', N'/image/Room/room50.jpg', N'30', N'80', N'15', N'0');

INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'66', N'1', N'/image/Room/room1.jpg', N'/image/Room/room2.jpg', N'/image/Room/room3.jpg', N'/image/Room/room4.jpg', N'/image/Room/room5.jpg', N'80', N'120', N'20', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'67', N'2', N'/image/Room/room6.jpg', N'/image/Room/room7.jpg', N'/image/Room/room8.jpg', N'/image/Room/room9.jpg', N'/image/Room/room10.jpg', N'80', N'180', N'15', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'68', N'1', N'/image/Room/room11.jpg', N'/image/Room/room12.jpg', N'/image/Room/room13.jpg', N'/image/Room/room14.jpg', N'/image/Room/room15.jpg', N'28', N'150', N'30', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'69', N'3', N'/image/Room/room16.jpg', N'/image/Room/room17.jpg', N'/image/Room/room18.jpg', N'/image/Room/room19.jpg', N'/image/Room/room20.jpg', N'55', N'250', N'25', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'70', N'1', N'/image/Room/room21.jpg', N'/image/Room/room22.jpg', N'/image/Room/room23.jpg', N'/image/Room/room24.jpg', N'/image/Room/room25.jpg', N'16', N'80', N'25', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'71', N'3', N'/image/Room/room26.jpg', N'/image/Room/room27.jpg', N'/image/Room/room28.jpg', N'/image/Room/room29.jpg', N'/image/Room/room30.jpg', N'35', N'130', N'25', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'72', N'1', N'/image/Room/room31.jpg', N'/image/Room/room32.jpg', N'/image/Room/room33.jpg', N'/image/Room/room34.jpg', N'/image/Room/room35.jpg', N'16', N'40', N'5', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'73', N'2', N'/image/Room/room36.jpg', N'/image/Room/room37.jpg', N'/image/Room/room38.jpg', N'/image/Room/room39.jpg', N'/image/Room/room40.jpg', N'16', N'80', N'5', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'74', N'3', N'/image/Room/room41.jpg', N'/image/Room/room42.jpg', N'/image/Room/room43.jpg', N'/image/Room/room44.jpg', N'/image/Room/room45.jpg', N'25', N'100', N'20', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'75', N'2', N'/image/Room/room46.jpg', N'/image/Room/room47.jpg', N'/image/Room/room48.jpg', N'/image/Room/room49.jpg', N'/image/Room/room50.jpg', N'30', N'80', N'15', N'0');

INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'76', N'1', N'/image/Room/room1.jpg', N'/image/Room/room2.jpg', N'/image/Room/room3.jpg', N'/image/Room/room4.jpg', N'/image/Room/room5.jpg', N'80', N'120', N'20', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'77', N'2', N'/image/Room/room6.jpg', N'/image/Room/room7.jpg', N'/image/Room/room8.jpg', N'/image/Room/room9.jpg', N'/image/Room/room10.jpg', N'80', N'180', N'15', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'78', N'1', N'/image/Room/room11.jpg', N'/image/Room/room12.jpg', N'/image/Room/room13.jpg', N'/image/Room/room14.jpg', N'/image/Room/room15.jpg', N'28', N'150', N'30', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'79', N'3', N'/image/Room/room16.jpg', N'/image/Room/room17.jpg', N'/image/Room/room18.jpg', N'/image/Room/room19.jpg', N'/image/Room/room20.jpg', N'55', N'250', N'25', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'80', N'1', N'/image/Room/room21.jpg', N'/image/Room/room22.jpg', N'/image/Room/room23.jpg', N'/image/Room/room24.jpg', N'/image/Room/room25.jpg', N'16', N'80', N'25', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'81', N'3', N'/image/Room/room26.jpg', N'/image/Room/room27.jpg', N'/image/Room/room28.jpg', N'/image/Room/room29.jpg', N'/image/Room/room30.jpg', N'35', N'130', N'25', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'82', N'1', N'/image/Room/room31.jpg', N'/image/Room/room32.jpg', N'/image/Room/room33.jpg', N'/image/Room/room34.jpg', N'/image/Room/room35.jpg', N'16', N'40', N'5', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'83', N'2', N'/image/Room/room36.jpg', N'/image/Room/room37.jpg', N'/image/Room/room38.jpg', N'/image/Room/room39.jpg', N'/image/Room/room40.jpg', N'16', N'80', N'5', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'84', N'3', N'/image/Room/room41.jpg', N'/image/Room/room42.jpg', N'/image/Room/room43.jpg', N'/image/Room/room44.jpg', N'/image/Room/room45.jpg', N'25', N'100', N'20', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'85', N'2', N'/image/Room/room46.jpg', N'/image/Room/room47.jpg', N'/image/Room/room48.jpg', N'/image/Room/room49.jpg', N'/image/Room/room50.jpg', N'30', N'80', N'15', N'0');

INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'86', N'1', N'/image/Room/room1.jpg', N'/image/Room/room2.jpg', N'/image/Room/room3.jpg', N'/image/Room/room4.jpg', N'/image/Room/room5.jpg', N'80', N'120', N'20', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'87', N'2', N'/image/Room/room6.jpg', N'/image/Room/room7.jpg', N'/image/Room/room8.jpg', N'/image/Room/room9.jpg', N'/image/Room/room10.jpg', N'80', N'180', N'15', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'88', N'1', N'/image/Room/room11.jpg', N'/image/Room/room12.jpg', N'/image/Room/room13.jpg', N'/image/Room/room14.jpg', N'/image/Room/room15.jpg', N'28', N'150', N'30', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'89', N'3', N'/image/Room/room16.jpg', N'/image/Room/room17.jpg', N'/image/Room/room18.jpg', N'/image/Room/room19.jpg', N'/image/Room/room20.jpg', N'55', N'250', N'25', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'90', N'1', N'/image/Room/room21.jpg', N'/image/Room/room22.jpg', N'/image/Room/room23.jpg', N'/image/Room/room24.jpg', N'/image/Room/room25.jpg', N'16', N'80', N'25', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'91', N'3', N'/image/Room/room26.jpg', N'/image/Room/room27.jpg', N'/image/Room/room28.jpg', N'/image/Room/room29.jpg', N'/image/Room/room30.jpg', N'35', N'130', N'25', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'92', N'1', N'/image/Room/room31.jpg', N'/image/Room/room32.jpg', N'/image/Room/room33.jpg', N'/image/Room/room34.jpg', N'/image/Room/room35.jpg', N'16', N'40', N'5', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'93', N'2', N'/image/Room/room36.jpg', N'/image/Room/room37.jpg', N'/image/Room/room38.jpg', N'/image/Room/room39.jpg', N'/image/Room/room40.jpg', N'16', N'80', N'5', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'94', N'3', N'/image/Room/room41.jpg', N'/image/Room/room42.jpg', N'/image/Room/room43.jpg', N'/image/Room/room44.jpg', N'/image/Room/room45.jpg', N'25', N'100', N'20', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'95', N'2', N'/image/Room/room46.jpg', N'/image/Room/room47.jpg', N'/image/Room/room48.jpg', N'/image/Room/room49.jpg', N'/image/Room/room50.jpg', N'30', N'80', N'15', N'0');

INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'96', N'1', N'/image/Room/room1.jpg', N'/image/Room/room2.jpg', N'/image/Room/room3.jpg', N'/image/Room/room4.jpg', N'/image/Room/room5.jpg', N'80', N'120', N'20', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'97', N'2', N'/image/Room/room6.jpg', N'/image/Room/room7.jpg', N'/image/Room/room8.jpg', N'/image/Room/room9.jpg', N'/image/Room/room10.jpg', N'80', N'180', N'15', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'98', N'1', N'/image/Room/room11.jpg', N'/image/Room/room12.jpg', N'/image/Room/room13.jpg', N'/image/Room/room14.jpg', N'/image/Room/room15.jpg', N'28', N'150', N'30', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'99', N'3', N'/image/Room/room16.jpg', N'/image/Room/room17.jpg', N'/image/Room/room18.jpg', N'/image/Room/room19.jpg', N'/image/Room/room20.jpg', N'55', N'250', N'25', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'100', N'1', N'/image/Room/room21.jpg', N'/image/Room/room22.jpg', N'/image/Room/room23.jpg', N'/image/Room/room24.jpg', N'/image/Room/room25.jpg', N'16', N'80', N'25', N'0');

/**/

INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'101', N'1', N'/image/Room/room1.jpg', N'/image/Room/room2.jpg', N'/image/Room/room3.jpg', N'/image/Room/room4.jpg', N'/image/Room/room5.jpg', N'80', N'120', N'20', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'102', N'2', N'/image/Room/room6.jpg', N'/image/Room/room7.jpg', N'/image/Room/room8.jpg', N'/image/Room/room9.jpg', N'/image/Room/room10.jpg', N'80', N'180', N'15', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'103', N'1', N'/image/Room/room11.jpg', N'/image/Room/room12.jpg', N'/image/Room/room13.jpg', N'/image/Room/room14.jpg', N'/image/Room/room15.jpg', N'28', N'150', N'30', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'104', N'3', N'/image/Room/room16.jpg', N'/image/Room/room17.jpg', N'/image/Room/room18.jpg', N'/image/Room/room19.jpg', N'/image/Room/room20.jpg', N'55', N'250', N'25', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'105', N'1', N'/image/Room/room21.jpg', N'/image/Room/room22.jpg', N'/image/Room/room23.jpg', N'/image/Room/room24.jpg', N'/image/Room/room25.jpg', N'16', N'80', N'25', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'106', N'3', N'/image/Room/room26.jpg', N'/image/Room/room27.jpg', N'/image/Room/room28.jpg', N'/image/Room/room29.jpg', N'/image/Room/room30.jpg', N'35', N'130', N'25', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'107', N'1', N'/image/Room/room31.jpg', N'/image/Room/room32.jpg', N'/image/Room/room33.jpg', N'/image/Room/room34.jpg', N'/image/Room/room35.jpg', N'16', N'40', N'5', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'108', N'2', N'/image/Room/room36.jpg', N'/image/Room/room37.jpg', N'/image/Room/room38.jpg', N'/image/Room/room39.jpg', N'/image/Room/room40.jpg', N'16', N'80', N'5', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'109', N'3', N'/image/Room/room41.jpg', N'/image/Room/room42.jpg', N'/image/Room/room43.jpg', N'/image/Room/room44.jpg', N'/image/Room/room45.jpg', N'25', N'100', N'20', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'110', N'2', N'/image/Room/room46.jpg', N'/image/Room/room47.jpg', N'/image/Room/room48.jpg', N'/image/Room/room49.jpg', N'/image/Room/room50.jpg', N'30', N'80', N'15', N'0');

INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'111', N'1', N'/image/Room/room1.jpg', N'/image/Room/room2.jpg', N'/image/Room/room3.jpg', N'/image/Room/room4.jpg', N'/image/Room/room5.jpg', N'80', N'120', N'20', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'112', N'2', N'/image/Room/room6.jpg', N'/image/Room/room7.jpg', N'/image/Room/room8.jpg', N'/image/Room/room9.jpg', N'/image/Room/room10.jpg', N'80', N'180', N'15', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'113', N'1', N'/image/Room/room11.jpg', N'/image/Room/room12.jpg', N'/image/Room/room13.jpg', N'/image/Room/room14.jpg', N'/image/Room/room15.jpg', N'28', N'150', N'30', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'114', N'3', N'/image/Room/room16.jpg', N'/image/Room/room17.jpg', N'/image/Room/room18.jpg', N'/image/Room/room19.jpg', N'/image/Room/room20.jpg', N'55', N'250', N'25', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'115', N'1', N'/image/Room/room21.jpg', N'/image/Room/room22.jpg', N'/image/Room/room23.jpg', N'/image/Room/room24.jpg', N'/image/Room/room25.jpg', N'16', N'80', N'25', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'116', N'3', N'/image/Room/room26.jpg', N'/image/Room/room27.jpg', N'/image/Room/room28.jpg', N'/image/Room/room29.jpg', N'/image/Room/room30.jpg', N'35', N'130', N'25', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'117', N'1', N'/image/Room/room31.jpg', N'/image/Room/room32.jpg', N'/image/Room/room33.jpg', N'/image/Room/room34.jpg', N'/image/Room/room35.jpg', N'16', N'40', N'5', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'118', N'2', N'/image/Room/room36.jpg', N'/image/Room/room37.jpg', N'/image/Room/room38.jpg', N'/image/Room/room39.jpg', N'/image/Room/room40.jpg', N'16', N'80', N'5', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'119', N'3', N'/image/Room/room41.jpg', N'/image/Room/room42.jpg', N'/image/Room/room43.jpg', N'/image/Room/room44.jpg', N'/image/Room/room45.jpg', N'25', N'100', N'20', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'120', N'2', N'/image/Room/room46.jpg', N'/image/Room/room47.jpg', N'/image/Room/room48.jpg', N'/image/Room/room49.jpg', N'/image/Room/room50.jpg', N'30', N'80', N'15', N'0');


INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'121', N'1', N'/image/Room/room1.jpg', N'/image/Room/room2.jpg', N'/image/Room/room3.jpg', N'/image/Room/room4.jpg', N'/image/Room/room5.jpg', N'80', N'120', N'20', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'122', N'2', N'/image/Room/room6.jpg', N'/image/Room/room7.jpg', N'/image/Room/room8.jpg', N'/image/Room/room9.jpg', N'/image/Room/room10.jpg', N'80', N'180', N'15', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'123', N'1', N'/image/Room/room11.jpg', N'/image/Room/room12.jpg', N'/image/Room/room13.jpg', N'/image/Room/room14.jpg', N'/image/Room/room15.jpg', N'28', N'150', N'30', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'124', N'3', N'/image/Room/room16.jpg', N'/image/Room/room17.jpg', N'/image/Room/room18.jpg', N'/image/Room/room19.jpg', N'/image/Room/room20.jpg', N'55', N'250', N'25', N'0');
INSERT [dbo].[Room] ([HotelID], [RoomTypeID], [Img1], [Img2], [Img3], [Img4], [Img5], [RoomSize], [Price], [Discount], [Status]) 
VALUES (N'125', N'1', N'/image/Room/room21.jpg', N'/image/Room/room22.jpg', N'/image/Room/room23.jpg', N'/image/Room/room24.jpg', N'/image/Room/room25.jpg', N'16', N'80', N'25', N'0');


/**********======Add data Booking======*********/

INSERT [dbo].[Booking] ([RoomID], [AccountID], [CheckIn], [CheckOut], [BookingStatusID], [Price], [QRCode]) 
VALUES (N'1', N'43', N'01/01/2022 10:50:50', N'03/01/2021 10:50:50', N'2', N'240', N'wwwroot/image/QrCode/file-6e84.qrr');
INSERT [dbo].[Booking] ([RoomID], [AccountID], [CheckIn], [CheckOut], [BookingStatusID], [Price], [QRCode]) 
VALUES (N'2', N'43', N'05/01/2022 10:50:50', N'07/01/2022 10:50:50', N'3', N'360', N'');
INSERT [dbo].[Booking] ([RoomID], [AccountID], [CheckIn], [CheckOut], [BookingStatusID], [Price], [QRCode]) 
VALUES (N'1', N'43', N'09/01/2022 10:50:50', N'10/01/2022 10:50:50', N'2', N'120', N'wwwroot/image/QrCode/file-6e84.qrr');
INSERT [dbo].[Booking] ([RoomID], [AccountID], [CheckIn], [CheckOut], [BookingStatusID], [Price], [QRCode]) 
VALUES (N'2', N'43', N'15/01/2022 10:50:50', N'16/01/2022 10:50:50', N'2', N'180', N'wwwroot/image/QrCode/file-6e84.qrr');
INSERT [dbo].[Booking] ([RoomID], [AccountID], [CheckIn], [CheckOut], [BookingStatusID], [Price], [QRCode]) 
VALUES (N'1', N'43', N'19/01/2022 10:50:50', N'22/01/2022 10:50:50', N'2', N'360', N'wwwroot/image/QrCode/file-6e84.qrr');
INSERT [dbo].[Booking] ([RoomID], [AccountID], [CheckIn], [CheckOut], [BookingStatusID], [Price], [QRCode]) 
VALUES (N'2', N'43', N'24/01/2022 10:50:50', N'25/01/2022 10:50:50', N'2', N'180', N'wwwroot/image/QrCode/file-6e84.qrr');
INSERT [dbo].[Booking] ([RoomID], [AccountID], [CheckIn], [CheckOut], [BookingStatusID], [Price], [QRCode]) 
VALUES (N'1', N'43', N'01/02/2022 10:50:50', N'03/01/2022 10:50:50', N'3', N'240', N'');
INSERT [dbo].[Booking] ([RoomID], [AccountID], [CheckIn], [CheckOut], [BookingStatusID], [Price], [QRCode]) 
VALUES (N'2', N'43', N'05/02/2022 10:50:50', N'06/02/2022 10:50:50', N'2', N'180', N'wwwroot/image/QrCode/file-6e84.qrr');
INSERT [dbo].[Booking] ([RoomID], [AccountID], [CheckIn], [CheckOut], [BookingStatusID], [Price], [QRCode]) 
VALUES (N'1', N'43', N'10/02/2022 10:50:50', N'15/02/2022 10:50:50', N'2', N'600', N'wwwroot/image/QrCode/file-6e84.qrr');
INSERT [dbo].[Booking] ([RoomID], [AccountID], [CheckIn], [CheckOut], [BookingStatusID], [Price], [QRCode]) 
VALUES (N'2', N'43', N'25/02/2022 10:50:50', N'26/02/2022 10:50:50', N'3', N'180', N'');
INSERT [dbo].[Booking] ([RoomID], [AccountID], [CheckIn], [CheckOut], [BookingStatusID], [Price], [QRCode]) 
VALUES (N'1', N'43', N'01/03/2022 10:50:50', N'03/03/2022 10:50:50', N'2', N'240', N'wwwroot/image/QrCode/file-6e84.qrr');
INSERT [dbo].[Booking] ([RoomID], [AccountID], [CheckIn], [CheckOut], [BookingStatusID], [Price], [QRCode]) 
VALUES (N'2', N'43', N'04/03/2022 10:50:50', N'05/03/2022 10:50:50', N'2', N'180', N'wwwroot/image/QrCode/file-6e84.qrr');
INSERT [dbo].[Booking] ([RoomID], [AccountID], [CheckIn], [CheckOut], [BookingStatusID], [Price], [QRCode]) 
VALUES (N'3', N'7', N'04/03/2022 12:50:50', N'05/03/2022 12:50:50', N'3', N'150', N'');
INSERT [dbo].[Booking] ([RoomID], [AccountID], [CheckIn], [CheckOut], [BookingStatusID], [Price], [QRCode]) 
VALUES (N'4', N'8', N'05/03/2022 10:50:50', N'06/03/2022 10:50:50', N'3', N'250', N'');
INSERT [dbo].[Booking] ([RoomID], [AccountID], [CheckIn], [CheckOut], [BookingStatusID], [Price], [QRCode]) 
VALUES (N'5', N'9', N'05/03/2022 13:50:50', N'06/03/2022 13:50:50', N'2', N'80', N'wwwroot/image/QrCode/file-6e84.qrr');
INSERT [dbo].[Booking] ([RoomID], [AccountID], [CheckIn], [CheckOut], [BookingStatusID], [Price], [QRCode]) 
VALUES (N'6', N'10', N'07/03/2022 10:50:50', N'09/03/2022 10:50:50', N'2', N'260', N'wwwroot/image/QrCode/file-6e84.qrr');
INSERT [dbo].[Booking] ([RoomID], [AccountID], [CheckIn], [CheckOut], [BookingStatusID], [Price], [QRCode]) 
VALUES (N'7', N'7', N'10/03/2022 10:50:50', N'11/03/2022 10:50:50', N'1', N'40', N'');
INSERT [dbo].[Booking] ([RoomID], [AccountID], [CheckIn], [CheckOut], [BookingStatusID], [Price], [QRCode]) 
VALUES (N'8', N'8', N'12/03/2022 10:50:50', N'15/03/2022 10:50:50', N'1', N'160', N'');
INSERT [dbo].[Booking] ([RoomID], [AccountID], [CheckIn], [CheckOut], [BookingStatusID], [Price], [QRCode]) 
VALUES (N'9', N'9', N'14/03/2022 10:50:50', N'16/03/2022 10:50:50', N'1', N'200', N'');
INSERT [dbo].[Booking] ([RoomID], [AccountID], [CheckIn], [CheckOut], [BookingStatusID], [Price], [QRCode]) 
VALUES (N'10', N'10', N'16/03/2022 10:50:50', N'17/03/2022 10:50:50', N'1', N'80', N'');
INSERT [dbo].[Booking] ([RoomID], [AccountID], [CheckIn], [CheckOut], [BookingStatusID], [Price], [QRCode]) 
VALUES (N'11', N'43', N'17/03/2022 10:50:50', N'18/03/2022 10:50:50', N'1', N'120', N'');
INSERT [dbo].[Booking] ([RoomID], [AccountID], [CheckIn], [CheckOut], [BookingStatusID], [Price], [QRCode]) 
VALUES (N'12', N'43', N'18/03/2022 10:50:50', N'19/03/2022 10:50:50', N'1', N'180', N'');
INSERT [dbo].[Booking] ([RoomID], [AccountID], [CheckIn], [CheckOut], [BookingStatusID], [Price], [QRCode]) 
VALUES (N'13', N'43', N'19/03/2022 10:50:50', N'20/03/2022 10:50:50', N'1', N'150', N'');


/**********======Add data Review======*********/

INSERT [dbo].[Review] ([AccountID], [HotelID], [Cleanliness], [Facilities], [Location], [Service], [Text], [Time])
VALUES (N'6', N'1', N'9', N'9', N'9', N'9', N'Amazing! Good job!', N'2022-02-25 10:50:50');
INSERT [dbo].[Review] ([AccountID], [HotelID], [Cleanliness], [Facilities], [Location], [Service], [Text], [Time])
VALUES (N'7', N'1', N'10', N'10', N'10', N'10', N'It is OK.', N'2022-02-25 16:59:50');
INSERT [dbo].[Review] ([AccountID], [HotelID], [Cleanliness], [Facilities], [Location], [Service], [Text], [Time])
VALUES (N'8', N'1', N'9', N'9', N'9', N'9', N'Wonderful!', N'2022-02-26 11:39:50');
INSERT [dbo].[Review] ([AccountID], [HotelID], [Cleanliness], [Facilities], [Location], [Service], [Text], [Time])
VALUES (N'9', N'1', N'10', N'10', N'10', N'10', N'Pretty good!', N'2022-02-27 17:55:50');
INSERT [dbo].[Review] ([AccountID], [HotelID], [Cleanliness], [Facilities], [Location], [Service], [Text], [Time])
VALUES (N'10', N'1', N'9', N'9', N'9', N'9', N'Amazing! Good job!', N'2022-02-28 11:39:50');
INSERT [dbo].[Review] ([AccountID], [HotelID], [Cleanliness], [Facilities], [Location], [Service], [Text], [Time])
VALUES (N'6', N'2', N'10', N'10', N'10', N'10', N'Amazing! Good job!', N'2022-03-01 08:39:50');
INSERT [dbo].[Review] ([AccountID], [HotelID], [Cleanliness], [Facilities], [Location], [Service], [Text], [Time])
VALUES (N'7', N'2', N'10', N'10', N'10', N'10', N'It is OK.', N'2022-03-01 11:39:50');
INSERT [dbo].[Review] ([AccountID], [HotelID], [Cleanliness], [Facilities], [Location], [Service], [Text], [Time])
VALUES (N'8', N'2', N'9', N'9', N'9', N'9', N'It is OK.', N'2022-03-01 15:39:50');
INSERT [dbo].[Review] ([AccountID], [HotelID], [Cleanliness], [Facilities], [Location], [Service], [Text], [Time])
VALUES (N'9', N'2', N'10', N'10', N'10', N'10', N'It is OK.', N'2022-03-02 10:30:30');
INSERT [dbo].[Review] ([AccountID], [HotelID], [Cleanliness], [Facilities], [Location], [Service], [Text], [Time])
VALUES (N'10', N'2', N'9', N'9', N'9', N'9', N'Wonderful!', N'2022-03-03 09:35:50');
INSERT [dbo].[Review] ([AccountID], [HotelID], [Cleanliness], [Facilities], [Location], [Service], [Text], [Time])
VALUES (N'6', N'3', N'10', N'10', N'10', N'10', N'Wonderful!', N'2022-03-03 11:49:50');
INSERT [dbo].[Review] ([AccountID], [HotelID], [Cleanliness], [Facilities], [Location], [Service], [Text], [Time])
VALUES (N'7', N'3', N'9', N'9', N'9', N'9', N'Pretty good!', N'2022-03-04 13:39:50');
INSERT [dbo].[Review] ([AccountID], [HotelID], [Cleanliness], [Facilities], [Location], [Service], [Text], [Time])
VALUES (N'8', N'3', N'9', N'9', N'9', N'9', N'Pretty good!', N'2022-03-05 11:39:50');
INSERT [dbo].[Review] ([AccountID], [HotelID], [Cleanliness], [Facilities], [Location], [Service], [Text], [Time])
VALUES (N'9', N'3', N'10', N'10', N'10', N'10', N'Pretty good!', N'2022-03-05 15:39:50');
INSERT [dbo].[Review] ([AccountID], [HotelID], [Cleanliness], [Facilities], [Location], [Service], [Text], [Time])
VALUES (N'10', N'3', N'9', N'9', N'9', N'9', N'Pretty good!', N'2022-03-06 16:39:50');




/**********======Add data Facilities======*********/

INSERT [dbo].[Facilities] ([FacilitiesName]) VALUES (N'24-hour front desk');
INSERT [dbo].[Facilities] ([FacilitiesName]) VALUES (N'Laundry');
INSERT [dbo].[Facilities] ([FacilitiesName]) VALUES (N'Elevator');
INSERT [dbo].[Facilities] ([FacilitiesName]) VALUES (N'1 swimming pool');
INSERT [dbo].[Facilities] ([FacilitiesName]) VALUES (N'Free WiFi');
INSERT [dbo].[Facilities] ([FacilitiesName]) VALUES (N'Non-smoking rooms');
INSERT [dbo].[Facilities] ([FacilitiesName]) VALUES (N'Fitness centre');
INSERT [dbo].[Facilities] ([FacilitiesName]) VALUES (N'Bar');
INSERT [dbo].[Facilities] ([FacilitiesName]) VALUES (N'Free parking');
INSERT [dbo].[Facilities] ([FacilitiesName]) VALUES (N'Terrace');
INSERT [dbo].[Facilities] ([FacilitiesName]) VALUES (N'Restaurant');
INSERT [dbo].[Facilities] ([FacilitiesName]) VALUES (N'Complimentary tea');


/**********======Add data HotelFacilities======*********/


INSERT [dbo].[HotelFacilities] ([HotelID], [FacilitiesID]) VALUES (N'1', N'1');
INSERT [dbo].[HotelFacilities] ([HotelID], [FacilitiesID]) VALUES (N'1', N'2');
INSERT [dbo].[HotelFacilities] ([HotelID], [FacilitiesID]) VALUES (N'1', N'3');
INSERT [dbo].[HotelFacilities] ([HotelID], [FacilitiesID]) VALUES (N'1', N'4');
INSERT [dbo].[HotelFacilities] ([HotelID], [FacilitiesID]) VALUES (N'3', N'1');
INSERT [dbo].[HotelFacilities] ([HotelID], [FacilitiesID]) VALUES (N'3', N'2');
INSERT [dbo].[HotelFacilities] ([HotelID], [FacilitiesID]) VALUES (N'3', N'3');
INSERT [dbo].[HotelFacilities] ([HotelID], [FacilitiesID]) VALUES (N'3', N'4');
INSERT [dbo].[HotelFacilities] ([HotelID], [FacilitiesID]) VALUES (N'3', N'5');
INSERT [dbo].[HotelFacilities] ([HotelID], [FacilitiesID]) VALUES (N'2', N'1');
INSERT [dbo].[HotelFacilities] ([HotelID], [FacilitiesID]) VALUES (N'2', N'2');
INSERT [dbo].[HotelFacilities] ([HotelID], [FacilitiesID]) VALUES (N'2', N'3');
INSERT [dbo].[HotelFacilities] ([HotelID], [FacilitiesID]) VALUES (N'2', N'4');
INSERT [dbo].[HotelFacilities] ([HotelID], [FacilitiesID]) VALUES (N'2', N'5');
INSERT [dbo].[HotelFacilities] ([HotelID], [FacilitiesID]) VALUES (N'2', N'6');
INSERT [dbo].[HotelFacilities] ([HotelID], [FacilitiesID]) VALUES (N'2', N'8');
INSERT [dbo].[HotelFacilities] ([HotelID], [FacilitiesID]) VALUES (N'1', N'5');
INSERT [dbo].[HotelFacilities] ([HotelID], [FacilitiesID]) VALUES (N'1', N'6');
INSERT [dbo].[HotelFacilities] ([HotelID], [FacilitiesID]) VALUES (N'1', N'7');
INSERT [dbo].[HotelFacilities] ([HotelID], [FacilitiesID]) VALUES (N'1', N'8');
INSERT [dbo].[HotelFacilities] ([HotelID], [FacilitiesID]) VALUES (N'1', N'11');
INSERT [dbo].[HotelFacilities] ([HotelID], [FacilitiesID]) VALUES (N'1', N'12');
INSERT [dbo].[HotelFacilities] ([HotelID], [FacilitiesID]) VALUES (N'4', N'1');
INSERT [dbo].[HotelFacilities] ([HotelID], [FacilitiesID]) VALUES (N'4', N'2');
INSERT [dbo].[HotelFacilities] ([HotelID], [FacilitiesID]) VALUES (N'4', N'3');
INSERT [dbo].[HotelFacilities] ([HotelID], [FacilitiesID]) VALUES (N'4', N'4');
INSERT [dbo].[HotelFacilities] ([HotelID], [FacilitiesID]) VALUES (N'4', N'5');
INSERT [dbo].[HotelFacilities] ([HotelID], [FacilitiesID]) VALUES (N'4', N'6');
INSERT [dbo].[HotelFacilities] ([HotelID], [FacilitiesID]) VALUES (N'4', N'7');


/**********======Add data Device======*********/

INSERT [dbo].[Device] ([DeviceName]) VALUES (N'Air conditioning');
INSERT [dbo].[Device] ([DeviceName]) VALUES (N'Bathrobes');
INSERT [dbo].[Device] ([DeviceName]) VALUES (N'Closet');
INSERT [dbo].[Device] ([DeviceName]) VALUES (N'Clothes rack');
INSERT [dbo].[Device] ([DeviceName]) VALUES (N'Coffee/tea maker');
INSERT [dbo].[Device] ([DeviceName]) VALUES (N'Free bottled water');
INSERT [dbo].[Device] ([DeviceName]) VALUES (N'Hair dryer');
INSERT [dbo].[Device] ([DeviceName]) VALUES (N'In-room safe box');
INSERT [dbo].[Device] ([DeviceName]) VALUES (N'Linens');
INSERT [dbo].[Device] ([DeviceName]) VALUES (N'Mirror');
INSERT [dbo].[Device] ([DeviceName]) VALUES (N'Refrigerator');
INSERT [dbo].[Device] ([DeviceName]) VALUES (N'Slippers');
INSERT [dbo].[Device] ([DeviceName]) VALUES (N'Sofa');
INSERT [dbo].[Device] ([DeviceName]) VALUES (N'Telephone');
INSERT [dbo].[Device] ([DeviceName]) VALUES (N'Towels');



/**********======Add data RoomDevice======*********/


INSERT [dbo].[RoomDevice] ([RoomID], [DeviceID]) VALUES (N'1', N'1');
INSERT [dbo].[RoomDevice] ([RoomID], [DeviceID]) VALUES (N'1', N'2');
INSERT [dbo].[RoomDevice] ([RoomID], [DeviceID]) VALUES (N'1', N'3');
INSERT [dbo].[RoomDevice] ([RoomID], [DeviceID]) VALUES (N'1', N'4');
INSERT [dbo].[RoomDevice] ([RoomID], [DeviceID]) VALUES (N'1', N'5');
INSERT [dbo].[RoomDevice] ([RoomID], [DeviceID]) VALUES (N'1', N'6');
INSERT [dbo].[RoomDevice] ([RoomID], [DeviceID]) VALUES (N'1', N'7');
INSERT [dbo].[RoomDevice] ([RoomID], [DeviceID]) VALUES (N'1', N'8');
INSERT [dbo].[RoomDevice] ([RoomID], [DeviceID]) VALUES (N'1', N'9');
INSERT [dbo].[RoomDevice] ([RoomID], [DeviceID]) VALUES (N'2', N'1');
INSERT [dbo].[RoomDevice] ([RoomID], [DeviceID]) VALUES (N'2', N'3');
INSERT [dbo].[RoomDevice] ([RoomID], [DeviceID]) VALUES (N'2', N'5');
INSERT [dbo].[RoomDevice] ([RoomID], [DeviceID]) VALUES (N'2', N'7');
INSERT [dbo].[RoomDevice] ([RoomID], [DeviceID]) VALUES (N'2', N'9');
INSERT [dbo].[RoomDevice] ([RoomID], [DeviceID]) VALUES (N'2', N'11');
INSERT [dbo].[RoomDevice] ([RoomID], [DeviceID]) VALUES (N'2', N'13');
INSERT [dbo].[RoomDevice] ([RoomID], [DeviceID]) VALUES (N'2', N'15');
INSERT [dbo].[RoomDevice] ([RoomID], [DeviceID]) VALUES (N'2', N'2');
INSERT [dbo].[RoomDevice] ([RoomID], [DeviceID]) VALUES (N'3', N'1');
INSERT [dbo].[RoomDevice] ([RoomID], [DeviceID]) VALUES (N'3', N'2');
INSERT [dbo].[RoomDevice] ([RoomID], [DeviceID]) VALUES (N'3', N'3');
INSERT [dbo].[RoomDevice] ([RoomID], [DeviceID]) VALUES (N'3', N'7');
INSERT [dbo].[RoomDevice] ([RoomID], [DeviceID]) VALUES (N'3', N'8');
INSERT [dbo].[RoomDevice] ([RoomID], [DeviceID]) VALUES (N'3', N'9');
INSERT [dbo].[RoomDevice] ([RoomID], [DeviceID]) VALUES (N'3', N'10');
INSERT [dbo].[RoomDevice] ([RoomID], [DeviceID]) VALUES (N'4', N'1');
INSERT [dbo].[RoomDevice] ([RoomID], [DeviceID]) VALUES (N'4', N'2');
INSERT [dbo].[RoomDevice] ([RoomID], [DeviceID]) VALUES (N'4', N'3');
INSERT [dbo].[RoomDevice] ([RoomID], [DeviceID]) VALUES (N'4', N'4');
INSERT [dbo].[RoomDevice] ([RoomID], [DeviceID]) VALUES (N'4', N'5');
INSERT [dbo].[RoomDevice] ([RoomID], [DeviceID]) VALUES (N'4', N'6');
INSERT [dbo].[RoomDevice] ([RoomID], [DeviceID]) VALUES (N'4', N'7');
INSERT [dbo].[RoomDevice] ([RoomID], [DeviceID]) VALUES (N'4', N'8');
INSERT [dbo].[RoomDevice] ([RoomID], [DeviceID]) VALUES (N'4', N'9');
INSERT [dbo].[RoomDevice] ([RoomID], [DeviceID]) VALUES (N'4', N'10');



/**********======Add data Service======*********/


INSERT [dbo].[Service] ([ServiceName], [Price]) VALUES (N'Daily housekeeping', N'10');
INSERT [dbo].[Service] ([ServiceName], [Price]) VALUES (N'Airport shuttle', N'10');
INSERT [dbo].[Service] ([ServiceName], [Price]) VALUES (N'Pets allowed', N'10');
INSERT [dbo].[Service] ([ServiceName], [Price]) VALUES (N'Laundry', N'10');
INSERT [dbo].[Service] ([ServiceName], [Price]) VALUES (N'Drinks', N'10');
INSERT [dbo].[Service] ([ServiceName], [Price]) VALUES (N'Bbreakfast', N'7.5');
INSERT [dbo].[Service] ([ServiceName], [Price]) VALUES (N'Fitness centre', N'5');


/**********======Add data RoomService======*********/


INSERT [dbo].[RoomService] ([RoomID], [ServiceID], [ServiceDefault]) VALUES (N'1', N'1', N'1');
INSERT [dbo].[RoomService] ([RoomID], [ServiceID], [ServiceDefault]) VALUES (N'1', N'2', N'0');
INSERT [dbo].[RoomService] ([RoomID], [ServiceID], [ServiceDefault]) VALUES (N'1', N'3', N'1');
INSERT [dbo].[RoomService] ([RoomID], [ServiceID], [ServiceDefault]) VALUES (N'2', N'1', N'0');
INSERT [dbo].[RoomService] ([RoomID], [ServiceID], [ServiceDefault]) VALUES (N'2', N'3', N'0');
INSERT [dbo].[RoomService] ([RoomID], [ServiceID], [ServiceDefault]) VALUES (N'2', N'4', N'0');
INSERT [dbo].[RoomService] ([RoomID], [ServiceID], [ServiceDefault]) VALUES (N'3', N'1', N'0');
INSERT [dbo].[RoomService] ([RoomID], [ServiceID], [ServiceDefault]) VALUES (N'3', N'2', N'0');
INSERT [dbo].[RoomService] ([RoomID], [ServiceID], [ServiceDefault]) VALUES (N'3', N'4', N'0');